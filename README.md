## Overview

VR-training analytics is an AWS serverless web application. Its main purpose is to display, through a web-hosted GUI, user metrics and indicators retrieved from VR trainings.

## Important Links

[enzo-zerega / vr-training-analytics-app — Bitbucket](https://bitbucket.org/enzo-zerega/vr-training-analytics-app/src/master/)

[VTA board - Agile Board - Jira (atlassian.net)](https://chasacademy-enzozerega.atlassian.net/secure/RapidBoard.jspa?projectKey=VTA&rapidView=1)

[Toggl Track](https://track.toggl.com/timer)


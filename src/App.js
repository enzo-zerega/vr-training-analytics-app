import { useState, Fragment, useEffect } from "react";
import { Route, Switch, useLocation } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core/styles";
import { CssBaseline } from "@material-ui/core";
import { theme } from "./components/style";
import {
  getInitialOrganization,
  getInitialUser,
  getOrganization,
  getUser,
} from "./components/data";
import { Home } from "./components/views";
import Auth from "@aws-amplify/auth";
import {
  AppBar,
  NavDrawer,
  UserRoutes,
  OrganizationRoutes,
  AdminRoutes,
} from "./components/common";

function App() {
  const location = useLocation();
  const [authState, updateAuthState] = useState("signIn");
  const [user, setUser] = useState(getInitialUser());
  const [organization, setOrganization] = useState(getInitialOrganization());
  const [superAdmin, setSuperAdmin] = useState(getInitialUser());
  const [userType, setUserType] = useState(["user"]);
  const [title, updateTitle] = useState("");
  const [drawer, setDrawer] = useState(false);

  useEffect(() => {
    Auth.currentAuthenticatedUser()
      .then((cognitoUser) => {
        setUserType(
          cognitoUser.signInUserSession.idToken.payload[
            "cognito:groups"
          ][0].toLowerCase()
        );
        if (userType === "user") {
          getUser(cognitoUser.attributes.sub).then((res) => {
            setUser(res.data.getUser);
            console.log(res.data.getUser);
          });
        } else if (userType === "org-admin") {
          getOrganization(cognitoUser.attributes.sub).then((res) => {
            setOrganization(res.data.getOrganization);
          });
        } else if (userType === "super-admin") {
          setSuperAdmin({
            id: cognitoUser.username,
            email: cognitoUser.attributes.email,
          });
        }
        updateAuthState("signedIn");
      })
      .catch((err) => {
        console.log({ err });
      });
  }, [userType]);

  const isNotSignedIn =
    authState === "signIn" ||
    authState === "forgotPassword" ||
    authState === "createPassword";
  const isSignedIn = authState === "signedIn";
  const isUser = userType === "user";
  const isOrgAdmin = userType === "org-admin";
  const isSuperAdmin = userType === "super-admin";

  const props = {
    user,
    organization,
    superAdmin,
    authState,
    userType,
    title,
    updateTitle,
    setUserType,
    setOrganization,
    setUser,
    updateAuthState,
    setSuperAdmin,
    isUser,
    isOrgAdmin,
    isSuperAdmin,
  };

  const handleDrawerToggle = () => {
    setDrawer(!drawer);
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {isSignedIn && (
        <Fragment>
          <AppBar handleClick={handleDrawerToggle} {...props} />
          <NavDrawer
            drawer={drawer}
            handleClose={handleDrawerToggle}
            {...props}
          />
        </Fragment>
      )}
      <Switch location={location} key={location.key}>
        {isNotSignedIn && (
          <Route path="/" exact component={() => <Home {...props} />} />
        )}
        {isUser && <UserRoutes {...props} />}
        {isOrgAdmin && <OrganizationRoutes {...props} />}
        {isSuperAdmin && <AdminRoutes {...props} />}
      </Switch>
    </ThemeProvider>
  );
}

export default App;

import { makeStyles } from "@material-ui/core/styles";
import BackgroundImage from "../../assets/img/background.svg";

const drawerWidth = 300;

const useStyles = makeStyles(
  (theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up("md")]: {
        display: "none",
      },
    },
    title: {
      flexGrow: 1,
      marginLeft: theme.spacing(1),
    },
    toolbar: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      [theme.breakpoints.up("md")]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
      },
    },
    drawer: {
      [theme.breakpoints.up("md")]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    appBar: {
      [theme.breakpoints.up("md")]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
      },
    },
    drawerPaper: {
      width: drawerWidth,
    },
    header: {
      marginBottom: theme.spacing(3),
    },
    avatar: {
      marginBottom: theme.spacing(1),
    },
    list: {
      marginTop: theme.spacing(-1),
    },
    listItemText: {
      marginLeft: theme.spacing(9),
    },
    listItemIcon: {
      marginRight: theme.spacing(-2),
    },
    myTrainings: {
      marginRight: theme.spacing(1),
    },
    trainingBoxes: {
      display: "flex",
      flexDirection: "column",
      flexWrap: "wrap",
      alignItems: "center",
      [theme.breakpoints.up("lg")]: {
        flexDirection: "row",
      },
    },
    trainingBox: {
      width: "300px",
      marginBottom: theme.spacing(4),
      [theme.breakpoints.up("sm")]: {
        marginRight: theme.spacing(4),
      },
    },
    trainingTitle: {
      textTransform: "uppercase",
      marginBottom: theme.spacing(2),
    },
    chip: {
      textTransform: "uppercase",
      fontSize: "0.65rem",
      color: theme.palette.secondary.main,
    },
    sessionHeading: {
      display: "flex",
      alignItems: "center",
    },
    sessionDivider: {
      flexGrow: 1,
      marginLeft: theme.spacing(1),
    },
    sessionBox: {
      width: theme.spacing(16),
      height: theme.spacing(16),
      marginTop: theme.spacing(2),
      marginRight: theme.spacing(2),
      marginBottom: theme.spacing(2),
      [theme.breakpoints.up("sm")]: {
        marginRight: theme.spacing(4),
      },
      "& > *": {
        display: "flex",
        flexDirection: "column",
      },
    },
    mySessions: {
      textAlign: "center",
    },
    counter: {
      "& > *": {
        padding: "0px !important",
      },
    },
    counterNum: {
      color: theme.palette.secondary.main,
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    counterZero: {
      color: "black",
    },
    counterZeroSignUp: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
    },
    session: {
      marginBottom: theme.spacing(2),
      "& > *": {
        marginLeft: theme.spacing(2),
      },
    },
    accordion: {
      marginTop: theme.spacing(2),
      display: "flex",
      justifyContent: "center",
    },
    accordionItem: {
      maxWidth: "800px",
      flexGrow: 1,
    },
    disabledAccordion: {
      backgroundColor: "rgba(0,0,0,0.01) !important",
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      alignSelf: "center",
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      marginLeft: "auto",
    },
    signIn: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      color: theme.palette.secondary.main,
      marginBottom: theme.spacing(10),
    },
    signUp: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      color: theme.palette.secondary.main,
      marginBottom: theme.spacing(10),
    },
    signOut: {
      textDecoration: "none",
      color: "#000000",
    },
    addTraining: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      color: theme.palette.secondary.main,
      marginBottom: theme.spacing(2),
    },
    signInTitle: {
      textAlign: "center",
      marginTop: theme.spacing(10),
      marginBottom: theme.spacing(13),
      textTransform: "uppercase",
      color: theme.palette.primary.main,
    },
    signUpTitle: {
      textAlign: "left",
      width: "90%",
      textTransform: "uppercase",
      color: theme.palette.secondary.main,
    },
    signUpSubtitle: {
      textAlign: "left",
      width: "90%",
      fontSize: "1.2rem",
      marginTop: theme.spacing(3),
      color: theme.palette.secondary.main,
    },
    signInIcon: {
      marginTop: theme.spacing(3),
      color: theme.palette.secondary.main,
      backgroundColor: "transparent",
      "& > *": {
        width: theme.spacing(4),
        height: theme.spacing(4),
      },
    },
    signInForm: {
      marginTop: theme.spacing(3),
      width: "90%", // Fix IE 11 issue.
    },
    signUpForm: {
      marginTop: theme.spacing(3),
      width: "90%", // Fix IE 11 issue.
      display: "flex",
      flexDirection: "column",
    },
    signInTextField: {
      marginBottom: theme.spacing(2),
    },
    signInHelperText: {
      marginBottom: theme.spacing(-2.3),
    },
    signInSubmit: {
      margin: theme.spacing(3, 0, 2),
    },
    signUpSubmit: {
      margin: theme.spacing(4, 0, 2),
      paddingLeft: theme.spacing(3),
      paddingRight: theme.spacing(3),
      width: "max-content",
      alignSelf: "center",
      borderRadius: "50px",
    },
    signInLink: {
      cursor: "pointer",
      float: "right",
      color: "#0645AD",
      textAlign: "right",
      marginBottom: theme.spacing(3),
      "&:hover": {
        textDecoration: "none",
      },
    },
    signInFooter: {
      flexGrow: 1,
    },
    signInImg: {
      marginBottom: theme.spacing(3),
    },
    gridBackground: {
      backgroundColor: theme.palette.secondary.light,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      width: "100vw",
      height: "100vh",
      backgroundImage: `url(${BackgroundImage})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      backgroundPosition: "center",
      overflowX: "hidden",
    },
    monthlyChart: {
      height: "300px",
    },
    dashboard: {
      width: "100%",
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center",
      marginBottom: theme.spacing(4),
    },
    dashboardItems: {
      marginTop: theme.spacing(2),
      width: "100%",
      maxWidth: "800px",
      display: "flex",
      flexWrap: "wrap",
    },
    dashboardItem: {
      padding: theme.spacing(3),
      marginBottom: theme.spacing(3),
      width: "100%",
      height: "300px",
      display: "flex",
      flexDirection: "column",
      "& > div": {
        flexGrow: 1,
      },
      [theme.breakpoints.down("sm")]: {
        "& > div": {
          height: "100px",
        },
      },
    },
    progress: {
      padding: theme.spacing(3),
      width: "100%",
      margin: theme.spacing(1),
      display: "flex",
      flexDirection: "column",
    },
    dashboardCard: {
      padding: theme.spacing(3),
      marginBottom: theme.spacing(3),
      maxWidth: "max-content",
      width: "100%",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      textDecoration: "none",
    },
    dashboardCardFull: {
      padding: theme.spacing(3),
      marginBottom: theme.spacing(3),
      width: "100%",
      display: "flex",
      flexDirection: "column",
      maxWidth: "800px",
    },
    dashboardCards: {
      display: "flex",
      flexWrap: "wrap",
      "& > *": {
        marginRight: theme.spacing(3),
      },
    },
    card: {
      padding: theme.spacing(3),
      width: "max-content",
      height: "min-content",
      margin: theme.spacing(1),
      display: "flex",
      flexDirection: "column",
    },
    cardGrow: {
      padding: theme.spacing(3),
      height: "min-content",
      minWidth: "250px",
      margin: theme.spacing(1),
      display: "flex",
      flexDirection: "column",
    },
    cardTitle: {
      textTransform: "uppercase",
      marginBottom: theme.spacing(2),
    },
    cardContent: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      maxWidth: theme.spacing(20),
      "& > *": {
        marginRight: theme.spacing(1),
        marginLeft: theme.spacing(1),
        fontSize: "2rem",
      },
    },
    smCard: {
      padding: theme.spacing(3),
      marginBottom: theme.spacing(3),
      alignSelf: "flex-start",
      width: "100%",
      display: "flex",
      flexDirection: "column",
    },
    flex: {
      display: "flex",
      justifyContent: "center",
    },
    flexLeft: {
      display: "flex",
      justifyContent: "left",
    },
    flexCol: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      width: "max-content",
    },
    stepDetails: {
      display: "flex",
      alignItems: "center",
      flexWrap: "wrap",
      [theme.breakpoints.down("sm")]: {
        justifyContent: "left",
      },
    },
    stepIndicators: {
      display: "flex",
      flexWrap: "wrap",
      margin: theme.spacing(2),
      flexBasis: "100%",
    },
    attempts: {
      margin: theme.spacing(1),
      flexBasis: "100%",
      [theme.breakpoints.down("sm")]: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
      },
    },
    attempt: {
      margin: theme.spacing(1),
      [theme.breakpoints.down("sm")]: {
        flexGrow: 1,
      },
    },
    attemptDetail: {
      margin: theme.spacing(1),
      display: "flex",
      alignItems: "center",
      flexWrap: "wrap",
      [theme.breakpoints.down("sm")]: {
        justifyContent: "center",
        flexDirection: "column",
        width: "auto",
      },
    },
    attemptTitle: {
      backgroundColor: "#f5f4f4",
      textTransform: "uppercase",
      padding: theme.spacing(1),
      alignSelf: "flex-start",
      width: "max-content",
      borderRadius: "4px 0 0 0",
    },
    timeStamp: {
      marginBottom: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
    attemptTime: {
      color: theme.palette.secondary.main,
      fontStyle: "italic",
      fontSize: "0.7rem",
      marginBottom: theme.spacing(1),
    },
    attemptMetrics: {
      flexBasis: "100%",
    },
    metricCardsContainer: {
      display: "flex",
      flexWrap: "wrap",
    },
    attemptTitleContainer: {
      flexBasis: "100%",
      display: "flex",
      justifyContent: "space-between",
    },
    attemptStatus: {
      margin: theme.spacing(1),
    },
    table: {},
    trainingChips: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      "& div:last-child": {
        marginBottom: "unset !important",
      },
    },
    trainingChip: {
      color: "ffffff",
      width: "max-content",
      backgroundColor: "#f2f9ff !important",
      marginBottom: theme.spacing(1),
    },
    usersTitle: {
      color: `${theme.palette.secondary.main} !important`,
      fontWeight: "700 !important",
    },
    trainingChipsRegister: {
      display: "flex",
      flexWrap: "wrap",
      marginTop: theme.spacing(2),
      "& > div": {
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(2),
      },
    },
    trainingChipRegister: {
      backgroundColor: "#DDF0FF !important",
    },
    trainingChipUnregister: {
      backgroundColor: theme.palette.gray.light,
    },
    breadcrumbs: {
      marginBottom: theme.spacing(3),
      "& li": {
        color: theme.palette.gray.main,
        margin: "unset",
      },
      "& li > a": {
        textDecoration: "none",
        color: theme.palette.gray.main,
      },
    },
    mainLinkButton: {
      marginRight: theme.spacing(1),
      marginBottom: theme.spacing(3),
    },
    linkToUser: {
      color: theme.palette.secondary.main,
    },
    disableText: {
      margin: theme.spacing(1),
      marginTop: theme.spacing(3),
      color: theme.palette.disable.main,
      display: "flex",
      alignItems: "center",
      "& svg": {
        marginRight: theme.spacing(1),
      },
    },
    deleteText: {
      margin: theme.spacing(1),
      color: theme.palette.delete.main,
      display: "flex",
      alignItems: "center",
      "& svg": {
        marginRight: theme.spacing(1),
      },
    },
    topButtons: {
      display: "flex",
      flexWrap: "wrap",
      "& a": {
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(2),
      },
    },
    tableItem: {
      padding: theme.spacing(2),
    },
  }),
  { index: 1 }
);

export default useStyles;

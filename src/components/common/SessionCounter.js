import React from "react";
import { useStyles } from "../style";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import { Typography } from "@material-ui/core";

export default function Counter(props) {
  const { count } = props;
  const classes = useStyles();

  return (
    <ToggleButtonGroup
      aria-label="Session counter"
      className={classes.counter}
      disabled
    >
      <ToggleButton value="" className={classes.counter} disabled>
        <Typography className={classes.counterNum}>{count}</Typography>
      </ToggleButton>
    </ToggleButtonGroup>
  );
}

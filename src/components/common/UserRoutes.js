import { Fragment } from "react";
import { ProtectedRoute } from ".";
import { User, Trainings, Training, Sessions, Session } from "../views";

export default function UserRoutes(props) {
  const { authState } = props;
  return (
    <Fragment>
      <ProtectedRoute
        path="/"
        exact
        authState={authState}
        component={() => <User {...props} />}
      />
      <ProtectedRoute
        path="/trainings"
        authState={authState}
        exact
        component={() => <Trainings {...props} />}
      />
      <ProtectedRoute
        path="/trainings/:training"
        authState={authState}
        exact
        component={() => <Training {...props} />}
      />
      <ProtectedRoute
        path="/trainings/:training/sessions"
        authState={authState}
        exact
        component={() => <Sessions {...props} />}
      />
      <ProtectedRoute
        path="/trainings/:training/sessions/guided/:session"
        authState={authState}
        exact
        component={() => <Session {...props} />}
      />
      <ProtectedRoute
        path="/trainings/:training/sessions/evaluations/:session"
        authState={authState}
        exact
        component={() => <Session {...props} />}
      />
    </Fragment>
  );
}

import { CircularProgress, makeStyles } from "@material-ui/core";

export default function ProgressCircle(props) {
  const progress = makeStyles((theme) => ({
    root: {
      position: "relative",
      marginTop: theme.spacing(0.8),
    },
    bottom: {
      color: theme.palette.grey[theme.palette.type === "light" ? 300 : 700],
    },
    top: {
      color: "#1A406B",
      animationDuration: "550ms",
      position: "absolute",
      left: 0,
    },
    circle: {
      strokeLinecap: "round",
    },
  }));

  const classes = progress();

  return (
    <div className={classes.root}>
      <CircularProgress
        variant="static"
        className={classes.bottom}
        thickness={4}
        value={100}
      />
      <CircularProgress
        variant="static"
        className={classes.top}
        classes={{
          circle: classes.circle,
        }}
        thickness={4}
        {...props}
      />
    </div>
  );
}

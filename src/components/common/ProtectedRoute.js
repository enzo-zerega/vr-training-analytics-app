import { Route, Redirect } from "react-router-dom";

export default function ProtectedRoute(props) {
  const { path, component, exact, authState } = props;
  if (authState === "signedIn")
    return <Route path={path} exact={exact} component={component} />;
  return <Redirect to="/" />;
}

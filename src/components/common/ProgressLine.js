import { withStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 30,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor:
      theme.palette.grey[theme.palette.type === "light" ? 300 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: "#1A406B",
  },
}))(LinearProgress);

export default function CustomizedProgressBars(props) {
  const { value } = props;
  return (
    <BorderLinearProgress variant="determinate" value={value ? value : 0} />
  );
}

import { makeStyles, useTheme } from "@material-ui/core/styles";
import { Typography, Paper } from "@material-ui/core";
import DoughnutChart from "../DoughnutChart";
import uniqueId from "lodash/uniqueId";

const useStyles = makeStyles((theme) => ({
  card: {
    display: "flex",
    flexDirection: "column",
    margin: theme.spacing(2),
    alignItems: "center",
    "& > *": {
      color: "#1A406B",
    },
  },
  subtasks: {
    width: "100%",
  },
  doughnutContainer: {
    backgroundColor: "#f2f9ff",
    padding: theme.spacing(2),
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  doughnut: {
    width: "70%",
  },
  leyend: {
    width: "30px",
    height: "30px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: theme.palette.completed.dark,
  },
  leyendItem: {
    display: "flex",
    alignItems: "center",
    marginTop: theme.spacing(1),
    "& > *": {
      marginRight: theme.spacing(1),
      textTransform: "uppercase",
    },
  },
  leyendContainer: {
    width: "max-content",
  },
  name: {
    textTransform: "uppercase",
    fontSize: "0.8rem",
    marginTop: theme.spacing(1),
  },
}));

export default function Chart(props) {
  const classes = useStyles();
  const theme = useTheme();
  const { metric, chartType } = props;
  const data = JSON.parse(metric.data);

  return (
    chartType === "doughnut" && (
      <div className={classes.subtasks}>
        <div className={classes.card}>
          <Paper elevation={0} className={classes.doughnutContainer} square>
            <div className={classes.doughnut}>
              <DoughnutChart data={data.dataset} />
            </div>
            <div className={classes.leyendContainer}>
              {data.labels.map((label, i) => (
                <div key={uniqueId("chart-")} className={classes.leyendItem}>
                  <div
                    className={classes.leyend}
                    style={{
                      backgroundColor:
                        theme.palette.doughnut[
                          Object.keys(theme.palette.doughnut)[i]
                        ],
                      color:
                        theme.palette.doughnutText[
                          Object.keys(theme.palette.doughnutText)[i]
                        ],
                    }}
                  >
                    {data.dataset[i]}
                  </div>
                  <Typography>{label}</Typography>
                </div>
              ))}
            </div>
          </Paper>
          <Typography className={classes.name}>{metric.name}</Typography>
        </div>
      </div>
    )
  );
}

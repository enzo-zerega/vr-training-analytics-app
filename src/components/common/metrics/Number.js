import { makeStyles } from "@material-ui/core/styles";
import { Typography, Paper, Box } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  card: {
    display: "flex",
    flexDirection: "column",
    margin: theme.spacing(2),
    alignItems: "center",
    "& > *": {
      color: "#1A406B",
    },
  },
  square: {
    backgroundColor: "#f2f9ff",
    textAlign: "center",
    "& > *": {
      width: theme.spacing(16),
      height: theme.spacing(16),
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      fontSize: "2.5rem",
    },
  },
  unit: {
    fontSize: "1rem",
    paddingLeft: theme.spacing(1),
  },
  name: {
    textTransform: "uppercase",
    fontSize: "0.8rem",
    marginTop: theme.spacing(1),
  },
}));

export default function Number(props) {
  const classes = useStyles();
  const { metric, chartType } = props;
  const data = JSON.parse(metric.data);

  return (
    chartType === "int-unit" && (
      <div className={classes.card}>
        <Paper elevation={0} className={classes.square} square>
          <Typography>
            {data.value}
            {data.unit && (
              <Box component="span" className={classes.unit}>
                {data.unit}
              </Box>
            )}
          </Typography>
        </Paper>
        <Typography className={classes.name}>{metric.name}</Typography>
      </div>
    )
  );
}

import { makeStyles } from "@material-ui/core/styles";
import { Typography, Paper } from "@material-ui/core";
import { CheckCircleOutlineRounded, CancelOutlined } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  card: {
    display: "flex",
    flexDirection: "column",
    margin: theme.spacing(2),
    alignItems: "center",
    "& > *": {
      color: "#1A406B",
    },
  },
  subtasks: {
    width: "100%",
  },
  rectangle: {
    backgroundColor: "#f2f9ff",
    display: "flex",
    width: "100%",
    padding: theme.spacing(2),
    "& > *": {
      alignItems: "center",
      justifyContent: "center",
      marginRight: theme.spacing(1),
    },
  },
}));

export default function Text(props) {
  const classes = useStyles();
  const { metric, chartType } = props;
  const data = JSON.parse(metric.data);

  return (
    chartType === "task" && (
      <div className={classes.subtasks}>
        <div className={classes.card}>
          <Paper elevation={0} className={classes.rectangle} square>
            {data.status === "completed" ? (
              <CheckCircleOutlineRounded />
            ) : (
              <CancelOutlined />
            )}

            <Typography>{data.description}</Typography>
          </Paper>
        </div>
      </div>
    )
  );
}

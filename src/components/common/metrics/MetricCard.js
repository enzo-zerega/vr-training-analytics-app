import { Chart, Text, Number } from "./";
import { useStyles } from "../../style";

export default function MetricCard(props) {
  const classes = useStyles();
  const { metric, metricCards } = props;
  const chartType = metricCards.find(
    (metricCard) => metricCard.type === "chart"
  ).card;
  const textType = metricCards.find((metricCard) => metricCard.type === "text")
    .card;
  const numberType = metricCards.find(
    (metricCard) => metricCard.type === "number"
  ).card;

  return (
    <div className={classes.metricCardsContainer}>
      {metric.type === "chart" && (
        <Chart metric={metric} chartType={chartType} />
      )}
      {metric.type === "text" && <Text metric={metric} chartType={textType} />}
      {metric.type === "number" && (
        <Number metric={metric} chartType={numberType} />
      )}
    </div>
  );
}

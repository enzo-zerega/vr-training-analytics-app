import { Fragment } from "react";
import { ProtectedRoute } from ".";
import {
  Resources,
  Organizations,
  AddOrganization,
  Trainings,
  AddTraining,
  Organization,
  EditOrganization,
  Training,
  EditTraining,
  Overview,
  Settings,
} from "../views";

export default function AdminRoutes(props) {
  const { authState } = props;
  return (
    <Fragment>
      <ProtectedRoute
        path="/"
        exact
        authState={authState}
        component={() => <Resources {...props} />}
      />
      <ProtectedRoute
        path="/organizations"
        authState={authState}
        exact
        component={() => <Organizations {...props} />}
      />
      <ProtectedRoute
        path="/sign-up/organization"
        authState={authState}
        exact
        component={() => <AddOrganization {...props} />}
      />
      <ProtectedRoute
        path="/trainings"
        authState={authState}
        exact
        component={() => <Trainings {...props} />}
      />
      <ProtectedRoute
        path="/trainings/add"
        authState={authState}
        exact
        component={() => <AddTraining {...props} />}
      />
      <ProtectedRoute
        path="/organizations/:organization/edit"
        authState={authState}
        exact
        component={() => <EditOrganization {...props} />}
      />
      <ProtectedRoute
        path="/organizations/:organization"
        authState={authState}
        exact
        component={() => <Organization {...props} />}
      />
      <ProtectedRoute
        path="/organizations/:organization/trainings"
        authState={authState}
        exact
        component={() => <Trainings {...props} />}
      />
      <ProtectedRoute
        path="/organizations/:organization/trainings/:training"
        authState={authState}
        exact
        component={() => <Training {...props} />}
      />
      <ProtectedRoute
        path="/organizations/:organization/trainings/:training/edit"
        authState={authState}
        exact
        component={() => <EditTraining {...props} />}
      />
      <ProtectedRoute
        path="/overview"
        authState={authState}
        exact
        component={() => <Overview {...props} />}
      />
      <ProtectedRoute
        path="/settings"
        authState={authState}
        exact
        component={() => <Settings {...props} />}
      />
    </Fragment>
  );
}

import { Doughnut } from "react-chartjs-2";
import { useTheme } from "@material-ui/core/styles";

export default function DoughnutChart(props) {
  const theme = useTheme();
  const { data: results } = props;
  const data = {
    datasets: [
      {
        backgroundColor: [
          theme.palette.doughnut.first,
          theme.palette.doughnut.second,
          theme.palette.doughnut.third,
          theme.palette.doughnut.fourth,
          theme.palette.doughnut.fifth,
          theme.palette.doughnut.sixth,
        ],
        data: results,
        borderWidth: 0,
      },
    ],
  };

  const options = {
    legend: {
      display: false,
    },
    hover: { mode: null },
    tooltips: {
      enabled: false,
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false,
          },
          ticks: {
            display: false,
          },
        },
      ],
      yAxes: [
        {
          gridLines: {
            display: false,
          },
          ticks: {
            display: false,
          },
        },
      ],
    },
  };
  return <Doughnut data={data} options={options} />;
}

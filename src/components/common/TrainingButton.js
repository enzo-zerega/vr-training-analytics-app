import { Button, withStyles } from "@material-ui/core";

const TrainingButton = withStyles({
  root: {
    textTransform: "none",
    justifyContent: "flex-start",
  },
})(Button);

export default TrainingButton;

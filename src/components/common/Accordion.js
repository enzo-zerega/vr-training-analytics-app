import { useEffect, useState } from "react";
import { useStyles } from "../style";
import AccordionMU from "@material-ui/core/Accordion";
import {
  AccordionDetails,
  AccordionSummary,
  Typography,
} from "@material-ui/core";
import { ExpandMore } from "@material-ui/icons";
import { StatusChip } from "../common";
import { MetricCard } from "../common/metrics";
import {
  spaceToHyphen,
  capitalizeFirstChar,
  sortObjectByPropertyString,
} from "../common/methods";
import dateFormat from "dateformat";
import uniqueId from "lodash/uniqueId";
import { TimeSpent } from "./indicators";

export default function Accordion(props) {
  const {
    steps: stepsFromProps,
    session: sessionFromProps,
    metricCards,
  } = props;
  const classes = useStyles();

  const stepsNumber = sessionFromProps.stepsNumber;
  const [expanded, setExpanded] = useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  useEffect(() => {
    sortObjectByPropertyString(stepsFromProps, "startedAt");
  }, [stepsFromProps]);

  const steps = (items) => {
    let arr = [];
    for (let i = 0; i < stepsNumber; i++) {
      let step = {};
      if (items[i] && items[i].name) {
        step = items[i];
        const stepStatus = step.status;
        arr.push(
          <AccordionMU
            key={i}
            expanded={expanded === spaceToHyphen(step.name)}
            onChange={handleChange(spaceToHyphen(step.name))}
          >
            <AccordionSummary
              expandIcon={<ExpandMore />}
              aria-controls={`${spaceToHyphen(step.name)}bh-content`}
              id={`${spaceToHyphen(step.name)}bh-header`}
            >
              <Typography className={classes.heading}>
                {capitalizeFirstChar(step.name)}
              </Typography>
              <div className={classes.secondaryHeading}>
                <StatusChip status={stepStatus} />
              </div>
            </AccordionSummary>
            <AccordionDetails className={classes.stepDetails}>
              <div className={classes.attempts}>
                <div className={classes.attemptDetail}>
                  <div className={classes.timeStamp}>
                    <Typography
                      component={"span"}
                      className={classes.attemptTime}
                    >
                      <div>Started At:</div>
                      <div>
                        {dateFormat(step.startedAt, "mmm d, yyyy, HH:MM")}
                      </div>
                      <div>Finished At:</div>
                      <div>
                        {dateFormat(step.finishedAt, "mmm d, yyyy, HH:MM")}
                      </div>
                    </Typography>
                  </div>
                  <TimeSpent data={step} scope={"step"} />
                  <div className={classes.attemptMetrics}>
                    {step.metrics.items.map((metric) => (
                      <MetricCard
                        key={uniqueId("metricCard-")}
                        metric={metric}
                        metricCards={metricCards}
                      />
                    ))}
                  </div>
                </div>
              </div>
            </AccordionDetails>
          </AccordionMU>
        );
      } else {
        arr.push(
          <AccordionMU
            key={i}
            disabled
            expanded={expanded === spaceToHyphen(`Step ${i + 1}`)}
            onChange={handleChange(spaceToHyphen(`Step ${i + 1}`))}
            className={classes.disabledAccordion}
            elevation={0}
          >
            <AccordionSummary
              aria-controls={`${spaceToHyphen(`Step ${i + 1}`)}bh-content`}
              id={`${spaceToHyphen(`Step ${i + 1}`)}bh-header`}
            >
              <Typography className={classes.heading}>
                {capitalizeFirstChar("Not initiated")}
              </Typography>
            </AccordionSummary>
            <AccordionDetails
              className={classes.stepDetails}
            ></AccordionDetails>
          </AccordionMU>
        );
      }
    }
    return arr;
  };

  return (
    <div className={classes.accordion}>
      <div className={classes.accordionItem}>
        <div>{steps(stepsFromProps)}</div>
      </div>
    </div>
  );
}

import Chip from "@material-ui/core/Chip";
import { useTheme } from "@material-ui/core/styles";
import { useStyles } from "../style";

export default function StatusChip(props) {
  const { status } = props;
  const classes = useStyles();
  const theme = useTheme();

  return status ? (
    <Chip
      label={status}
      className={classes.chip}
      style={{
        backgroundColor:
          status !== "not initiated" && status !== ""
            ? theme.palette[status].main
            : theme.palette.gray.main,
      }}
    />
  ) : (
    <></>
  );
}

import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import { Link } from "react-router-dom";
import Auth from "@aws-amplify/auth";
import { useStyles } from "../style";

export default function Options(props) {
  const { anchorEl, isOptionOpen, handleClose } = props;
  const classes = useStyles();
  function signOut() {
    Auth.signOut();
  }

  return (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isOptionOpen}
      onClose={handleClose}
    >
      <MenuItem component={Link} to="/settings" onClick={handleClose}>
        Settings
      </MenuItem>
      <a
        href="/"
        onClick={() => {
          handleClose();
          signOut();
        }}
        className={classes.signOut}
      >
        <MenuItem>Sign Out</MenuItem>
      </a>
    </Menu>
  );
}

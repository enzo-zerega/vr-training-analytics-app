import { Fragment } from "react";
import { ProtectedRoute } from ".";
import {
  Resources,
  Users,
  User,
  Trainings,
  Training,
  Sessions,
  Session,
  Organization,
  Settings,
  EditUser,
} from "../views";
import { SignUp } from "../auth";

export default function OrganizationRoutes(props) {
  const { authState } = props;
  return (
    <Fragment>
      <ProtectedRoute
        path="/"
        exact
        authState={authState}
        component={() => <Resources {...props} />}
      />
      <ProtectedRoute
        path="/sign-up"
        authState={authState}
        exact
        component={() => <SignUp {...props} />}
      />
      <ProtectedRoute
        path="/users"
        authState={authState}
        exact
        component={() => <Users {...props} />}
      />
      <ProtectedRoute
        path="/users/:user"
        authState={authState}
        exact
        component={() => <User {...props} />}
      />
      <ProtectedRoute
        path="/users/:user/trainings"
        authState={authState}
        exact
        component={() => <Trainings {...props} />}
      />
      <ProtectedRoute
        path="/users/:user/trainings/:training"
        authState={authState}
        exact
        component={() => <Training {...props} />}
      />
      <ProtectedRoute
        path="/users/:user/trainings/:training/sessions"
        authState={authState}
        exact
        component={() => <Sessions {...props} />}
      />
      <ProtectedRoute
        path="/users/:user/trainings/:training/sessions/guided/:session"
        authState={authState}
        exact
        component={() => <Session {...props} />}
      />
      <ProtectedRoute
        path="/users/:user/trainings/:training/sessions/evaluations/:session"
        authState={authState}
        exact
        component={() => <Session {...props} />}
      />
      <ProtectedRoute
        path="/users/:user/edit"
        authState={authState}
        exact
        component={() => <EditUser {...props} />}
      />
      <ProtectedRoute
        path="/trainings"
        authState={authState}
        exact
        component={() => <Trainings {...props} />}
      />
      <ProtectedRoute
        path="/trainings/:training"
        authState={authState}
        exact
        component={() => <Training {...props} />}
      />
      <ProtectedRoute
        path="/trainings/:training/users"
        authState={authState}
        exact
        component={() => <Users {...props} />}
      />
      <ProtectedRoute
        path="/overview"
        authState={authState}
        exact
        component={() => <Organization {...props} />}
      />
      <ProtectedRoute
        path="/settings"
        authState={authState}
        exact
        component={() => <Settings {...props} />}
      />
    </Fragment>
  );
}

import { useState, Fragment } from "react";
import {
  AppBar as AppBarM,
  Toolbar,
  Typography,
  IconButton,
} from "@material-ui/core";
import { Menu, MoreVert, ArrowBack } from "@material-ui/icons";
import useStyles from "../style/UseStyles";
import Options from "./Options";
import { useHistory } from "react-router-dom";
import { capitalizeFirstChar } from "./methods";

export default function AppBar(props) {
  const { title, handleClick } = props;
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const isOptionOpen = Boolean(anchorEl);
  const history = useHistory();
  const pathname = history.location.pathname;

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOptionsOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleGoBack = () => {
    history.goBack();
  };

  return (
    <Fragment>
      <AppBarM
        position="fixed"
        elevation={1}
        className={classes.appBar}
        color="primary"
      >
        <Toolbar>
          <IconButton
            edge="start"
            onClick={handleClick}
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
          >
            <Menu />
          </IconButton>
          {pathname !== "/" ? (
            <Fragment>
              <IconButton
                edge="start"
                color="inherit"
                aria-label="return"
                onClick={handleGoBack}
              >
                <ArrowBack />
              </IconButton>
              <Typography variant="h6" className={classes.title} noWrap>
                {capitalizeFirstChar(title)}
              </Typography>
            </Fragment>
          ) : (
            <Fragment>
              <IconButton edge="start" style={{ opacity: 0 }} disabled>
                <ArrowBack />
              </IconButton>
              <Typography variant="h6" className={classes.title} noWrap>
                {capitalizeFirstChar(title)}
              </Typography>
            </Fragment>
          )}

          <div>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="options"
              onClick={handleOptionsOpen}
            >
              <MoreVert />
            </IconButton>
            <Options
              anchorEl={anchorEl}
              handleClose={handleClose}
              isOptionOpen={isOptionOpen}
            />
          </div>
        </Toolbar>
      </AppBarM>
    </Fragment>
  );
}

import { Typography } from "@material-ui/core";
import { useStyles } from "../../style";
import {
  Progress,
  TimeSpent,
  MonthlyTimeSpent,
  SessionsStatus,
  Attempts,
} from "./";
import uniqueId from "lodash/uniqueId";

export default function Indicators(props) {
  const { items } = props;
  const classes = useStyles();

  return items.map((indicator) => {
    if (indicator.type.toLowerCase() === "progress") {
      return <Progress {...props} key={uniqueId("indicator-")} />;
    } else if (indicator.type.toLowerCase() === "timespent") {
      return <TimeSpent {...props} key={uniqueId("indicator-")} />;
    } else if (indicator.type.toLowerCase() === "monthlytimespent") {
      return <MonthlyTimeSpent {...props} key={uniqueId("indicator-")} />;
    } else if (indicator.type.toLowerCase() === "sessionstatus") {
      return <SessionsStatus {...props} key={uniqueId("indicator-")} />;
    } else if (indicator.type.toLowerCase() === "attempts") {
      return <Attempts {...props} key={uniqueId("indicator-")} />;
    } else {
      return (
        <Typography
          className={classes.counterZero}
          key={uniqueId("indicator-")}
        >
          No indicators available
        </Typography>
      );
    }
  });
}

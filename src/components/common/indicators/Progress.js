import { ProgressLine } from "../";
import { useStyles } from "../../style";
import { Paper, Typography } from "@material-ui/core";
import { calcProgress } from "../methods";

export default function Progress(props) {
  const { scope, data } = props;
  const classes = useStyles();
  const progress = calcProgress(data, scope);

  return (
    <Paper className={classes.progress}>
      <Typography variant="h6" className={classes.cardTitle} color="secondary">
        Progress
      </Typography>
      <ProgressLine value={progress} />
    </Paper>
  );
}

import { useStyles } from "../../style";
import { ListItemText, ListItemIcon } from "@material-ui/core";
import { Group } from "@material-ui/icons";
import { LinkButton } from "../";
import { Link } from "react-router-dom";

export default function UsersCount(props) {
  const { users, training, global, disabled } = props;
  const classes = useStyles();
  const count = users ? users.length : 0;

  return (
    <LinkButton
      variant="contained"
      color="secondary"
      disabled={disabled}
      component={Link}
      to={!global ? `/trainings/${training}/users` : "/users"}
      className={classes.mainLinkButton}
    >
      <ListItemIcon className={classes.listItemIcon}>
        <Group color="primary" />
      </ListItemIcon>
      <ListItemText
        className={classes.myTrainings}
        primary={count !== 1 ? `${count} users` : `${count} user`}
      />
    </LinkButton>
  );
}

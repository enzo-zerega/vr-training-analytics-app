import { AccessTime } from "@material-ui/icons";
import { useStyles } from "../../style";
import { Paper, Typography } from "@material-ui/core";
import { calcTimeSpent } from "../methods";

export default function TimeSpent(props) {
  const { data, scope } = props;
  const classes = useStyles();
  const timeSpent = calcTimeSpent(data, scope);

  return (
    <Paper className={classes.card}>
      <Typography variant="h6" className={classes.cardTitle} color="secondary">
        Time Spent
      </Typography>
      <Typography
        className={classes.cardContent}
        color="secondary"
        variant="h5"
      >
        <AccessTime />
        {timeSpent} min
      </Typography>
    </Paper>
  );
}

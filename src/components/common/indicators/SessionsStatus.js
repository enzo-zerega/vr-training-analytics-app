import { useStyles } from "../../style";
import { Paper, Typography } from "@material-ui/core";
import { CheckCircleOutline, ErrorOutline } from "@material-ui/icons";
import { countSessionsStatus } from "../methods";

export default function SessionsStatus(props) {
  const { data, scope } = props;
  const classes = useStyles();
  const status = countSessionsStatus(data, scope);

  return (
    <Paper className={classes.card}>
      <Typography variant="h6" className={classes.cardTitle} color="secondary">
        Sessions
      </Typography>
      <div className={classes.flexLeft}>
        <Typography variant="h6" color="secondary">
          Guided
        </Typography>
      </div>
      <div className={classes.flexLeft}>
        <Typography
          className={classes.cardContent}
          color="secondary"
          variant="h5"
        >
          <CheckCircleOutline style={{ color: "#A5D6A7" }} />
          {status.guided.completed}
        </Typography>
        <Typography
          className={classes.cardContent}
          color="secondary"
          variant="h5"
        >
          <ErrorOutline />
          {status.guided.other}
        </Typography>
      </div>
      <div className={classes.flexLeft}>
        <Typography variant="h6" color="secondary">
          Evaluation
        </Typography>
      </div>
      <div className={classes.flexLeft}>
        <Typography
          className={classes.cardContent}
          color="secondary"
          variant="h5"
        >
          <CheckCircleOutline style={{ color: "#A5D6A7" }} />
          {status.evaluation.completed}
        </Typography>
        <Typography
          className={classes.cardContent}
          color="secondary"
          variant="h5"
        >
          <ErrorOutline />
          {status.evaluation.other}
        </Typography>
      </div>
    </Paper>
  );
}

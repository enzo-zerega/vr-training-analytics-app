import { useStyles } from "../../style";
import { Paper, Typography } from "@material-ui/core";

export default function SessionCount(props) {
  const { data } = props;
  const classes = useStyles();

  const sessionsCount = data.length;

  return (
    <Paper className={classes.card}>
      <Typography variant="h6" className={classes.cardTitle} color="secondary">
        {sessionsCount !== 1 ? "sessions" : "session"}
      </Typography>
      <Typography
        className={classes.cardContent}
        color="secondary"
        variant="h5"
      >
        {sessionsCount}
      </Typography>
    </Paper>
  );
}

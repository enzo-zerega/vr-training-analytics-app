import { useStyles } from "../../style";
import { Paper, Typography } from "@material-ui/core";
import { calcMonthlyTimeSpent } from "../methods";
import { BarChart } from "../";

export default function MonthlyTimeSpent(props) {
  const { data, scope } = props;
  const classes = useStyles();

  const dataset = calcMonthlyTimeSpent(data, scope);

  return (
    <Paper className={classes.cardGrow}>
      <Typography variant="h6" className={classes.cardTitle} color="secondary">
        Monthly Time Spent
      </Typography>
      <div>
        {dataset[0].year ? (
          <BarChart data={dataset} />
        ) : (
          <Typography className={classes.counterZero}>
            Na data available
          </Typography>
        )}
      </div>
    </Paper>
  );
}

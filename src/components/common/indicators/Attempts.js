import { useStyles } from "../../style";
import { Paper, Typography } from "@material-ui/core";

export default function Attempts(props) {
  const { attempts } = props;
  const classes = useStyles();

  return (
    <Paper className={classes.card}>
      <Typography variant="h6" className={classes.cardTitle} color="secondary">
        Attempts
      </Typography>
      <Typography
        className={classes.cardContent}
        color="secondary"
        variant="h5"
      >
        {attempts}
      </Typography>
    </Paper>
  );
}

import { Bar } from "react-chartjs-2";
import { useTheme } from "@material-ui/core/styles";

export default function BarChart(props) {
  const theme = useTheme();
  const { data: dataFromProps } = props;
  let data = {
    labels: [""],
    datasets: [
      {
        backgroundColor: theme.palette.secondary.main,
        data: "",
        minBarThickness: 10,
        maxBarThickness: 30,
      },
    ],
  };

  const yearsFields = Object.keys(dataFromProps).map(
    (key) => dataFromProps[key].year
  );
  const years = yearsFields.filter((item, i, ar) => ar.indexOf(item) === i);

  try {
    data = {
      labels: dataFromProps.map((month) => month.month),
      datasets: [
        {
          backgroundColor: theme.palette.secondary.main,
          data: dataFromProps.map((month) => month.time),
          minBarThickness: 10,
          maxBarThickness: 30,
        },
      ],
    };
  } catch (err) {}

  const options = {
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    animation: {
      duration: 0,
    },
    hover: { mode: null },
    tooltips: {
      enabled: false,
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false,
          },
          scaleLabel: {
            display: true,
            labelString: `month, ${
              years.length <= 1 ? years[0] : `${years[0]}/${years[1]}`
            }`,
          },
        },
      ],
      yAxes: [
        {
          gridLines: {
            display: false,
          },
          ticks: {
            display: true,
            stepSize: 20,
          },
          scaleLabel: {
            display: true,
            labelString: "minutes",
          },
        },
      ],
    },
  };
  return <Bar data={data} options={options} />;
}

import { calcMinutes, reverseObjectByPropertyString } from "./";

export default function calcTimeSpent(data, scope) {
  if (scope === "global") {
    let totalTime = 0;
    data.map((user) =>
      user.trainings.items.map((training) =>
        training.sessions.items.map((session) =>
          session.steps.items.map((step) => {
            const startedAt = new Date(step.startedAt);
            const finishedAt = new Date(step.finishedAt);
            totalTime += calcMinutes(finishedAt - startedAt);
            return step;
          })
        )
      )
    );
    return totalTime;
  } else if (scope === "training") {
    let totalTime = 0;
    data.map((training) =>
      training.sessions.items.map((session) =>
        session.steps.items.map((step) => {
          const startedAt = new Date(step.startedAt);
          const finishedAt = new Date(step.finishedAt);
          totalTime += calcMinutes(finishedAt - startedAt);
          return step;
        })
      )
    );
    return totalTime;
  } else if (scope === "session") {
    let totalTime = 0;
    data.map((session) =>
      session.steps.items.map((step) => {
        const startedAt = new Date(step.startedAt);
        const finishedAt = new Date(step.finishedAt);
        totalTime += calcMinutes(finishedAt - startedAt);
        return step;
      })
    );
    return totalTime;
  } else if (scope === "step") {
    const startedAt = new Date(data.startedAt);
    const finishedAt = new Date(data.finishedAt);
    let totalTime = calcMinutes(finishedAt - startedAt);
    return totalTime;
  } else if (scope === "monthly-by-training") {
    let totalTime = [];
    data.map((training) =>
      training.sessions.items.map((session) =>
        session.steps.items.map((step) =>
          step.attempts.items.map((attempt) => {
            const startedAt = new Date(attempt.startedAt);
            const finishedAt = new Date(attempt.finishedAt);

            if (
              totalTime.find(
                (element) => element.year === startedAt.getFullYear()
              )
            ) {
              if (
                totalTime
                  .find((element) => element.year === startedAt.getFullYear())
                  .months.find((el) => el.month === startedAt.getMonth())
              ) {
                totalTime
                  .find((element) => element.year === startedAt.getFullYear())
                  .months.find(
                    (el) => el.month === startedAt.getMonth()
                  ).minutes += calcMinutes(finishedAt - startedAt);
              } else {
                totalTime
                  .find((element) => element.year === startedAt.getFullYear())
                  .months.push({
                    month: startedAt.getMonth(),
                    minutes: calcMinutes(finishedAt - startedAt),
                  });
              }
            } else {
              totalTime.push({
                year: startedAt.getFullYear(),
                months: [
                  {
                    month: startedAt.getMonth(),
                    minutes: calcMinutes(finishedAt - startedAt),
                  },
                ],
              });
            }
            return session;
          })
        )
      )
    );

    reverseObjectByPropertyString(totalTime, "year");
    totalTime.map((year) =>
      reverseObjectByPropertyString(year.months, "month")
    );
    return totalTime;
  }
}

export default function calcProgress(data, scope) {
  if (scope === "global") {
    let totalSteps = 0;
    let completedSteps = 0;
    data.map((user) => {
      user.trainings.items.map((training) => {
        training.sessions.items.map((session) => {
          totalSteps += session.stepsNumber;
          session.steps.items.map((step) => {
            if (step.status === "completed") completedSteps += 1;
            return step;
          });
          return session;
        });
        return training;
      });
      return user;
    });

    return (completedSteps / totalSteps) * 100;
  } else if (scope === "training") {
    let trainingSteps = 0;
    let completedSteps = 0;
    data.map((training) => {
      training.sessions.items.map((session) => {
        trainingSteps += session.stepsNumber;
        session.steps.items.map((step) => {
          if (step.status === "completed") completedSteps += 1;
          return step;
        });
        return session;
      });
      return training;
    });
    return (completedSteps / trainingSteps) * 100;
  } else if (scope === "session") {
    let totalSteps = 0;
    let completedSteps = 0;
    data.map((session) => {
      totalSteps += session.stepsNumber;
      session.steps.items.map((step) => {
        if (step.status === "completed") completedSteps += 1;
        return step;
      });
      return session;
    });
    return (completedSteps / totalSteps) * 100;
  }
}

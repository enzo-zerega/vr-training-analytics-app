export default function calcSteps(data) {
  let steps = data.stepsNumber;
  let completed = 0;
  let failed = 0;
  let started = 0;
  data.steps.items.map((step) => {
    if (step.status === "completed") completed += 1;
    else if (step.status === "failed") failed += 1;
    else if (step.status === "started") started += 1;
    return step;
  });
  return { steps, completed, failed, started };
}

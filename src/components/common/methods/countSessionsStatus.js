import { calcSteps, checkSessionStatus } from "./";

export default function countSessionsStatus(data, scope) {
  if (scope === "global") {
    let completedGuidedSessions = 0;
    let otherGuidedSesssions = 0;
    let completedEvaluationSessions = 0;
    let otherEvaluationSessions = 0;
    data.map((user) => {
      user.trainings.items.map((training) => {
        training.sessions.items.map((session) => {
          const stepsStatus = calcSteps(session);
          const sessionStatus = checkSessionStatus(stepsStatus);
          if (session.type.toLocaleLowerCase() === "guided") {
            if (sessionStatus.toLocaleLowerCase() === "completed") {
              completedGuidedSessions += 1;
            } else {
              otherGuidedSesssions += 1;
            }
          } else if (session.type.toLocaleLowerCase() === "evaluation") {
            if (sessionStatus.toLocaleLowerCase() === "completed") {
              completedEvaluationSessions += 1;
            } else {
              otherEvaluationSessions += 1;
            }
          }
          return session;
        });
        return training;
      });
      return user;
    });

    return {
      guided: {
        completed: completedGuidedSessions,
        other: otherGuidedSesssions,
      },
      evaluation: {
        completed: completedEvaluationSessions,
        other: otherEvaluationSessions,
      },
    };
  } else if (scope === "training") {
    let completedGuidedSessions = 0;
    let otherGuidedSesssions = 0;
    let completedEvaluationSessions = 0;
    let otherEvaluationSessions = 0;
    data.map((training) => {
      training.sessions.items.map((session) => {
        const stepsStatus = calcSteps(session);
        const sessionStatus = checkSessionStatus(stepsStatus);
        if (session.type.toLocaleLowerCase() === "guided") {
          if (sessionStatus.toLocaleLowerCase() === "completed") {
            completedGuidedSessions += 1;
          } else {
            otherGuidedSesssions += 1;
          }
        } else if (session.type.toLocaleLowerCase() === "evaluation") {
          if (sessionStatus.toLocaleLowerCase() === "completed") {
            completedEvaluationSessions += 1;
          } else {
            otherEvaluationSessions += 1;
          }
        }
        return session;
      });
      return training;
    });

    return {
      guided: {
        completed: completedGuidedSessions,
        other: otherGuidedSesssions,
      },
      evaluation: {
        completed: completedEvaluationSessions,
        other: otherEvaluationSessions,
      },
    };
  } else if (scope === "session") {
    let completedGuidedSessions = 0;
    let otherGuidedSesssions = 0;
    let completedEvaluationSessions = 0;
    let otherEvaluationSessions = 0;
    data.map((session) => {
      const stepsStatus = calcSteps(session);
      const sessionStatus = checkSessionStatus(stepsStatus);
      if (session.type.toLocaleLowerCase() === "guided") {
        if (sessionStatus.toLocaleLowerCase() === "completed") {
          completedGuidedSessions += 1;
        } else {
          otherGuidedSesssions += 1;
        }
      } else if (session.type.toLocaleLowerCase() === "evaluation") {
        if (sessionStatus.toLocaleLowerCase() === "completed") {
          completedEvaluationSessions += 1;
        } else {
          otherEvaluationSessions += 1;
        }
      }
      return session;
    });

    return {
      guided: {
        completed: completedGuidedSessions,
        other: otherGuidedSesssions,
      },
      evaluation: {
        completed: completedEvaluationSessions,
        other: otherEvaluationSessions,
      },
    };
  }
}

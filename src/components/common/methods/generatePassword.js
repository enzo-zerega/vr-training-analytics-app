function getRandomUpperCase() {
  return String.fromCharCode(Math.floor(Math.random() * 26) + 65);
}
function getRandomLowerCase() {
  return String.fromCharCode(Math.floor(Math.random() * 26) + 97);
}
function getRandomNumber() {
  return String.fromCharCode(Math.floor(Math.random() * 10) + 48);
}
function getRandomSymbol() {
  var symbol = "!@#$%^&*(){}[]=<>/,.|~?";
  return symbol[Math.floor(Math.random() * symbol.length)];
}

export default function generatePassword() {
  let password =
    getRandomUpperCase() +
    getRandomLowerCase() +
    getRandomLowerCase() +
    getRandomLowerCase() +
    getRandomLowerCase() +
    getRandomLowerCase() +
    getRandomNumber() +
    getRandomSymbol();
  password = password
    .split("")
    .sort(function () {
      return 0.5 - Math.random();
    })
    .join("");
  return password;
}

export default function calcMinutes(diff) {
  return Math.floor(diff / 60e3);
}

export default function spaceToHyphen(string) {
  let result = "";
  try {
    result = string.split(" ").join("-").toLowerCase();
  } catch (err) {
    console.log({ err });
  }

  return result;
}

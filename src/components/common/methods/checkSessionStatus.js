export default function checkSessionStatus(stepsStatus) {
  if (stepsStatus.steps === stepsStatus.completed) return "completed";
  else if (stepsStatus.failed >= 1) return "failed";
  else if (stepsStatus.completed >= 1 || stepsStatus.started >= 1)
    return "started";
  else return "not initiated";
}

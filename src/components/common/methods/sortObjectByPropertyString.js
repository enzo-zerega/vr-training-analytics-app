export default function sortObjectByPropertyString(obj, property) {
  obj.sort(function (a, b) {
    let nameA;
    let nameB;
    if (typeof a[property] && b[property] === "string") {
      nameA = a[property].toUpperCase(); // ignore upper and lowercase
      nameB = b[property].toUpperCase(); // ignore upper and lowercase
    } else {
      nameA = a[property];
      nameB = b[property];
    }

    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    return 0;
  });
  return obj;
}

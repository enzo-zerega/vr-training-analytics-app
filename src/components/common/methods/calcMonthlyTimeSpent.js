import { calcMinutes, reverseObjectByPropertyString } from "./";

const months = [
  "jan",
  "feb",
  "mar",
  "apr",
  "may",
  "jun",
  "jul",
  "aug",
  "sep",
  "oct",
  "nov",
  "dic",
];

function calcTimeSpent(data, scope) {
  if (scope === "global") {
    let totalTime = [];
    data.map((user) =>
      user.trainings.items.map((training) =>
        training.sessions.items.map((session) =>
          session.steps.items.map((step) => {
            const startedAt = new Date(step.startedAt);
            const finishedAt = new Date(step.finishedAt);
            if (
              totalTime.find(
                (element) => element.year === startedAt.getFullYear()
              )
            ) {
              if (
                totalTime
                  .find((element) => element.year === startedAt.getFullYear())
                  .months.find((el) => el.month === startedAt.getMonth())
              ) {
                totalTime
                  .find((element) => element.year === startedAt.getFullYear())
                  .months.find(
                    (el) => el.month === startedAt.getMonth()
                  ).minutes += calcMinutes(finishedAt - startedAt);
              } else {
                totalTime
                  .find((element) => element.year === startedAt.getFullYear())
                  .months.push({
                    month: startedAt.getMonth(),
                    minutes: calcMinutes(finishedAt - startedAt),
                  });
              }
            } else {
              totalTime.push({
                year: startedAt.getFullYear(),
                months: [
                  {
                    month: startedAt.getMonth(),
                    minutes: calcMinutes(finishedAt - startedAt),
                  },
                ],
              });
            }
            return step;
          })
        )
      )
    );

    reverseObjectByPropertyString(totalTime, "year");
    totalTime.map((year) =>
      reverseObjectByPropertyString(year.months, "month")
    );
    return totalTime;
  } else if (scope === "training") {
    let totalTime = [];
    data.map((training) =>
      training.sessions.items.map((session) =>
        session.steps.items.map((step) => {
          const startedAt = new Date(step.startedAt);
          const finishedAt = new Date(step.finishedAt);
          if (
            totalTime.find(
              (element) => element.year === startedAt.getFullYear()
            )
          ) {
            if (
              totalTime
                .find((element) => element.year === startedAt.getFullYear())
                .months.find((el) => el.month === startedAt.getMonth())
            ) {
              totalTime
                .find((element) => element.year === startedAt.getFullYear())
                .months.find(
                  (el) => el.month === startedAt.getMonth()
                ).minutes += calcMinutes(finishedAt - startedAt);
            } else {
              totalTime
                .find((element) => element.year === startedAt.getFullYear())
                .months.push({
                  month: startedAt.getMonth(),
                  minutes: calcMinutes(finishedAt - startedAt),
                });
            }
          } else {
            totalTime.push({
              year: startedAt.getFullYear(),
              months: [
                {
                  month: startedAt.getMonth(),
                  minutes: calcMinutes(finishedAt - startedAt),
                },
              ],
            });
          }
          return step;
        })
      )
    );

    reverseObjectByPropertyString(totalTime, "year");
    totalTime.map((year) =>
      reverseObjectByPropertyString(year.months, "month")
    );
    return totalTime;
  } else if (scope === "session") {
    let totalTime = [];
    data.map((session) =>
      session.steps.items.map((step) => {
        const startedAt = new Date(step.startedAt);
        const finishedAt = new Date(step.finishedAt);
        if (
          totalTime.find((element) => element.year === startedAt.getFullYear())
        ) {
          if (
            totalTime
              .find((element) => element.year === startedAt.getFullYear())
              .months.find((el) => el.month === startedAt.getMonth())
          ) {
            totalTime
              .find((element) => element.year === startedAt.getFullYear())
              .months.find(
                (el) => el.month === startedAt.getMonth()
              ).minutes += calcMinutes(finishedAt - startedAt);
          } else {
            totalTime
              .find((element) => element.year === startedAt.getFullYear())
              .months.push({
                month: startedAt.getMonth(),
                minutes: calcMinutes(finishedAt - startedAt),
              });
          }
        } else {
          totalTime.push({
            year: startedAt.getFullYear(),
            months: [
              {
                month: startedAt.getMonth(),
                minutes: calcMinutes(finishedAt - startedAt),
              },
            ],
          });
        }
        return step;
      })
    );

    reverseObjectByPropertyString(totalTime, "year");
    totalTime.map((year) =>
      reverseObjectByPropertyString(year.months, "month")
    );
    return totalTime;
  }
}

export default function calcMonthlyTimeSpent(user, scope) {
  if (scope === "global") {
    const time = calcTimeSpent(user, scope);
    if (time.length > 0) {
      const lastMonth = time[0].months[0].month;
      const lastYear = time[0].year;
      let firstMonth = 0;
      if (lastMonth < 6) {
        firstMonth = lastMonth + 6;
      } else {
        firstMonth = lastMonth - 6;
      }
      let arr = [];
      for (let i = 0; i < 7; i++) {
        if (firstMonth + i <= 11) {
          try {
            arr.push({
              year: lastYear,
              month: months[firstMonth + i],
              time: time
                .find((item) => item.year === lastYear)
                .months.find((el) => el.month === firstMonth + i).minutes,
            });
          } catch {
            arr.push({
              year: lastYear,
              month: months[firstMonth + i],
              time: 0,
            });
          }
        } else {
          for (let j = 0; j < 7 - i; j++) {
            try {
              arr.push({
                year: lastYear - 1,
                month: months[j],
                time: time
                  .find((item) => item.year === lastYear - 1)
                  .months.find((el) => el.month === j).minutes,
              });
            } catch {
              arr.push({
                year: lastYear - 1,
                month: months[j],
                time: 0,
              });
            }
          }
        }
      }
      return arr;
    } else return [{ year: "", month: "", time: 0 }];
  } else if (scope === "training") {
    const time = calcTimeSpent(user, scope);
    if (time.length > 0) {
      const lastMonth = time[0].months[0].month;
      const lastYear = time[0].year;
      let firstMonth = 0;
      if (lastMonth < 6) {
        firstMonth = lastMonth + 6;
      } else {
        firstMonth = lastMonth - 6;
      }
      let arr = [];
      for (let i = 0; i < 7; i++) {
        if (firstMonth + i <= 11) {
          try {
            arr.push({
              year: lastYear,
              month: months[firstMonth + i],
              time: time
                .find((item) => item.year === lastYear)
                .months.find((el) => el.month === firstMonth + i).minutes,
            });
          } catch {
            arr.push({
              year: lastYear,
              month: months[firstMonth + i],
              time: 0,
            });
          }
        } else {
          for (let j = 0; j < 7 - i; j++) {
            try {
              arr.push({
                year: lastYear - 1,
                month: months[j],
                time: time
                  .find((item) => item.year === lastYear - 1)
                  .months.find((el) => el.month === j).minutes,
              });
            } catch {
              arr.push({
                year: lastYear - 1,
                month: months[j],
                time: 0,
              });
            }
          }
        }
      }
      return arr;
    } else return [{ year: "", month: "", time: 0 }];
  } else if (scope === "session") {
    const time = calcTimeSpent(user, scope);
    if (time.length > 0) {
      const lastMonth = time[0].months[0].month;
      const lastYear = time[0].year;
      let firstMonth = 0;
      if (lastMonth < 6) {
        firstMonth = lastMonth + 6;
      } else {
        firstMonth = lastMonth - 6;
      }
      let arr = [];
      for (let i = 0; i < 7; i++) {
        if (firstMonth + i <= 11) {
          try {
            arr.push({
              year: lastYear,
              month: months[firstMonth + i],
              time: time
                .find((item) => item.year === lastYear)
                .months.find((el) => el.month === firstMonth + i).minutes,
            });
          } catch {
            arr.push({
              year: lastYear,
              month: months[firstMonth + i],
              time: 0,
            });
          }
        } else {
          for (let j = 0; j < 7 - i; j++) {
            try {
              arr.push({
                year: lastYear - 1,
                month: months[j],
                time: time
                  .find((item) => item.year === lastYear - 1)
                  .months.find((el) => el.month === j).minutes,
              });
            } catch {
              arr.push({
                year: lastYear - 1,
                month: months[j],
                time: 0,
              });
            }
          }
        }
      }
      return arr;
    } else return [{ year: "", month: "", time: 0 }];
  }
}

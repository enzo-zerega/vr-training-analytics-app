export default function capitalizeFirstChar(str) {
  if (typeof str === "string" && str.length !== 0)
    return (
      str.replace(/-/g, " ")[0].toUpperCase() + str.replace(/-/g, " ").slice(1)
    );
  else return str;
}

import { Button, withStyles } from "@material-ui/core";

const SessionButton = withStyles({
  root: {
    textTransform: "none",
    justifyContent: "flex-start",
  },
})(Button);

export default SessionButton;

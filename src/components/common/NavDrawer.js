import {
  Divider,
  Drawer,
  Hidden,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Avatar,
  Typography,
  Container,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { ViewList, Dashboard, ViewModule } from "@material-ui/icons";
import { useStyles } from "../style";
import { capitalizeFirstChar } from "./methods";
import { Link } from "react-router-dom";

function NavDrawer(props) {
  const {
    window,
    drawer,
    handleClose,
    user,
    organization,
    isUser,
    isOrgAdmin,
    isSuperAdmin,
  } = props;
  const classes = useStyles();
  const theme = useTheme();

  let drawerValuesPermanent;
  let drawerValuesTemporary;
  if (isUser) {
    const username = user.name;
    const avatar = user.organization.avatar;
    const organizationName = user.organization.name;
    const trainings = user.trainings.items.map((training) =>
      capitalizeFirstChar(training.details.name)
    );
    drawerValuesPermanent = (
      <div>
        <div className={classes.toolbar} />
        <Container className={classes.header}>
          <Avatar alt={username} src={avatar} className={classes.avatar} />
          <Typography variant="h6">{username}</Typography>
          <Typography variant="subtitle2">{organizationName}</Typography>
        </Container>
        <Divider />
        <ListItem component={Link} to="/" button>
          <ListItemIcon className={classes.listItemIcon}>
            <Dashboard />
          </ListItemIcon>
          <ListItemText primary="Overview" />
        </ListItem>
        <Divider />
        <ListItem component={Link} to="/trainings" button>
          <ListItemIcon className={classes.listItemIcon}>
            <ViewList />
          </ListItemIcon>
          <ListItemText primary="My trainings" />
        </ListItem>
        <Divider />
        <List className={classes.list}>
          {trainings.map((training, index) => (
            <ListItem
              button
              key={index}
              component={Link}
              to={`/trainings/${training.split(" ").join("-").toLowerCase()}`}
            >
              <ListItemText
                className={classes.listItemText}
                primary={training}
              />
            </ListItem>
          ))}
        </List>
      </div>
    );

    drawerValuesTemporary = (
      <div>
        <div className={classes.toolbar} />
        <Container className={classes.header}>
          <Avatar alt={username} src={avatar} className={classes.avatar} />
          <Typography variant="h6">{username}</Typography>
          <Typography variant="subtitle2">{organizationName}</Typography>
        </Container>
        <Divider />
        <ListItem component={Link} to="/" onClick={handleClose} button>
          <ListItemIcon className={classes.listItemIcon}>
            <Dashboard />
          </ListItemIcon>
          <ListItemText primary="Overview" />
        </ListItem>
        <Divider />
        <ListItem component={Link} to="/trainings" onClick={handleClose} button>
          <ListItemIcon className={classes.listItemIcon}>
            <ViewList />
          </ListItemIcon>
          <ListItemText primary="My trainings" />
        </ListItem>
        <Divider />
        <List className={classes.list} onClick={handleClose}>
          {trainings.map((training, index) => (
            <ListItem
              button
              key={index}
              component={Link}
              to={`/trainings/${training.split(" ").join("-").toLowerCase()}`}
            >
              <ListItemText
                className={classes.listItemText}
                primary={training}
              />
            </ListItem>
          ))}
        </List>
      </div>
    );
  } else if (isOrgAdmin) {
    const usersCount = organization ? organization.users.items.length : 0;
    const avatar = organization ? organization.avatar : "";
    drawerValuesPermanent = (
      <div>
        <div className={classes.toolbar} />
        <Container className={classes.header}>
          <Avatar
            alt={organization ? organization.name : ""}
            src={avatar}
            className={classes.avatar}
          />
          <Typography variant="h6">
            {organization ? organization.name : ""}
          </Typography>
          <Typography variant="subtitle2">
            {usersCount}
            {usersCount > 1 || usersCount === 0 ? " users" : " user"}
          </Typography>
        </Container>
        <Divider />
        <ListItem component={Link} to="/" button>
          <ListItemIcon className={classes.listItemIcon}>
            <ViewModule />
          </ListItemIcon>
          <ListItemText primary="Resources" />
        </ListItem>
        <Divider />
        <List className={classes.list}>
          <ListItem button component={Link} to={`/users`}>
            <ListItemText className={classes.listItemText} primary="Users" />
          </ListItem>
          <ListItem button component={Link} to={`/trainings`}>
            <ListItemText
              className={classes.listItemText}
              primary="Trainings"
            />
          </ListItem>
          <ListItem button component={Link} to={`/overview`}>
            <ListItemText className={classes.listItemText} primary="Overview" />
          </ListItem>
        </List>
      </div>
    );

    drawerValuesTemporary = (
      <div>
        <div className={classes.toolbar} />
        <Container className={classes.header}>
          <Avatar
            alt={organization ? organization.name : ""}
            src={avatar}
            className={classes.avatar}
          />
          <Typography variant="h6">
            {organization ? organization.name : ""}
          </Typography>
          <Typography variant="subtitle2">
            {usersCount}
            {usersCount > 1 || usersCount === 0 ? " users" : " user"}
          </Typography>
        </Container>
        <Divider />
        <ListItem component={Link} to="/" onClick={handleClose} button>
          <ListItemIcon className={classes.listItemIcon}>
            <ViewModule />
          </ListItemIcon>
          <ListItemText primary="Resources" />
        </ListItem>
        <Divider />
        <List className={classes.list} onClick={handleClose}>
          <ListItem button component={Link} to={`/users`}>
            <ListItemText className={classes.listItemText} primary="Users" />
          </ListItem>
          <ListItem button component={Link} to={`/trainings`}>
            <ListItemText
              className={classes.listItemText}
              primary="Trainings"
            />
          </ListItem>
          <ListItem button component={Link} to={`/overview`}>
            <ListItemText className={classes.listItemText} primary="Overview" />
          </ListItem>
        </List>
      </div>
    );
  } else if (isSuperAdmin) {
    drawerValuesPermanent = (
      <div>
        <div className={classes.toolbar} />
        <Container className={classes.header}>
          <Typography variant="h6">Super Admin</Typography>
        </Container>
        <Divider />
        <ListItem component={Link} to="/" button>
          <ListItemIcon className={classes.listItemIcon}>
            <ViewModule />
          </ListItemIcon>
          <ListItemText primary="Resources" />
        </ListItem>
        <Divider />
        <List className={classes.list}>
          <ListItem button component={Link} to={`/organizations`}>
            <ListItemText
              className={classes.listItemText}
              primary="Organizations"
            />
          </ListItem>
          <ListItem button component={Link} to={`/trainings`}>
            <ListItemText
              className={classes.listItemText}
              primary="Trainings"
            />
          </ListItem>
          <ListItem button component={Link} to={`/overview`}>
            <ListItemText className={classes.listItemText} primary="Overview" />
          </ListItem>
        </List>
      </div>
    );

    drawerValuesTemporary = (
      <div>
        <div className={classes.toolbar} />
        <Container className={classes.header}>
          <Typography variant="h6">Super Admin</Typography>
        </Container>
        <Divider />
        <ListItem component={Link} to="/" onClick={handleClose} button>
          <ListItemIcon className={classes.listItemIcon}>
            <ViewModule />
          </ListItemIcon>
          <ListItemText primary="Resources" />
        </ListItem>
        <Divider />
        <List className={classes.list} onClick={handleClose}>
          <ListItem button component={Link} to={`/organizations`}>
            <ListItemText
              className={classes.listItemText}
              primary="Organizations"
            />
          </ListItem>
          <ListItem button component={Link} to={`/trainings`}>
            <ListItemText
              className={classes.listItemText}
              primary="Trainings"
            />
          </ListItem>
          <ListItem button component={Link} to={`/overview`}>
            <ListItemText className={classes.listItemText} primary="Overview" />
          </ListItem>
        </List>
      </div>
    );
  }

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <nav className={classes.drawer} aria-label="mailbox folders">
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Hidden mdUp implementation="css">
        <Drawer
          container={container}
          variant="temporary"
          anchor={theme.direction === "rtl" ? "right" : "left"}
          open={drawer}
          onClose={handleClose}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {drawerValuesTemporary}
        </Drawer>
      </Hidden>
      <Hidden smDown implementation="css">
        <Drawer
          classes={{
            paper: classes.drawerPaper,
          }}
          variant="permanent"
          open
        >
          {drawerValuesPermanent}
        </Drawer>
      </Hidden>
    </nav>
  );
}

export default NavDrawer;

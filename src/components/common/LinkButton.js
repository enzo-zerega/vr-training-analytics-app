import { Button, withStyles } from "@material-ui/core";

const LinkButton = withStyles({
  root: {
    borderRadius: 50,
  },
})(Button);

export default LinkButton;

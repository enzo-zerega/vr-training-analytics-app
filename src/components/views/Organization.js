import { useEffect, useState, Fragment } from "react";
import { useStyles } from "../style";
import { Indicators, UsersCount } from "../common/indicators";
import { getOrganization, listSessions, listSessionsByOrgId } from "../data";
import { Typography, ListItemIcon, ListItemText } from "@material-ui/core";
import { Edit, ViewList } from "@material-ui/icons";
import { useParams, Link } from "react-router-dom";
import { LinkButton } from "../common";

export default function Organization(props) {
  const {
    updateTitle,
    organization: organizationFromProps,
    user: userFromProps,
    isSuperAdmin,
    isOrgAdmin,
    setOrganization: setOrganizationFromProps,
  } = props;
  const { organization: organizationFromParams } = useParams();
  const [globalIndicators, setGlobalIndicators] = useState(
    JSON.parse(organizationFromProps.globalIndicators)
  );
  const [sessions, setSessions] = useState(
    userFromProps.trainings.items[0].sessions.items
  );
  const [organization, setOrganization] = useState(organizationFromProps);
  const classes = useStyles();

  useEffect(() => {
    if (isOrgAdmin) {
      updateTitle("Overview");
      listSessions().then((res) => {
        setSessions(res.data.listSessions.items);
      });
    } else if (isSuperAdmin) {
      getOrganization(organizationFromParams).then((res) => {
        setOrganization(res.data.getOrganization);
        updateTitle(res.data.getOrganization.name);
        setGlobalIndicators(
          JSON.parse(res.data.getOrganization.globalIndicators)
        );
      });
      listSessionsByOrgId(organizationFromParams).then((res) => {
        setSessions(res.data.listSessions.items);
      });
    }
  }, [updateTitle, isOrgAdmin, organizationFromParams, isSuperAdmin]);

  const organizationTrainings = organization.trainings.items
    ? organization.trainings.items.length
    : 0;

  const handleClick = () => {
    if (isSuperAdmin) setOrganizationFromProps(organization);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Typography
        variant="h6"
        className={classes.trainingTitle}
        color="secondary"
      >
        Organization dashboard
      </Typography>
      <div className={classes.topButtons}>
        <UsersCount
          users={
            isOrgAdmin
              ? organizationFromProps.users.items
              : organization.users.items
          }
          disabled={isSuperAdmin ? true : false}
          global={true}
        />
        {isSuperAdmin && (
          <Fragment>
            <LinkButton
              variant="contained"
              color="secondary"
              component={Link}
              onClick={handleClick}
              to={{
                pathname: `/organizations/${organizationFromParams}/trainings`,
                organization,
              }}
            >
              <ListItemIcon className={classes.listItemIcon}>
                <ViewList color="primary" />
              </ListItemIcon>
              <ListItemText
                className={classes.myTrainings}
                primary={
                  organizationTrainings !== 1
                    ? `${organizationTrainings} trainings`
                    : `${organizationTrainings} training`
                }
              />
            </LinkButton>
            <LinkButton
              variant="contained"
              color="secondary"
              component={Link}
              onClick={handleClick}
              to={`/organizations/${organizationFromParams}/edit`}
            >
              <ListItemIcon className={classes.listItemIcon}>
                <Edit color="primary" />
              </ListItemIcon>
              <ListItemText
                className={classes.myTrainings}
                primary="Edit organization"
              />
            </LinkButton>
          </Fragment>
        )}
      </div>
      <div className={classes.dashboard}>
        <div className={classes.dashboardItems}>
          <Indicators
            items={globalIndicators}
            data={sessions}
            scope="session"
          />
        </div>
      </div>
    </main>
  );
}

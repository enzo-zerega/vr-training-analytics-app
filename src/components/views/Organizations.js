import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Chip,
  ListItemText,
  ListItemIcon,
  Typography,
} from "@material-ui/core";
import { OpenInNew, Add } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { listOrganizations, getInitialOrganization } from "../data";
import { useStyles } from "../style";
import { Link } from "react-router-dom";
import { LinkButton } from "../common";
import uniqueId from "lodash/uniqueId";

export default function Organizations(props) {
  const { updateTitle } = props;
  const classes = useStyles();
  const [organizations, setOrganizations] = useState({
    items: [getInitialOrganization()],
  });

  useEffect(() => {
    updateTitle("Organizations");
    listOrganizations().then((res) => {
      setOrganizations(res.data.listOrganizations);
    });
  }, [updateTitle]);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.dashboard}>
        <LinkButton
          variant="contained"
          color="secondary"
          component={Link}
          to="/sign-up/organization"
        >
          <ListItemIcon className={classes.listItemIcon}>
            <Add color="primary" />
          </ListItemIcon>
          <ListItemText
            className={classes.myTrainings}
            primary="Add organization"
          />
        </LinkButton>
      </div>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow className={classes.usersTitle}>
              <TableCell className={classes.usersTitle}>Name</TableCell>
              <TableCell className={classes.usersTitle} align="center">
                Trainings
              </TableCell>
              <TableCell className={classes.usersTitle} align="center">
                Users
              </TableCell>
              <TableCell className={classes.usersTitle} align="right">
                Link
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {organizations.items.length ? (
              organizations.items.map((organization) => (
                <TableRow key={uniqueId("users-")}>
                  <TableCell>{organization.name}</TableCell>
                  <TableCell align="center">
                    <div className={classes.trainingChips}>
                      {organization.trainings.items.map((training) => (
                        <Chip
                          key={uniqueId("organizations-trainings-")}
                          className={classes.trainingChip}
                          label={training.name}
                        />
                      ))}
                    </div>
                  </TableCell>
                  <TableCell align="center">
                    {organization.users.items.length}
                  </TableCell>
                  <TableCell align="right">
                    <Link to={`/organizations/${organization.id}`}>
                      <OpenInNew
                        fontSize="small"
                        className={classes.linkToUser}
                      />
                    </Link>
                  </TableCell>
                </TableRow>
              ))
            ) : (
              <Typography className={classes.tableItem}>
                No organizations avaliable
              </Typography>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </main>
  );
}

import { useStyles } from "../style";
import { Paper, Typography, Container } from "@material-ui/core";
import name from "../../assets/img/name_white.png";
import { Auth } from "../auth";

export default function Home(props) {
  const classes = useStyles();
  return (
    <main className={classes.gridBackground}>
      <Typography component="h1" variant="h6" className={classes.signInTitle}>
        VR Training Metrics
      </Typography>
      <Container component="div" maxWidth="xs">
        <Paper>
          <Auth {...props} />
        </Paper>
      </Container>
      <div className={classes.signInFooter} />
      <img
        src={name}
        alt="gleechi"
        width="150"
        height="auto"
        className={classes.signInImg}
      />
    </main>
  );
}

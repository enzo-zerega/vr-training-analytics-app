import { Typography, Divider, ListItemText } from "@material-ui/core";
import { Link, useParams } from "react-router-dom";
import { useStyles } from "../style";
import {
  spaceToHyphen,
  calcProgress,
  capitalizeFirstChar,
} from "../common/methods";
import { SessionButton, ProgressCircle, SessionCounter } from "../common";
import { useEffect, useState } from "react";
import { CheckCircleOutline } from "@material-ui/icons";

export default function Sessions(props) {
  const { user: userFromProps, isUser, isOrgAdmin, updateTitle } = props;
  const { training: trainingFromParams, user: userFromParams } = useParams();
  const classes = useStyles();
  const sessions = userFromProps.trainings.items.find(
    (training) => spaceToHyphen(training.details.name) === trainingFromParams
  ).sessions.items;
  const [guidedSessions, setGuidedSessions] = useState(0);
  const [evaluationSessions, setEvaluationSessions] = useState(0);

  useEffect(() => {
    if (isUser) {
      updateTitle("Sessions");
    } else if (userFromParams) {
      updateTitle(userFromProps.name);
    }
    setGuidedSessions(
      sessions.filter((session) => session.type === "guided").length
    );
    setEvaluationSessions(
      sessions.filter((session) => session.type === "evaluation").length
    );
  }, [sessions, updateTitle, isUser, userFromParams, userFromProps.name]);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Typography
        variant="h6"
        className={classes.trainingTitle}
        color="secondary"
      >
        {isOrgAdmin && capitalizeFirstChar(trainingFromParams)} Training
        Sessions
      </Typography>
      <div className={classes.sessionHeading}>
        <Typography variant="subtitle1" color="secondary">
          Guided sessions
        </Typography>
        <Divider className={classes.sessionDivider} light />
        <SessionCounter count={guidedSessions} />
      </div>
      {guidedSessions ? (
        sessions
          .slice(0)
          .reverse()
          .map(
            (session, index) =>
              session.type === "guided" && (
                <SessionButton
                  key={index}
                  className={classes.sessionBox}
                  variant="contained"
                  color="primary"
                  component={Link}
                  to={
                    isUser
                      ? `/trainings/${spaceToHyphen(
                          trainingFromParams
                        )}/sessions/guided/${spaceToHyphen(session.name)}`
                      : `/users/${userFromParams}/trainings/${spaceToHyphen(
                          trainingFromParams
                        )}/sessions/guided/${spaceToHyphen(session.name)}`
                  }
                >
                  <ListItemText
                    className={classes.mySessions}
                    primary={session.name}
                  />
                  {calcProgress([session], "session") === 100 ? (
                    <CheckCircleOutline
                      fontSize="large"
                      style={{
                        color: "#A5D6A7",
                        transform: "scale(1.3)",
                        margin: "0.6rem",
                      }}
                    />
                  ) : (
                    <ProgressCircle
                      value={calcProgress([session], "session")}
                    />
                  )}
                </SessionButton>
              )
          )
      ) : (
        <Typography className={classes.counterZero}>
          No guided sessions available
        </Typography>
      )}
      <div className={classes.sessionHeading}>
        <Typography variant="subtitle1" color="secondary">
          Evaluation sessions
        </Typography>
        <Divider className={classes.sessionDivider} light />
        <SessionCounter count={evaluationSessions} />
      </div>
      {evaluationSessions ? (
        sessions
          .slice(0)
          .reverse()
          .map(
            (session, index) =>
              session.type === "evaluation" && (
                <SessionButton
                  key={index}
                  className={classes.sessionBox}
                  variant="contained"
                  color="primary"
                  component={Link}
                  to={
                    isUser
                      ? `/trainings/${spaceToHyphen(
                          trainingFromParams
                        )}/sessions/evaluations/${spaceToHyphen(session.name)}`
                      : `/users/${userFromParams}/trainings/${spaceToHyphen(
                          trainingFromParams
                        )}/sessions/evaluations/${spaceToHyphen(session.name)}`
                  }
                >
                  <ListItemText
                    className={classes.mySessions}
                    primary={session.name}
                  />
                  {calcProgress([session], "session") === 100 ? (
                    <CheckCircleOutline
                      fontSize="large"
                      style={{
                        color: "#A5D6A7",
                        transform: "scale(1.3)",
                        margin: "0.65rem",
                      }}
                    />
                  ) : (
                    <ProgressCircle
                      value={calcProgress([session], "session")}
                    />
                  )}
                </SessionButton>
              )
          )
      ) : (
        <Typography className={classes.counterZero}>
          No evaluation sessions available
        </Typography>
      )}
    </main>
  );
}

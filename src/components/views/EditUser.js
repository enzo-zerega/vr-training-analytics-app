import { useStyles } from "../style";
import {
  Typography,
  Grid,
  TextField,
  Button,
  Chip,
  Snackbar,
} from "@material-ui/core";
import { Cancel, Add, Delete, Error } from "@material-ui/icons";
import { useParams } from "react-router-dom";
import AWS from "aws-sdk";
import awsmobile from "../../aws-exports";
import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import uniqueId from "lodash/uniqueId";
import { createTraining, deleteTraining, updateUser } from "../data";
import { Alert as MuiAlert } from "@material-ui/lab";

export default function EditUser(props) {
  const {
    updateTitle,
    user: userFromProps,
    organization: organizationFromProps,
  } = props;
  const classes = useStyles();
  const { user: userFromParams } = useParams();
  const [email, setEmail] = useState("");
  const [orgTrainings, setOrgTrainings] = useState(
    organizationFromProps.trainings.items
  );
  const [trainings, setTrainings] = useState(userFromProps.trainings.items);
  const [availableTrainings, setAvailableTrainings] = useState(
    organizationFromProps.trainings.items
  );
  const [signUpErrors, setSignUpErrors] = useState([]);

  const { register, handleSubmit, errors } = useForm({
    defaultValues: {
      name: userFromProps.name,
    },
  });

  const { REACT_APP_IDENTITY_POOL_ID } = process.env;

  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: REACT_APP_IDENTITY_POOL_ID,
  });

  AWS.config.region = awsmobile.aws_cognito_region;
  const poolId = awsmobile.aws_user_pools_id;

  async function getUser(Username, UserPoolId) {
    try {
      const cognito = new AWS.CognitoIdentityServiceProvider();
      await cognito
        .adminGetUser({ Username, UserPoolId })
        .promise()
        .then((res) => {
          const userEmail = res.UserAttributes.find(
            (attribute) => attribute.Name === "email"
          ).Value;
          setEmail(userEmail);
        });
    } catch (err) {
      setSignUpErrors([
        ...signUpErrors,
        { type: err.code, message: err.message },
      ]);
    }
  }

  useEffect(() => {
    updateTitle("Edit user");
    getUser(userFromParams, poolId);
    setTrainings(userFromProps.trainings.items);
    setOrgTrainings(organizationFromProps.trainings.items);
    let arr = [];
    availableTrainings.map((training) => {
      const isTraining = trainings.find(
        (userTraining) => userTraining.details.id === training.id
      );
      if (!isTraining) arr.push(training);
      return training;
    });
    setAvailableTrainings(arr);
    // DON'T ADD availableTrainings IN THE DEPENDENCY ARRAY
  }, [
    poolId,
    trainings,
    updateTitle,
    userFromParams,
    userFromProps.trainings.items,
    organizationFromProps.trainings.items,
  ]);

  const handleDelete = (id) => {
    const filteredTrainings = trainings.filter(
      (training) => training.details.id !== id
    );
    const selectedTraining = orgTrainings.filter(
      (training) => training.id === id
    );
    setAvailableTrainings([...availableTrainings, selectedTraining[0]]);
    setTrainings(filteredTrainings);
  };

  const handleAdd = (id) => {
    const selectedTraining = orgTrainings.filter(
      (training) => training.id === id
    );
    const filteredTrainings = availableTrainings.filter(
      (training) => training.id !== id
    );

    setAvailableTrainings(filteredTrainings);
    setTrainings([...trainings, { details: selectedTraining[0] }]);
  };

  async function onSubmit(validatedData) {
    const { name } = validatedData;
    if (userFromProps.name !== name.trim())
      updateUser({ id: userFromProps.id, name });
    trainings.map((training) => {
      const isTraining = userFromProps.trainings.items.find(
        (item) => item.details.id === training.details.id
      );
      if (!isTraining)
        createTraining({
          userId: userFromProps.id,
          organizationId: organizationFromProps.id,
          metricCards: "{}",
          organizationTrainingId: training.details.id,
        }).catch((err) =>
          err.errors.map((error) =>
            setSignUpErrors([...signUpErrors, { message: error.message }])
          )
        );
      return training;
    });
    userFromProps.trainings.items.map((training) => {
      const isTraining = trainings.find(
        (item) => item.details.id === training.details.id
      );
      if (!isTraining) {
        deleteTraining({ id: training.id }).catch((err) =>
          err.errors.map((error) =>
            setSignUpErrors([...signUpErrors, { message: error.message }])
          )
        );
      }
      return training;
    });
  }

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSignUpErrors([]);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.dashboard}>
        <div className={classes.signUp}>
          <form
            className={classes.signUpForm}
            noValidate
            onSubmit={handleSubmit((data) => onSubmit(data))}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  autoComplete="email"
                  name="email"
                  variant="outlined"
                  fullWidth
                  id="email"
                  label="Email"
                  value={email}
                  InputLabelProps={{ shrink: true }}
                  autoFocus
                  disabled
                />
              </Grid>
              <Grid item xs={12}>
                {!errors.name ? (
                  <TextField
                    variant="outlined"
                    color="secondary"
                    fullWidth
                    name="name"
                    label="Name *"
                    type="text"
                    id="name"
                    autoComplete="name"
                    inputRef={register({
                      required: "The first name field can't be empty",
                      maxLength: {
                        value: 50,
                        message: "The name cannot have more than 50 characters",
                      },
                    })}
                  />
                ) : (
                  <TextField
                    variant="outlined"
                    fullWidth
                    name="name"
                    label="Name *"
                    type="text"
                    id="name"
                    autoComplete="name"
                    error
                    helperText={errors.name.message}
                    FormHelperTextProps={{
                      className: classes.signInHelperText,
                    }}
                  />
                )}
              </Grid>
            </Grid>
            <Typography className={classes.signUpSubtitle}>
              Trainings:
            </Typography>
            <div className={classes.trainingChipsRegister}>
              {trainings.length ? (
                trainings.map((training) => (
                  <Chip
                    className={classes.trainingChipRegister}
                    label={`${training.details.name}`}
                    onDelete={() => {
                      handleDelete(training.details.id);
                    }}
                    deleteIcon={<Cancel />}
                    key={uniqueId("edit-")}
                  />
                ))
              ) : (
                <Typography className={classes.counterZero}>
                  Not enrrolled to any trainings yet.
                </Typography>
              )}
            </div>
            <Typography className={classes.signUpSubtitle}>
              Enrroll to new training:
            </Typography>
            <div className={classes.trainingChipsRegister}>
              {availableTrainings.length ? (
                availableTrainings.map((orgTraining) => (
                  <Chip
                    className={classes.trainingChipUnregister}
                    label={`${orgTraining.name}`}
                    onDelete={() => {
                      handleAdd(orgTraining.id);
                    }}
                    deleteIcon={<Add />}
                    key={uniqueId("edit-")}
                  />
                ))
              ) : (
                <Typography className={classes.counterZero}>
                  No more trainings available.
                </Typography>
              )}
            </div>
            <Button
              type="submit"
              variant="contained"
              color="secondary"
              className={classes.signInSubmit}
            >
              Save changes
            </Button>
          </form>
          <Typography className={classes.disableText}>
            <Error />
            Disable user
          </Typography>
          <Typography className={classes.deleteText}>
            <Delete />
            Delete user
          </Typography>
        </div>
        {signUpErrors &&
          signUpErrors.map((error) => (
            <Snackbar
              open={true}
              autoHideDuration={6000}
              onClose={handleClose}
              key={uniqueId("sign-in-")}
            >
              <Alert onClose={handleClose} severity="error">
                {error.message}
              </Alert>
            </Snackbar>
          ))}
      </div>
    </main>
  );
}

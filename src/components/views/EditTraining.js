import { useStyles } from "../style";
import {
  Button,
  Grid,
  TextField,
  Typography,
  Snackbar,
  FormControl,
  Select,
  InputLabel,
  Chip,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@material-ui/core";
import { Cancel, Add, Delete, Error } from "@material-ui/icons";
import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Alert as MuiAlert } from "@material-ui/lab";
import uniqueId from "lodash/uniqueId";
import { useParams } from "react-router-dom";
import { spaceToHyphen } from "../common/methods";
import {
  listTrainingIndicators,
  listSessionIndicators,
  listMetricCards,
  updateOrganizationTraining,
} from "../data";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const availableTrainingIndicators = listTrainingIndicators();
const availableSessionIndicators = listSessionIndicators();
const chart = listMetricCards().find((card) => card.type === "chart");
const text = listMetricCards().find((card) => card.type === "text");
const number = listMetricCards().find((card) => card.type === "number");

export default function EditTraining(props) {
  const { organization: organizationFromProps } = props;
  const { training: trainingFromParams } = useParams();
  const [signUpErrors, setSignUpErrors] = useState([]);
  const [selectedOrganization, setSelectedOrganization] = useState("");
  const [chartCard, setChartCard] = useState(chart.cards[0]);
  const [textCard, setTextCard] = useState(text.cards[0]);
  const [numberCard, setNumberCard] = useState(number.cards[0]);
  const training = organizationFromProps.trainings.items.find(
    (training) => spaceToHyphen(training.name) === trainingFromParams
  );
  const [trainingIndicators, setTrainingIndicators] = useState([]);
  const [sessionIndicators, setSessionIndicators] = useState([]);
  const [selectedChart, setSelectedChart] = useState("");
  const [selectedText, setSelectedText] = useState("");
  const [selectedNumber, setSelectedNumber] = useState("");
  const [selectedTrainingIndicators, setSelectedTrainingIndicators] = useState(
    trainingIndicators
  );
  const [
    unselectedTrainingIndicators,
    setUnselectedTrainingIndicators,
  ] = useState([]);

  const [selectedSessionIndicators, setSelectedSessionIndicators] = useState(
    sessionIndicators
  );
  const [
    unselectedSessionIndicators,
    setUnselectedSessionIndicators,
  ] = useState([]);

  const classes = useStyles();
  const { register, handleSubmit, errors } = useForm({
    defaultValues: {
      name: training ? training.name : "",
    },
  });

  function populateFields(item) {
    let trainingIndicatorsArr = [];
    JSON.parse(item.trainingIndicators).map((indicator) =>
      trainingIndicators.push(indicator.type)
    );
    setTrainingIndicators(trainingIndicatorsArr);
    setUnselectedTrainingIndicators(trainingIndicatorsArr);
    let sessionIndicatorsArr = [];
    JSON.parse(item.sessionIndicators).map((indicator) =>
      sessionIndicators.push(indicator.type)
    );
    setSessionIndicators(sessionIndicatorsArr);
    setUnselectedSessionIndicators(sessionIndicatorsArr);
    const selectedChartString = JSON.parse(item.metricCards).find(
      (card) => card.type === "chart"
    ).card;
    setSelectedChart(selectedChartString);
    const selectedTextString = JSON.parse(item.metricCards).find(
      (card) => card.type === "text"
    ).card;
    setSelectedText(selectedTextString);
    const selectedNumberString = JSON.parse(item.metricCards).find(
      (card) => card.type === "number"
    ).card;
    setSelectedNumber(selectedNumberString);
  }

  useEffect(() => {
    populateFields(training);

    let arr = [];
    availableTrainingIndicators.map((indicator) => {
      const isSelectedIndicator = selectedTrainingIndicators.find(
        (selectedIndicator) => selectedIndicator === indicator
      );
      if (!isSelectedIndicator) arr.push(indicator);
      return indicator;
    });
    setUnselectedTrainingIndicators(arr);
    let array = [];
    availableSessionIndicators.map((indicator) => {
      const isSelectedIndicator = selectedTrainingIndicators.find(
        (selectedIndicator) => selectedIndicator === indicator
      );
      if (!isSelectedIndicator) array.push(indicator);
      return indicator;
    });
    setUnselectedSessionIndicators(array);
  }, []);

  async function onSubmit(validatedData) {
    const { name } = validatedData;
    let isChanged = false;
    let input = { id: training.id, organizationId: organizationFromProps.id };
    if (name !== training.name) {
      input = { ...input, name };
      isChanged = true;
    }
    let changedTrainingIndicators = false;
    availableTrainingIndicators.map((indicator) => {
      const ind = selectedTrainingIndicators.find(
        (selected) => selected === indicator
      );
      if (!ind) changedTrainingIndicators = true;
      return indicator;
    });
    if (changedTrainingIndicators) {
      let trainingIndicatorsAwsjson = [];
      selectedTrainingIndicators.map((indicator) => {
        trainingIndicatorsAwsjson.push({ type: indicator });
        return indicator;
      });
      trainingIndicatorsAwsjson = JSON.stringify(trainingIndicatorsAwsjson);
      input = { ...input, trainingIndicators: trainingIndicatorsAwsjson };
      isChanged = true;
    }
    let changedSessionIndicators = false;
    availableSessionIndicators.map((indicator) => {
      const ind = selectedSessionIndicators.find(
        (selected) => selected === indicator
      );
      if (!ind) changedSessionIndicators = true;
      return indicator;
    });
    if (changedSessionIndicators) {
      let sessionIndicatorsAwsjson = [];
      selectedSessionIndicators.map((indicator) => {
        sessionIndicatorsAwsjson.push({ type: indicator });
        return indicator;
      });
      sessionIndicatorsAwsjson = JSON.stringify(sessionIndicatorsAwsjson);
      input = { ...input, sessionIndicators: sessionIndicatorsAwsjson };
      isChanged = true;
    }
    let changedMetricCards = false;
    let metricCardsAwsjson = [
      { type: "chart", card: selectedChart },
      { type: "text", card: selectedText },
      { type: "number", card: selectedNumber },
    ];
    metricCardsAwsjson = JSON.stringify(metricCardsAwsjson);
    if (
      selectedChart !==
      JSON.parse(training.metricCards).find((card) => card.type === "chart")
        .card
    ) {
      changedMetricCards = true;
      isChanged = true;
    }
    if (
      selectedText !==
      JSON.parse(training.metricCards).find((card) => card.type === "text").card
    ) {
      changedMetricCards = true;
      isChanged = true;
    }
    if (
      selectedNumber !==
      JSON.parse(training.metricCards).find((card) => card.type === "number")
        .card
    ) {
      changedMetricCards = true;
      isChanged = true;
    }
    if (changedMetricCards) {
      input = { ...input, metricCards: metricCardsAwsjson };
    }
    if (isChanged) {
      try {
        updateOrganizationTraining(input);
      } catch (err) {
        setSignUpErrors([
          ...signUpErrors,
          { type: err.code, message: err.message },
        ]);
      }
    } else {
      setSignUpErrors([...signUpErrors, { message: "No changes were made" }]);
    }
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSignUpErrors([]);
  };

  const handleChange = (event) => {
    const value = event.target.value;
    setSelectedOrganization(value);
  };

  const handleChartChange = (event) => {
    setChartCard(event.target.value);
    setSelectedChart(event.target.value);
  };

  const handleTextChange = (event) => {
    setTextCard(event.target.value);
    setSelectedText(event.target.value);
  };

  const handleNumberChange = (event) => {
    setNumberCard(event.target.value);
    setSelectedNumber(event.target.value);
  };

  const handleDeleteTrainingIndicator = (item) => {
    const filteredIndicators = selectedTrainingIndicators.filter(
      (indicator) => indicator !== item
    );
    const selectedIndicator = availableTrainingIndicators.filter(
      (indicator) => indicator === item
    );
    setUnselectedTrainingIndicators([
      ...unselectedTrainingIndicators,
      selectedIndicator[0],
    ]);
    setSelectedTrainingIndicators(filteredIndicators);
  };

  const handleAddTrainingIndicator = (item) => {
    const selectedIndicator = availableTrainingIndicators.filter(
      (indicator) => indicator === item
    );
    const filteredIndicators = unselectedTrainingIndicators.filter(
      (indicator) => indicator !== item
    );

    setUnselectedTrainingIndicators(filteredIndicators);
    setSelectedTrainingIndicators([
      ...selectedTrainingIndicators,
      selectedIndicator[0],
    ]);
  };

  const handleDeleteSessionIndicator = (item) => {
    const filteredIndicators = selectedSessionIndicators.filter(
      (indicator) => indicator !== item
    );
    const selectedIndicator = availableSessionIndicators.filter(
      (indicator) => indicator === item
    );
    setUnselectedSessionIndicators([
      ...unselectedSessionIndicators,
      selectedIndicator[0],
    ]);
    setSelectedSessionIndicators(filteredIndicators);
  };

  const handleAddSessionIndicator = (item) => {
    const selectedIndicator = availableSessionIndicators.filter(
      (indicator) => indicator === item
    );
    const filteredIndicators = unselectedSessionIndicators.filter(
      (indicator) => indicator !== item
    );

    setUnselectedSessionIndicators(filteredIndicators);
    setSelectedSessionIndicators([
      ...selectedSessionIndicators,
      selectedIndicator[0],
    ]);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.dashboard}>
        <div className={classes.addTraining}>
          <Typography
            className={classes.signUpTitle}
            component="h1"
            variant="h6"
          >
            Edit training
          </Typography>
          <form
            className={classes.signUpForm}
            noValidate
            onSubmit={handleSubmit((data) => onSubmit(data))}
          >
            <Grid container spacing={4}>
              <Grid item xs={12}>
                {!errors.name ? (
                  <TextField
                    autoComplete="name"
                    color="secondary"
                    inputRef={register({
                      required: "The name field can't be empty",
                      maxLength: {
                        value: 50,
                        message: "The name cannot have more than 50 characters",
                      },
                    })}
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Training name"
                    autoFocus
                  />
                ) : (
                  <TextField
                    autoComplete="name"
                    error
                    helperText={errors.name.message}
                    FormHelperTextProps={{
                      className: classes.signInHelperText,
                    }}
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Training name"
                    autoFocus
                  />
                )}
              </Grid>
              <Grid item xs={12}>
                <FormControl variant="outlined" fullWidth required disabled>
                  <InputLabel
                    htmlFor="outlined-organization-native-simple"
                    color="secondary"
                  >
                    Organization
                  </InputLabel>
                  <Select
                    native
                    color="secondary"
                    value={selectedOrganization}
                    onChange={handleChange}
                    label="Organization"
                    inputProps={{
                      name: "organization",
                      id: "outlined-organization-native-simple",
                    }}
                  >
                    <option value={organizationFromProps.name} selected>
                      {organizationFromProps.name}
                    </option>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <Typography className={classes.signUpSubtitle}>
              Training indicators
            </Typography>
            <div className={classes.trainingChipsRegister}>
              {selectedTrainingIndicators.map((indicator) => (
                <Chip
                  className={classes.trainingChipRegister}
                  label={`${indicator}`}
                  onDelete={() => {
                    handleDeleteTrainingIndicator(indicator);
                  }}
                  deleteIcon={<Cancel />}
                  key={uniqueId("training-indicators-")}
                />
              ))}
              {availableTrainingIndicators.length ? (
                unselectedTrainingIndicators.map((indicator) => (
                  <Chip
                    className={classes.trainingChipUnregister}
                    label={`${indicator}`}
                    onDelete={() => {
                      handleAddTrainingIndicator(indicator);
                    }}
                    deleteIcon={<Add />}
                    key={uniqueId("training-indicators-")}
                  />
                ))
              ) : (
                <Typography className={classes.counterZero}>
                  No training indicators available.
                </Typography>
              )}
            </div>
            <Typography className={classes.signUpSubtitle}>
              Session indicators
            </Typography>
            <div className={classes.trainingChipsRegister}>
              {selectedSessionIndicators.map((indicator) => (
                <Chip
                  className={classes.trainingChipRegister}
                  label={`${indicator}`}
                  onDelete={() => {
                    handleDeleteSessionIndicator(indicator);
                  }}
                  deleteIcon={<Cancel />}
                  key={uniqueId("session-indicators-")}
                />
              ))}
              {availableSessionIndicators.length ? (
                unselectedSessionIndicators.map((indicator) => (
                  <Chip
                    className={classes.trainingChipUnregister}
                    label={`${indicator}`}
                    onDelete={() => {
                      handleAddSessionIndicator(indicator);
                    }}
                    deleteIcon={<Add />}
                    key={uniqueId("session-indicators-")}
                  />
                ))
              ) : (
                <Typography className={classes.counterZero}>
                  No session indicators available.
                </Typography>
              )}
            </div>
            <Typography className={classes.signUpSubtitle}>
              Metric cards
            </Typography>
            <FormControl
              component="fieldset"
              fullWidth
              color="secondary"
              className={classes.signUpSubtitle}
            >
              <FormLabel component="legend">Chart</FormLabel>
              <RadioGroup
                aria-label="chart"
                name="chart"
                value={chartCard}
                onChange={handleChartChange}
              >
                {chart.cards.map((card) => (
                  <FormControlLabel
                    value={card}
                    control={<Radio />}
                    label={card}
                    checked={selectedChart === card}
                    key={uniqueId("cart-")}
                  />
                ))}
              </RadioGroup>
            </FormControl>
            <FormControl
              component="fieldset"
              fullWidth
              color="secondary"
              className={classes.signUpSubtitle}
            >
              <FormLabel component="legend">Text</FormLabel>
              <RadioGroup
                aria-label="text"
                name="text"
                value={textCard}
                onChange={handleTextChange}
              >
                {text.cards.map((card) => (
                  <FormControlLabel
                    value={card}
                    control={<Radio />}
                    label={card}
                    checked={selectedText === card}
                    key={uniqueId("text-")}
                  />
                ))}
              </RadioGroup>
            </FormControl>
            <FormControl
              component="fieldset"
              fullWidth
              color="secondary"
              className={classes.signUpSubtitle}
            >
              <FormLabel component="legend">Number</FormLabel>
              <RadioGroup
                aria-label="number"
                name="number"
                value={numberCard}
                onChange={handleNumberChange}
              >
                {number.cards.map((card) => (
                  <FormControlLabel
                    value={card}
                    control={<Radio />}
                    label={card}
                    checked={selectedNumber === card}
                    key={uniqueId("number-")}
                  />
                ))}
              </RadioGroup>
            </FormControl>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.signUpSubmit}
            >
              Save changes
            </Button>
          </form>
          <Typography className={classes.disableText}>
            <Error />
            Disable training
          </Typography>
          <Typography className={classes.deleteText}>
            <Delete />
            Delete training
          </Typography>
        </div>
        {signUpErrors &&
          signUpErrors.map((error) => (
            <Snackbar
              open={true}
              autoHideDuration={6000}
              onClose={handleClose}
              key={uniqueId("sign-in-")}
            >
              <Alert onClose={handleClose} severity="error">
                {error.message}
              </Alert>
            </Snackbar>
          ))}
      </div>
    </main>
  );
}

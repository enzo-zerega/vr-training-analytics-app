import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Chip,
  ListItemText,
  ListItemIcon,
  Typography,
} from "@material-ui/core";
import { OpenInNew, Add } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { getUsers, getUsersByTraining, getInitialUser } from "../data";
import { useStyles } from "../style";
import { Link, useParams } from "react-router-dom";
import { capitalizeFirstChar, spaceToHyphen } from "../common/methods";
import { LinkButton } from "../common";
import uniqueId from "lodash/uniqueId";

export default function Users(props) {
  const { updateTitle, organization: organizationFromProps } = props;
  const { training: trainingFromParams } = useParams();
  const [users, setUsers] = useState([getInitialUser()]);
  const classes = useStyles();

  useEffect(() => {
    if (!trainingFromParams) {
      updateTitle("Users");
      getUsers(organizationFromProps.id).then((res) =>
        setUsers(res.data.listUsers.items)
      );
    } else {
      updateTitle(`${capitalizeFirstChar(trainingFromParams)} users`);
      const trainingId = organizationFromProps.trainings.items.find(
        (training) => spaceToHyphen(training.name) === trainingFromParams
      ).id;
      getUsersByTraining(trainingId).then((res) => {
        setUsers(res);
      });
    }
  }, [
    trainingFromParams,
    updateTitle,
    organizationFromProps.id,
    organizationFromProps.trainings.items,
  ]);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.dashboard}>
        <LinkButton
          variant="contained"
          color="secondary"
          component={Link}
          to="/sign-up"
        >
          <ListItemIcon className={classes.listItemIcon}>
            <Add color="primary" />
          </ListItemIcon>
          <ListItemText className={classes.myTrainings} primary="Add user" />
        </LinkButton>
      </div>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow className={classes.usersTitle}>
              <TableCell className={classes.usersTitle}>Name</TableCell>
              <TableCell className={classes.usersTitle} align="center">
                Trainings
              </TableCell>
              <TableCell className={classes.usersTitle} align="right">
                Link
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.length ? (
              users.map((user) => (
                <TableRow key={uniqueId("users-")}>
                  <TableCell component="th" scope="row">
                    {user.name}
                  </TableCell>
                  <TableCell align="center">
                    <div className={classes.trainingChips}>
                      {user.trainings.items.map((training) => (
                        <Chip
                          key={uniqueId("users-trainings-")}
                          className={classes.trainingChip}
                          label={training.details.name}
                        />
                      ))}
                    </div>
                  </TableCell>
                  <TableCell align="right">
                    <Link to={`/users/${user.id}`}>
                      <OpenInNew
                        fontSize="small"
                        className={classes.linkToUser}
                      />
                    </Link>
                  </TableCell>
                </TableRow>
              ))
            ) : (
              <Typography className={classes.tableItem}>
                No users avaliable
              </Typography>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </main>
  );
}

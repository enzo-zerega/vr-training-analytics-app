import { useEffect, useState } from "react";
import { useStyles } from "../style";
import { Indicators, UsersCount } from "../common/indicators";
import { listOrganizationTrainings, listSessions, listUsers } from "../data";
import { ListItemIcon, ListItemText } from "@material-ui/core";
import { ViewList } from "@material-ui/icons";
import { Link } from "react-router-dom";
import { LinkButton } from "../common";

const globalIndicators = [
  { type: "progress" },
  { type: "timespent" },
  { type: "monthlytimespent" },
  { type: "sessionstatus" },
];

export default function Overview(props) {
  const {
    updateTitle,
    organization: organizationFromProps,
    user: userFromProps,
    isSuperAdmin,
  } = props;

  const [sessions, setSessions] = useState(
    userFromProps.trainings.items[0].sessions.items
  );
  const [trainings, setTrainings] = useState(
    organizationFromProps.trainings.items
  );
  const [users, setUsers] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    updateTitle("Overview");
    listSessions().then((res) => {
      setSessions(res.data.listSessions.items);
    });
    listOrganizationTrainings().then((res) => {
      setTrainings(res.data.listOrganizationTrainings.items);
    });
    listUsers().then((res) => {
      setUsers(res.data.listUsers.items);
    });
  }, [updateTitle]);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.topButtons}>
        <UsersCount
          users={users}
          global={true}
          disabled={isSuperAdmin ? true : false}
        />
        {isSuperAdmin && (
          <LinkButton
            variant="contained"
            color="secondary"
            component={Link}
            to={`/trainings`}
          >
            <ListItemIcon className={classes.listItemIcon}>
              <ViewList color="primary" />
            </ListItemIcon>
            <ListItemText
              className={classes.myTrainings}
              primary={
                trainings.length !== 1
                  ? `${trainings.length} trainings`
                  : `${trainings.length} training`
              }
            />
          </LinkButton>
        )}
      </div>
      <div className={classes.dashboard}>
        <div className={classes.dashboardItems}>
          <Indicators
            items={globalIndicators}
            data={sessions}
            scope="session"
          />
        </div>
      </div>
    </main>
  );
}

import { ListItemText } from "@material-ui/core";
import { Group, ViewList, Equalizer, Business } from "@material-ui/icons";
import { SessionButton } from "../common";
import { Link } from "react-router-dom";
import { useStyles } from "../style";
import { useEffect, Fragment } from "react";

export default function Resources(props) {
  const classes = useStyles();
  const { updateTitle, isOrgAdmin, isSuperAdmin } = props;

  useEffect(() => {
    updateTitle("Resources");
  }, [updateTitle]);

  const orgLinks = (
    <Fragment>
      <SessionButton
        className={classes.sessionBox}
        variant="contained"
        color="primary"
        component={Link}
        to="/users"
      >
        <ListItemText className={classes.mySessions} primary="Users" />
        <Group />
      </SessionButton>
      <SessionButton
        className={classes.sessionBox}
        variant="contained"
        color="primary"
        component={Link}
        to="/trainings"
      >
        <ListItemText className={classes.mySessions} primary="Trainings" />
        <ViewList />
      </SessionButton>
      <SessionButton
        className={classes.sessionBox}
        variant="contained"
        color="primary"
        component={Link}
        to="/overview"
      >
        <ListItemText className={classes.mySessions} primary="Overview" />
        <Equalizer />
      </SessionButton>
    </Fragment>
  );

  const adminLinks = (
    <Fragment>
      <SessionButton
        className={classes.sessionBox}
        variant="contained"
        color="primary"
        component={Link}
        to="/organizations"
      >
        <ListItemText className={classes.mySessions} primary="Organizations" />
        <Business />
      </SessionButton>
      <SessionButton
        className={classes.sessionBox}
        variant="contained"
        color="primary"
        component={Link}
        to="/trainings"
      >
        <ListItemText className={classes.mySessions} primary="Trainings" />
        <ViewList />
      </SessionButton>
      <SessionButton
        className={classes.sessionBox}
        variant="contained"
        color="primary"
        component={Link}
        to="/overview"
      >
        <ListItemText className={classes.mySessions} primary="Overview" />
        <Equalizer />
      </SessionButton>
    </Fragment>
  );

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.dashboard}>
        {isOrgAdmin && orgLinks}
        {isSuperAdmin && adminLinks}
      </div>
      <div className={classes.dashboard}></div>
    </main>
  );
}

import { useStyles } from "../style";
import {
  Button,
  Grid,
  TextField,
  Typography,
  Snackbar,
  FormControl,
  Select,
  InputLabel,
  Chip,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@material-ui/core";
import { Cancel, Add } from "@material-ui/icons";
import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Alert as MuiAlert } from "@material-ui/lab";
import uniqueId from "lodash/uniqueId";
import {
  listOrganizations,
  listTrainingIndicators,
  listSessionIndicators,
  listMetricCards,
  createOrganizationTraining,
} from "../data";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const trainingIndicators = listTrainingIndicators();
const sessionIndicators = listSessionIndicators();
const chart = listMetricCards().find((card) => card.type === "chart");
const text = listMetricCards().find((card) => card.type === "text");
const number = listMetricCards().find((card) => card.type === "number");

export default function AddTraining() {
  const { register, handleSubmit, errors } = useForm();
  const [signUpErrors, setSignUpErrors] = useState([]);
  const [organization, setOrganization] = useState("");
  const [organizations, setOrganizations] = useState([{ name: "", id: "" }]);
  const [chartCard, setChartCard] = useState(chart.cards[0]);
  const [textCard, setTextCard] = useState(text.cards[0]);
  const [numberCard, setNumberCard] = useState(number.cards[0]);
  const [selectedTrainingIndicators, setSelectedTrainingIndicators] = useState(
    trainingIndicators
  );
  const [
    unselectedTrainingIndicators,
    setUnselectedTrainingIndicators,
  ] = useState([]);
  const [selectedSessionIndicators, setSelectedSessionIndicators] = useState(
    sessionIndicators
  );
  const [
    unselectedSessionIndicators,
    setUnselectedSessionIndicators,
  ] = useState([]);
  const classes = useStyles();

  async function onSubmit(validatedData) {
    const { name } = validatedData;

    let trainingIndicatorsAwsjson = [];
    selectedTrainingIndicators.map((indicator) => {
      trainingIndicatorsAwsjson.push({ type: indicator });
      return indicator;
    });
    trainingIndicatorsAwsjson = JSON.stringify(trainingIndicatorsAwsjson);

    let sessionIndicatorsAwsjson = [];
    selectedSessionIndicators.map((indicator) => {
      sessionIndicatorsAwsjson.push({ type: indicator });
      return indicator;
    });
    sessionIndicatorsAwsjson = JSON.stringify(sessionIndicatorsAwsjson);

    let metricCardsAwsjson = [
      { type: "chart", card: chartCard },
      { type: "text", card: textCard },
      { type: "number", card: numberCard },
    ];
    metricCardsAwsjson = JSON.stringify(metricCardsAwsjson);

    try {
      createOrganizationTraining({
        name,
        metricCards: metricCardsAwsjson,
        organizationId: organization,
        trainingIndicators: trainingIndicatorsAwsjson,
        sessionIndicators: sessionIndicatorsAwsjson,
      });
    } catch (err) {
      setSignUpErrors([
        ...signUpErrors,
        { type: err.code, message: err.message },
      ]);
    }
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSignUpErrors([]);
  };

  const handleChange = (event) => {
    const value = event.target.value;
    setOrganization(value);
  };

  const handleChartChange = (event) => {
    setChartCard(event.target.value);
  };

  const handleTextChange = (event) => {
    setTextCard(event.target.value);
  };

  const handleNumberChange = (event) => {
    setNumberCard(event.target.value);
  };

  const handleDeleteTrainingIndicator = (item) => {
    const filteredIndicators = selectedTrainingIndicators.filter(
      (indicator) => indicator !== item
    );
    const selectedIndicator = trainingIndicators.filter(
      (indicator) => indicator === item
    );
    setUnselectedTrainingIndicators([
      ...unselectedTrainingIndicators,
      selectedIndicator[0],
    ]);
    setSelectedTrainingIndicators(filteredIndicators);
  };

  const handleAddTrainingIndicator = (item) => {
    const selectedIndicator = trainingIndicators.filter(
      (indicator) => indicator === item
    );
    const filteredIndicators = unselectedTrainingIndicators.filter(
      (indicator) => indicator !== item
    );

    setUnselectedTrainingIndicators(filteredIndicators);
    setSelectedTrainingIndicators([
      ...selectedTrainingIndicators,
      selectedIndicator[0],
    ]);
  };

  const handleDeleteSessionIndicator = (item) => {
    const filteredIndicators = selectedSessionIndicators.filter(
      (indicator) => indicator !== item
    );
    const selectedIndicator = sessionIndicators.filter(
      (indicator) => indicator === item
    );
    setUnselectedSessionIndicators([
      ...unselectedSessionIndicators,
      selectedIndicator[0],
    ]);
    setSelectedSessionIndicators(filteredIndicators);
  };

  const handleAddSessionIndicator = (item) => {
    const selectedIndicator = sessionIndicators.filter(
      (indicator) => indicator === item
    );
    const filteredIndicators = unselectedSessionIndicators.filter(
      (indicator) => indicator !== item
    );

    setUnselectedSessionIndicators(filteredIndicators);
    setSelectedSessionIndicators([
      ...selectedSessionIndicators,
      selectedIndicator[0],
    ]);
  };

  useEffect(() => {
    listOrganizations().then((res) =>
      setOrganizations(res.data.listOrganizations.items)
    );
  }, []);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.dashboard}>
        <div className={classes.addTraining}>
          <Typography
            className={classes.signUpTitle}
            component="h1"
            variant="h6"
          >
            Create new training
          </Typography>
          <form
            className={classes.signUpForm}
            noValidate
            onSubmit={handleSubmit((data) => onSubmit(data))}
          >
            <Grid container spacing={4}>
              <Grid item xs={12}>
                {!errors.name ? (
                  <TextField
                    autoComplete="name"
                    color="secondary"
                    inputRef={register({
                      required: "The name field can't be empty",
                      maxLength: {
                        value: 50,
                        message: "The name cannot have more than 50 characters",
                      },
                    })}
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Training name"
                    autoFocus
                  />
                ) : (
                  <TextField
                    autoComplete="name"
                    error
                    helperText={errors.name.message}
                    FormHelperTextProps={{
                      className: classes.signInHelperText,
                    }}
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Training name"
                    autoFocus
                  />
                )}
              </Grid>
              <Grid item xs={12}>
                {!errors.organization ? (
                  <FormControl variant="outlined" fullWidth required>
                    <InputLabel
                      htmlFor="outlined-organization-native-simple"
                      color="secondary"
                    >
                      Organization
                    </InputLabel>
                    <Select
                      native
                      color="secondary"
                      value={organization}
                      onChange={handleChange}
                      label="Organization"
                      inputRef={register({
                        required: "The organization field can't be empty",
                      })}
                      inputProps={{
                        name: "organization",
                        id: "outlined-organization-native-simple",
                      }}
                    >
                      <option aria-label="None" value="" />
                      {organizations.map((org) => (
                        <option value={org.id} key={uniqueId("options-")}>
                          {org.name}
                        </option>
                      ))}
                    </Select>
                  </FormControl>
                ) : (
                  <FormControl variant="outlined" fullWidth required>
                    <InputLabel
                      htmlFor="outlined-organization-native-simple"
                      color="secondary"
                    >
                      Organization
                    </InputLabel>
                    <Select
                      native
                      value={organization}
                      onChange={handleChange}
                      error
                      label="Organization"
                      inputRef={register({
                        required: "The organization field can't be empty",
                      })}
                      inputProps={{
                        name: "organization",
                        id: "outlined-organization-native-simple",
                      }}
                      helperText={errors.organization.message}
                      FormHelperTextProps={{
                        className: classes.signInHelperText,
                      }}
                    >
                      <option aria-label="None" value="" />
                      {organizations.map((org) => (
                        <option value={org.id} key={uniqueId("options-")}>
                          {org.name}
                        </option>
                      ))}
                    </Select>
                  </FormControl>
                )}
              </Grid>
            </Grid>
            <Typography className={classes.signUpSubtitle}>
              Training indicators
            </Typography>
            <div className={classes.trainingChipsRegister}>
              {selectedTrainingIndicators.map((indicator) => (
                <Chip
                  className={classes.trainingChipRegister}
                  label={`${indicator}`}
                  onDelete={() => {
                    handleDeleteTrainingIndicator(indicator);
                  }}
                  deleteIcon={<Cancel />}
                  key={uniqueId("training-indicators-")}
                />
              ))}
              {trainingIndicators.length ? (
                unselectedTrainingIndicators.map((indicator) => (
                  <Chip
                    className={classes.trainingChipUnregister}
                    label={`${indicator}`}
                    onDelete={() => {
                      handleAddTrainingIndicator(indicator);
                    }}
                    deleteIcon={<Add />}
                    key={uniqueId("training-indicators-")}
                  />
                ))
              ) : (
                <Typography className={classes.counterZero}>
                  No training indicators available.
                </Typography>
              )}
            </div>
            <Typography className={classes.signUpSubtitle}>
              Session indicators
            </Typography>
            <div className={classes.trainingChipsRegister}>
              {selectedSessionIndicators.map((indicator) => (
                <Chip
                  className={classes.trainingChipRegister}
                  label={`${indicator}`}
                  onDelete={() => {
                    handleDeleteSessionIndicator(indicator);
                  }}
                  deleteIcon={<Cancel />}
                  key={uniqueId("session-indicators-")}
                />
              ))}
              {sessionIndicators.length ? (
                unselectedSessionIndicators.map((indicator) => (
                  <Chip
                    className={classes.trainingChipUnregister}
                    label={`${indicator}`}
                    onDelete={() => {
                      handleAddSessionIndicator(indicator);
                    }}
                    deleteIcon={<Add />}
                    key={uniqueId("session-indicators-")}
                  />
                ))
              ) : (
                <Typography className={classes.counterZero}>
                  No session indicators available.
                </Typography>
              )}
            </div>
            <Typography className={classes.signUpSubtitle}>
              Metric cards
            </Typography>
            <FormControl
              component="fieldset"
              fullWidth
              color="secondary"
              className={classes.signUpSubtitle}
            >
              <FormLabel component="legend">Chart</FormLabel>
              <RadioGroup
                aria-label="chart"
                name="chart"
                value={chartCard}
                onChange={handleChartChange}
              >
                {chart.cards.map((card) => (
                  <FormControlLabel
                    value={card}
                    control={<Radio />}
                    label={card}
                  />
                ))}
              </RadioGroup>
            </FormControl>
            <FormControl
              component="fieldset"
              fullWidth
              color="secondary"
              className={classes.signUpSubtitle}
            >
              <FormLabel component="legend">Text</FormLabel>
              <RadioGroup
                aria-label="text"
                name="text"
                value={textCard}
                onChange={handleTextChange}
              >
                {text.cards.map((card) => (
                  <FormControlLabel
                    value={card}
                    control={<Radio />}
                    label={card}
                  />
                ))}
              </RadioGroup>
            </FormControl>
            <FormControl
              component="fieldset"
              fullWidth
              color="secondary"
              className={classes.signUpSubtitle}
            >
              <FormLabel component="legend">Number</FormLabel>
              <RadioGroup
                aria-label="number"
                name="number"
                value={numberCard}
                onChange={handleNumberChange}
              >
                {number.cards.map((card) => (
                  <FormControlLabel
                    value={card}
                    control={<Radio />}
                    label={card}
                  />
                ))}
              </RadioGroup>
            </FormControl>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.signUpSubmit}
            >
              Add training
            </Button>
          </form>
        </div>
        {signUpErrors &&
          signUpErrors.map((error) => (
            <Snackbar
              open={true}
              autoHideDuration={6000}
              onClose={handleClose}
              key={uniqueId("sign-in-")}
            >
              <Alert onClose={handleClose} severity="error">
                {error.message}
              </Alert>
            </Snackbar>
          ))}
      </div>
    </main>
  );
}

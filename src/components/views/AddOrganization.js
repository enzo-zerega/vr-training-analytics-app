import { useStyles } from "../style";
import {
  Button,
  Grid,
  TextField,
  Typography,
  Snackbar,
  Chip,
} from "@material-ui/core";
import { Cancel, Add } from "@material-ui/icons";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Alert as MuiAlert } from "@material-ui/lab";
import uniqueId from "lodash/uniqueId";
import { listGlobalIndicators, createOrganization } from "../data";
import awsmobile from "../../aws-exports";
import AWS from "aws-sdk";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const globalIndicators = listGlobalIndicators();

const { REACT_APP_IDENTITY_POOL_ID } = process.env;

AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: REACT_APP_IDENTITY_POOL_ID,
});

AWS.config.region = awsmobile.aws_cognito_region;

export default function AddOrganization(props) {
  const { register, handleSubmit, errors } = useForm();
  const [signUpErrors, setSignUpErrors] = useState([]);
  const classes = useStyles();
  const [selectedIndicators, setSelectedIndicators] = useState(
    globalIndicators
  );
  const [unselectedIndicators, setUnselectedIndicators] = useState([]);

  async function onSubmit(validatedData) {
    const { email, name, avatar } = validatedData;
    try {
      const cognito = new AWS.CognitoIdentityServiceProvider();
      await cognito
        .adminCreateUser({
          UserPoolId: `${awsmobile.aws_user_pools_id}`,
          Username: email,
          TemporaryPassword: undefined,
        })
        .promise()
        .then((res) => {
          addUserToGroup(res.User);
          return res.User;
        })
        .then((user) => {
          let awsjson = [];
          selectedIndicators.map((indicator) => {
            awsjson.push({ type: indicator });
            return indicator;
          });
          awsjson = JSON.stringify(awsjson);
          !avatar
            ? createOrganization({
                id: user.Username,
                name: name.trim(),
                globalIndicators: awsjson,
              })
            : createOrganization({
                id: user.username,
                name: name.trim(),
                avatar: avatar.trim(),
                globalIndicators: awsjson,
              });
          return user;
        });
    } catch (err) {
      setSignUpErrors([
        ...signUpErrors,
        { type: err.code, message: err.message },
      ]);
    }
  }

  async function addUserToGroup(user) {
    try {
      const cognito = new AWS.CognitoIdentityServiceProvider();

      await cognito
        .adminAddUserToGroup({
          GroupName: "org-admin",
          UserPoolId: `${awsmobile.aws_user_pools_id}`,
          Username: user.Username,
        })
        .promise();
    } catch (err) {
      setSignUpErrors([
        ...signUpErrors,
        { type: err.code, message: err.message },
      ]);
    }
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSignUpErrors([]);
  };

  const handleDelete = (item) => {
    const filteredIndicators = selectedIndicators.filter(
      (indicator) => indicator !== item
    );
    const selectedIndicator = globalIndicators.filter(
      (indicator) => indicator === item
    );
    setUnselectedIndicators([...unselectedIndicators, selectedIndicator[0]]);
    setSelectedIndicators(filteredIndicators);
  };

  const handleAdd = (item) => {
    const selectedIndicator = globalIndicators.filter(
      (indicator) => indicator === item
    );
    const filteredIndicators = unselectedIndicators.filter(
      (indicator) => indicator !== item
    );

    setUnselectedIndicators(filteredIndicators);
    setSelectedIndicators([...selectedIndicators, selectedIndicator[0]]);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.dashboard}>
        <div className={classes.signUp}>
          <Typography
            className={classes.signUpTitle}
            component="h1"
            variant="h6"
          >
            Register Form
          </Typography>
          <form
            className={classes.signUpForm}
            noValidate
            onSubmit={handleSubmit((data) => onSubmit(data))}
          >
            <Grid container spacing={4}>
              <Grid item xs={12}>
                {!errors.name ? (
                  <TextField
                    autoComplete="name"
                    color="secondary"
                    inputRef={register({
                      required: "The name field can't be empty",
                      maxLength: {
                        value: 50,
                        message: "The name cannot have more than 50 characters",
                      },
                    })}
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Organization name"
                    autoFocus
                  />
                ) : (
                  <TextField
                    autoComplete="name"
                    error
                    helperText={errors.name.message}
                    FormHelperTextProps={{
                      className: classes.signInHelperText,
                    }}
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Organization name"
                    autoFocus
                  />
                )}
              </Grid>
              <Grid item xs={12}>
                {!errors.email ? (
                  <TextField
                    color="secondary"
                    inputRef={register({
                      required: "The email field can't be empty",
                      pattern: {
                        value: /\S+@\S+\.\S+/,
                        message: "Enter a valid email address",
                      },
                    })}
                    variant="outlined"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                  />
                ) : (
                  <TextField
                    error
                    helperText={errors.email.message}
                    FormHelperTextProps={{
                      className: classes.signInHelperText,
                    }}
                    variant="outlined"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                  />
                )}
              </Grid>
              <Grid item xs={12}>
                {!errors.avatar ? (
                  <TextField
                    autoComplete="avatar"
                    color="secondary"
                    inputRef={register({
                      maxLength: {
                        value: 200,
                        message: "The url cannot have more than 200 characters",
                      },
                    })}
                    name="avatar"
                    variant="outlined"
                    fullWidth
                    id="avatar"
                    label="Avatar url"
                  />
                ) : (
                  <TextField
                    autoComplete="avatar"
                    error
                    helperText={errors.name.message}
                    FormHelperTextProps={{
                      className: classes.signInHelperText,
                    }}
                    name="avatar"
                    variant="outlined"
                    fullWidth
                    id="avatar"
                    label="Avatar url"
                  />
                )}
              </Grid>
            </Grid>
            <Typography className={classes.signUpSubtitle}>
              Global indicators
            </Typography>
            <div className={classes.trainingChipsRegister}>
              {selectedIndicators.map((indicator) => (
                <Chip
                  className={classes.trainingChipRegister}
                  label={`${indicator}`}
                  onDelete={() => {
                    handleDelete(indicator);
                  }}
                  deleteIcon={<Cancel />}
                  key={uniqueId("indicators-")}
                />
              ))}
              {globalIndicators.length ? (
                unselectedIndicators.map((indicator) => (
                  <Chip
                    className={classes.trainingChipUnregister}
                    label={`${indicator}`}
                    onDelete={() => {
                      handleAdd(indicator);
                    }}
                    deleteIcon={<Add />}
                    key={uniqueId("indicators-")}
                  />
                ))
              ) : (
                <Typography className={classes.counterZero}>
                  No global indicators available.
                </Typography>
              )}
            </div>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.signUpSubmit}
            >
              Add organization
            </Button>
          </form>
        </div>
        {signUpErrors &&
          signUpErrors.map((error) => (
            <Snackbar
              open={true}
              autoHideDuration={6000}
              onClose={handleClose}
              key={uniqueId("sign-in-")}
            >
              <Alert onClose={handleClose} severity="error">
                {error.message}
              </Alert>
            </Snackbar>
          ))}
      </div>
    </main>
  );
}

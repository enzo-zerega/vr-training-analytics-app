import { useStyles } from "../style";
import { ListItemText, ListItemIcon, Typography } from "@material-ui/core";
import { Apps, Edit } from "@material-ui/icons";
import { LinkButton } from "../common";
import { useParams, Link } from "react-router-dom";
import { useEffect, useState } from "react";
import { spaceToHyphen } from "../common/methods";
import { listTrainingsByOrgTrainingId, getOrganization } from "../data";
import { UsersCount, Indicators } from "../common/indicators";

export default function Training(props) {
  const {
    updateTitle,
    user: userFromProps,
    organization: organizationFromProps,
    setOrganization: setOrganizationFromProps,
    isUser,
    isOrgAdmin,
    isSuperAdmin,
  } = props;
  const {
    training: trainingFromParams,
    user: userFromParams,
    organization: organizationFromParams,
  } = useParams();
  const classes = useStyles();
  const training = userFromProps.trainings.items.find(
    (training) => spaceToHyphen(training.details.name) === trainingFromParams
  );
  const [orgTraining, setOrgTraining] = useState(userFromProps.trainings.items);
  const [organization, setOrganization] = useState(organizationFromProps);
  const [trainingIndicators, setTrainingIndicators] = useState([{ type: "" }]);

  const areAggregatedUsers =
    (isOrgAdmin && !userFromParams) || (isSuperAdmin && organizationFromParams);

  useEffect(() => {
    isUser && updateTitle(trainingFromParams);
    userFromParams && updateTitle(userFromProps.name);
    if (isUser || userFromParams) {
      setTrainingIndicators(
        JSON.parse(
          userFromProps.trainings.items.find(
            (training) =>
              spaceToHyphen(training.details.name) === trainingFromParams
          ).details.trainingIndicators
        )
      );
    } else if (isSuperAdmin && organizationFromParams) {
      updateTitle(trainingFromParams);

      getOrganization(organizationFromParams)
        .then((res) => {
          setOrganization(res.data.getOrganization);
          return res.data.getOrganization;
        })
        .then((resp) => {
          setTrainingIndicators(
            JSON.parse(
              resp.trainings.items.find(
                (training) =>
                  spaceToHyphen(training.name) === trainingFromParams
              ).trainingIndicators
            )
          );
          return resp;
        })
        .then((respo) => {
          const trainingId = respo.trainings.items.find(
            (training) => spaceToHyphen(training.name) === trainingFromParams
          ).id;
          listTrainingsByOrgTrainingId(trainingId).then((res) =>
            setOrgTraining(res.data.listTrainings.items)
          );
        });
      if (organization.trainings.items[0].id) {
        let trainingId = "";
        try {
          trainingId = organization.trainings.items.find(
            (training) => spaceToHyphen(training.name) === trainingFromParams
          ).id;
          listTrainingsByOrgTrainingId(trainingId).then((res) =>
            setOrgTraining(res.data.listTrainings.items)
          );
        } catch (err) {
          console.log({ err });
        }
      }
    } else if (isOrgAdmin && !organizationFromParams) {
      setTrainingIndicators(
        JSON.parse(
          organizationFromProps.trainings.items.find(
            (training) => spaceToHyphen(training.name) === trainingFromParams
          ).trainingIndicators
        )
      );
      const trainingId = organizationFromProps.trainings.items.find(
        (training) => spaceToHyphen(training.name) === trainingFromParams
      ).id;
      listTrainingsByOrgTrainingId(trainingId).then((res) =>
        setOrgTraining(res.data.listTrainings.items)
      );
    }
  }, []);

  function handleClick() {
    setOrganizationFromProps(organization);
  }

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      {isOrgAdmin && (
        <Typography
          variant="h6"
          className={classes.trainingTitle}
          color="secondary"
        >
          Training overview
        </Typography>
      )}
      <div className={classes.topButtons}>
        {(isUser || userFromParams) && (
          <LinkButton
            variant="contained"
            color="secondary"
            component={Link}
            className={classes.mainLinkButton}
            to={
              isUser
                ? `/trainings/${trainingFromParams}/sessions`
                : `/users/${userFromParams}/trainings/${trainingFromParams}/sessions`
            }
          >
            <ListItemIcon className={classes.listItemIcon}>
              <Apps color="primary" />
            </ListItemIcon>
            <ListItemText
              className={classes.myTrainings}
              primary={"sessions"}
            />
          </LinkButton>
        )}
        {areAggregatedUsers && (
          <UsersCount
            users={organization ? organization.users.items : 0}
            training={trainingFromParams}
            disabled={isSuperAdmin ? true : false}
          />
        )}
        {isSuperAdmin && (
          <LinkButton
            variant="contained"
            color="secondary"
            component={Link}
            onClick={() => handleClick()}
            to={`/organizations/${organizationFromParams}/trainings/${trainingFromParams}/edit`}
          >
            <ListItemIcon className={classes.listItemIcon}>
              <Edit color="primary" />
            </ListItemIcon>
            <ListItemText
              className={classes.myTrainings}
              primary="Edit training"
            />
          </LinkButton>
        )}
      </div>
      <div className={classes.dashboard}>
        <div className={classes.dashboardItems}>
          <Indicators
            items={trainingIndicators}
            isUser={isUser}
            isOrgAdmin={isOrgAdmin}
            scope="training"
            data={isUser || userFromParams ? [training] : orgTraining}
          />
        </div>
      </div>
    </main>
  );
}

import { useStyles } from "../style";
import {
  Button,
  Grid,
  TextField,
  Typography,
  Snackbar,
  Chip,
} from "@material-ui/core";
import { Cancel, Add, Error, Delete } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Alert as MuiAlert } from "@material-ui/lab";
import uniqueId from "lodash/uniqueId";
import { listGlobalIndicators, updateOrganization } from "../data";
import { useParams } from "react-router-dom";
import awsmobile from "../../aws-exports";
import AWS from "aws-sdk";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const { REACT_APP_IDENTITY_POOL_ID } = process.env;

AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: REACT_APP_IDENTITY_POOL_ID,
});

AWS.config.region = awsmobile.aws_cognito_region;

const availableIndicators = listGlobalIndicators();

export default function EditOrganization(props) {
  const { updateTitle, organization: organizationFromProps } = props;
  let globalIndicators = [];
  JSON.parse(organizationFromProps.globalIndicators).map((indicator) =>
    globalIndicators.push(indicator.type)
  );
  const { organization: organizationFromParams } = useParams();
  const [signUpErrors, setSignUpErrors] = useState([]);
  const classes = useStyles();

  const [selectedIndicators, setSelectedIndicators] = useState(
    globalIndicators
  );
  const [unselectedIndicators, setUnselectedIndicators] = useState([]);
  const [email, setEmail] = useState("");
  const { register, handleSubmit, errors } = useForm({
    defaultValues: {
      name: organizationFromProps.name,
      avatar: organizationFromProps.avatar,
    },
  });
  const { REACT_APP_IDENTITY_POOL_ID } = process.env;

  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: REACT_APP_IDENTITY_POOL_ID,
  });

  AWS.config.region = awsmobile.aws_cognito_region;
  const poolId = awsmobile.aws_user_pools_id;

  async function getUser(Username, UserPoolId) {
    try {
      const cognito = new AWS.CognitoIdentityServiceProvider();
      await cognito
        .adminGetUser({ Username, UserPoolId })
        .promise()
        .then((res) => {
          const userEmail = res.UserAttributes.find(
            (attribute) => attribute.Name === "email"
          ).Value;
          setEmail(userEmail);
        });
    } catch (err) {
      setSignUpErrors([
        ...signUpErrors,
        { type: err.code, message: err.message },
      ]);
    }
  }

  async function onSubmit(validatedData) {
    const { name, avatar } = validatedData;
    let isChanged = false;
    let input = { id: organizationFromProps.id };
    if (name !== organizationFromProps.name) {
      input = { ...input, name };
      isChanged = true;
    }
    if (avatar !== organizationFromProps.avatar && avatar !== "") {
      input = { ...input, avatar };
      isChanged = true;
    }
    let changedIndicators = false;
    globalIndicators.map((indicator) => {
      const ind = selectedIndicators.find((selected) => selected === indicator);
      if (!ind) changedIndicators = true;
      return indicator;
    });
    if (changedIndicators) {
      let awsjson = [];
      selectedIndicators.map((indicator) => {
        awsjson.push({ type: indicator });
        return indicator;
      });
      awsjson = JSON.stringify(awsjson);
      input = { ...input, globalIndicators: awsjson };
      isChanged = true;
    }
    if (isChanged) {
      try {
        updateOrganization(input);
      } catch (err) {
        setSignUpErrors([
          ...signUpErrors,
          { type: err.code, message: err.message },
        ]);
      }
    } else {
      setSignUpErrors([...signUpErrors, { message: "No changes were made" }]);
    }
  }
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSignUpErrors([]);
  };

  const handleDelete = (item) => {
    const filteredIndicators = selectedIndicators.filter(
      (indicator) => indicator !== item
    );
    const selectedIndicator = globalIndicators.filter(
      (indicator) => indicator === item
    );
    setUnselectedIndicators([...unselectedIndicators, selectedIndicator[0]]);
    setSelectedIndicators(filteredIndicators);
  };

  const handleAdd = (item) => {
    const selectedIndicator = globalIndicators.filter(
      (indicator) => indicator === item
    );
    const filteredIndicators = unselectedIndicators.filter(
      (indicator) => indicator !== item
    );

    setUnselectedIndicators(filteredIndicators);
    setSelectedIndicators([...selectedIndicators, selectedIndicator[0]]);
  };
  getUser(organizationFromParams, poolId);
  useEffect(() => {
    updateTitle("Edit organization");

    let arr = [];
    availableIndicators.map((indicator) => {
      const isSelectedIndicator = selectedIndicators.find(
        (selectedIndicator) => selectedIndicator === indicator
      );
      if (!isSelectedIndicator) arr.push(indicator);
      return indicator;
    });
    setUnselectedIndicators(arr);
  }, [organizationFromParams, poolId, updateTitle, selectedIndicators]);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.dashboard}>
        <div className={classes.signUp}>
          <Typography
            className={classes.signUpTitle}
            component="h1"
            variant="h6"
          >
            Edit Form
          </Typography>
          <form
            className={classes.signUpForm}
            noValidate
            onSubmit={handleSubmit((data) => onSubmit(data))}
          >
            <Grid container spacing={4}>
              <Grid item xs={12}>
                {!errors.name ? (
                  <TextField
                    autoComplete="name"
                    color="secondary"
                    inputRef={register({
                      required: "The name field can't be empty",
                      maxLength: {
                        value: 50,
                        message: "The name cannot have more than 50 characters",
                      },
                    })}
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Organization name"
                    autoFocus
                  />
                ) : (
                  <TextField
                    autoComplete="name"
                    error
                    helperText={errors.name.message}
                    FormHelperTextProps={{
                      className: classes.signInHelperText,
                    }}
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Organization name"
                    autoFocus
                  />
                )}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  color="secondary"
                  variant="outlined"
                  disabled
                  value={email}
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                {!errors.avatar ? (
                  <TextField
                    autoComplete="avatar"
                    color="secondary"
                    inputRef={register({
                      maxLength: {
                        value: 200,
                        message: "The url cannot have more than 200 characters",
                      },
                    })}
                    name="avatar"
                    variant="outlined"
                    fullWidth
                    id="avatar"
                    label="Avatar url"
                  />
                ) : (
                  <TextField
                    autoComplete="avatar"
                    error
                    helperText={errors.name.message}
                    FormHelperTextProps={{
                      className: classes.signInHelperText,
                    }}
                    name="avatar"
                    variant="outlined"
                    fullWidth
                    id="avatar"
                    label="Avatar url"
                  />
                )}
              </Grid>
            </Grid>
            <Typography className={classes.signUpSubtitle}>
              Global indicators
            </Typography>
            <div className={classes.trainingChipsRegister}>
              {selectedIndicators.map((indicator) => (
                <Chip
                  className={classes.trainingChipRegister}
                  label={`${indicator}`}
                  onDelete={() => {
                    handleDelete(indicator);
                  }}
                  deleteIcon={<Cancel />}
                  key={uniqueId("indicators-")}
                />
              ))}
              {globalIndicators.length ? (
                unselectedIndicators.map((indicator) => (
                  <Chip
                    className={classes.trainingChipUnregister}
                    label={`${indicator}`}
                    onDelete={() => {
                      handleAdd(indicator);
                    }}
                    deleteIcon={<Add />}
                    key={uniqueId("indicators-")}
                  />
                ))
              ) : (
                <Typography className={classes.counterZero}>
                  No global indicators available.
                </Typography>
              )}
            </div>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.signUpSubmit}
            >
              Save changes
            </Button>
          </form>
          <Typography className={classes.disableText}>
            <Error />
            Disable organization
          </Typography>
          <Typography className={classes.deleteText}>
            <Delete />
            Delete organization
          </Typography>
        </div>
        {signUpErrors &&
          signUpErrors.map((error) => (
            <Snackbar
              open={true}
              autoHideDuration={6000}
              onClose={handleClose}
              key={uniqueId("sign-in-")}
            >
              <Alert onClose={handleClose} severity="error">
                {error.message}
              </Alert>
            </Snackbar>
          ))}
      </div>
    </main>
  );
}

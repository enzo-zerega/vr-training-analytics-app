import { useEffect, useState } from "react";
import { useStyles } from "../style";
import { ListItemIcon, ListItemText, Typography } from "@material-ui/core";
import { ViewList, Edit } from "@material-ui/icons";
import { LinkButton } from "../common";
import { Indicators } from "../common/indicators";
import { Link, useParams } from "react-router-dom";
import { getUser } from "../data";

export default function UserDashboard(props) {
  const {
    updateTitle,
    user: userFromProps,
    isUser,
    isOrgAdmin,
    setUser: setUserFromProps,
  } = props;
  const { user: userFromParams } = useParams();
  const [user, setUser] = useState(userFromProps);
  const [globalIndicators, setGlobalIndicators] = useState(
    JSON.parse(userFromProps.organization.globalIndicators)
  );
  const classes = useStyles();

  useEffect(() => {
    if (isUser) {
      updateTitle("Overview");
    } else if (isOrgAdmin) {
      getUser(userFromParams).then((res) => {
        setUser(res.data.getUser);
        setGlobalIndicators(
          JSON.parse(res.data.getUser.organization.globalIndicators)
        );
        updateTitle(res.data.getUser.name);
      });
    }
  }, [isUser, updateTitle, isOrgAdmin, userFromParams]);

  const handleClick = () => {
    if (isOrgAdmin) setUserFromProps(user);
  };

  const userTrainings = user.trainings.items[0].details.name
    ? user.trainings.items.length
    : 0;

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      {isOrgAdmin && (
        <Typography
          variant="h6"
          className={classes.trainingTitle}
          color="secondary"
        >
          User overview
        </Typography>
      )}
      <div className={classes.topButtons}>
        <LinkButton
          variant="contained"
          color="secondary"
          component={Link}
          onClick={handleClick}
          to={
            isUser
              ? "/trainings"
              : { pathname: `/users/${userFromParams}/trainings`, user }
          }
        >
          <ListItemIcon className={classes.listItemIcon}>
            <ViewList color="primary" />
          </ListItemIcon>
          <ListItemText
            className={classes.myTrainings}
            primary={
              userTrainings !== 1
                ? `${userTrainings} trainings`
                : `${userTrainings} training`
            }
          />
        </LinkButton>
        {isOrgAdmin && (
          <LinkButton
            variant="contained"
            color="secondary"
            component={Link}
            onClick={handleClick}
            to={`/users/${userFromParams}/edit`}
          >
            <ListItemIcon className={classes.listItemIcon}>
              <Edit color="primary" />
            </ListItemIcon>
            <ListItemText className={classes.myTrainings} primary="Edit user" />
          </LinkButton>
        )}
      </div>
      <div className={classes.dashboard}>
        <div className={classes.dashboardItems}>
          <Indicators
            items={globalIndicators}
            scope="global"
            data={isUser ? [userFromProps] : [user]}
          />
        </div>
      </div>
    </main>
  );
}

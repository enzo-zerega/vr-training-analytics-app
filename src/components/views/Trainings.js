import { useStyles } from "../style";
import {
  ListItemText,
  ListItemIcon,
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  Paper,
  Typography,
} from "@material-ui/core";
import { CheckCircleOutline, Add, OpenInNew } from "@material-ui/icons";
import { Link, useParams } from "react-router-dom";
import { TrainingButton, ProgressCircle, LinkButton } from "../common";
import {
  spaceToHyphen,
  capitalizeFirstChar,
  calcProgress,
} from "../common/methods";
import { useEffect, useState } from "react";
import { listTrainings } from "../data";
import uniqueId from "lodash/uniqueId";

export default function Trainings(props) {
  const {
    updateTitle,
    user: userFromProps,
    organization: organizationFromProps,
    isUser,
    isOrgAdmin,
    isSuperAdmin,
  } = props;
  const {
    user: userFromParams,
    organization: organizationFromParams,
  } = useParams();
  const [trainings, setTrainings] = useState(userFromProps.trainings.items);
  const organizationTrainings = organizationFromProps.trainings.items;

  const classes = useStyles();

  useEffect(() => {
    updateTitle("Trainings");
    if (isUser) {
    } else if (userFromParams) {
      updateTitle(userFromProps.name);
    } else if (isSuperAdmin && !organizationFromParams) {
      listTrainings().then(
        (res) =>
          res.data.listOrganizationTrainings.items.length !== 0 &&
          setTrainings(res.data.listOrganizationTrainings.items)
      );
    }
  }, [
    isUser,
    updateTitle,
    organizationFromParams,
    userFromParams,
    userFromProps.name,
    isSuperAdmin,
  ]);

  if (isUser || userFromParams) {
    return (
      <main className={classes.content}>
        <div className={classes.toolbar} />

        {trainings.length &&
          trainings.map((training, index) => (
            <div className={classes.trainingBoxes}>
              <TrainingButton
                key={index}
                className={classes.trainingBox}
                variant="contained"
                color="primary"
                component={Link}
                to={
                  isUser
                    ? `/trainings/${spaceToHyphen(training.details.name)}`
                    : `/users/${userFromParams}/trainings/${spaceToHyphen(
                        training.details.name
                      )}`
                }
              >
                <ListItemText
                  className={classes.myTrainings}
                  primary={capitalizeFirstChar(training.details.name)}
                />
                {calcProgress([training], "training") === 100 ? (
                  <CheckCircleOutline
                    fontSize="large"
                    style={{
                      color: "#A5D6A7",
                      transform: "scale(1.3)",
                      margin: "0.6rem",
                    }}
                  />
                ) : (
                  <ProgressCircle
                    value={
                      calcProgress([training], "training")
                        ? calcProgress([training], "training")
                        : 0
                    }
                  />
                )}
              </TrainingButton>
            </div>
          ))}
      </main>
    );
  } else if (isOrgAdmin) {
    return (
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <div className={classes.trainingBoxes}>
          {trainings.length ? (
            organizationTrainings.map((training, index) => (
              <TrainingButton
                key={index}
                className={classes.trainingBox}
                variant="contained"
                color="primary"
                component={Link}
                to={`/trainings/${spaceToHyphen(training.name)}`}
              >
                <ListItemText
                  className={classes.myTrainings}
                  primary={capitalizeFirstChar(training.name)}
                />
              </TrainingButton>
            ))
          ) : (
            <Typography className={classes.tableItem}>
              No trainings avaliable
            </Typography>
          )}
        </div>
      </main>
    );
  } else if (isSuperAdmin && !organizationFromParams) {
    return (
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <div className={classes.dashboard}>
          <LinkButton
            variant="contained"
            color="secondary"
            component={Link}
            to="/trainings/add"
          >
            <ListItemIcon className={classes.listItemIcon}>
              <Add color="primary" />
            </ListItemIcon>
            <ListItemText
              className={classes.myTrainings}
              primary="Add training"
            />
          </LinkButton>
        </div>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow className={classes.usersTitle}>
                <TableCell className={classes.usersTitle}>Name</TableCell>
                <TableCell className={classes.usersTitle} align="right">
                  Link
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {trainings[0].name ? (
                trainings.map((training) => (
                  <TableRow key={uniqueId("trainings-")}>
                    <TableCell>{training.name}</TableCell>
                    <TableCell align="right">
                      <Link
                        to={`/organizations/${
                          training.organizationId
                        }/trainings/${spaceToHyphen(training.name)}`}
                      >
                        <OpenInNew
                          fontSize="small"
                          className={classes.linkToUser}
                        />
                      </Link>
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <Typography className={classes.tableItem}>
                  No trainings avaliable
                </Typography>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </main>
    );
  } else if (isSuperAdmin && organizationFromParams) {
    return (
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <div className={classes.trainingBoxes}>
          {trainings.length ? (
            organizationTrainings.map((training, index) => (
              <TrainingButton
                key={index}
                className={classes.trainingBox}
                variant="contained"
                color="primary"
                component={Link}
                to={`/organizations/${organizationFromParams}/trainings/${spaceToHyphen(
                  training.name
                )}`}
              >
                <ListItemText
                  className={classes.myTrainings}
                  primary={capitalizeFirstChar(training.name)}
                />
              </TrainingButton>
            ))
          ) : (
            <Typography>No trainings avaliable</Typography>
          )}
        </div>
      </main>
    );
  }
}

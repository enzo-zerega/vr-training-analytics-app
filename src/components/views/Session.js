import { Typography } from "@material-ui/core";
import { useParams } from "react-router-dom";
import { useStyles } from "../style";
import {
  spaceToHyphen,
  capitalizeFirstChar,
  calcSteps,
  sortObjectByPropertyString,
  checkSessionStatus,
} from "../common/methods";
import { Indicators } from "../common/indicators";
import { Accordion, StatusChip } from "../common";
import { useEffect, useState } from "react";

export default function Session(props) {
  const {
    training: trainingFromParams,
    session: sessionFromParams,
    session: userFromParams,
  } = useParams();
  const { updateTitle, user: userFromProps, isUser } = props;
  const classes = useStyles();
  const [sessionStatus, setSessionStatus] = useState("");
  const sessionIndicators = JSON.parse(
    userFromProps.trainings.items.find(
      (training) => spaceToHyphen(training.details.name) === trainingFromParams
    ).details.sessionIndicators
  );
  const metricCards = JSON.parse(
    userFromProps.trainings.items.find(
      (training) => spaceToHyphen(training.details.name) === trainingFromParams
    ).details.metricCards
  );
  const session = userFromProps.trainings.items
    .find(
      (training) => spaceToHyphen(training.details.name) === trainingFromParams
    )
    .sessions.items.find(
      (session) => spaceToHyphen(session.name) === sessionFromParams
    );
  const steps = userFromProps.trainings.items
    .find(
      (training) => spaceToHyphen(training.details.name) === trainingFromParams
    )
    .sessions.items.find(
      (session) => spaceToHyphen(session.name) === sessionFromParams
    ).steps.items;

  useEffect(() => {
    if (isUser) {
      updateTitle(capitalizeFirstChar(trainingFromParams));
    } else if (userFromParams) {
      updateTitle(userFromProps.name);
    }
    sortObjectByPropertyString(session.steps.items, "name");
    setSessionStatus(checkSessionStatus(calcSteps(session)));
  }, [
    isUser,
    trainingFromParams,
    updateTitle,
    session.steps.items,
    session,
    userFromParams,
    userFromProps.name,
  ]);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <Typography
        variant="h6"
        className={classes.trainingTitle}
        color="secondary"
      >
        Session Metrics
      </Typography>
      <Typography
        variant="subtitle1"
        color="secondary"
        className={classes.session}
      >
        {capitalizeFirstChar(session.name)}
        <StatusChip status={sessionStatus} />
      </Typography>
      <div className={classes.stepIndicators}>
        <Indicators
          items={sessionIndicators}
          data={[session]}
          scope="session"
        />
      </div>
      <Accordion
        user={userFromProps}
        metricCards={metricCards}
        steps={steps}
        session={session}
      />
    </main>
  );
}

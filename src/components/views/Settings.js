import { useStyles } from "../style";
import { Grid, TextField, Typography, Button } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { getInitialUser } from "../data";
import { useEffect, useState } from "react";

const validationSchema = {
  currentPassword: {
    required: "Write a password",
  },
  newPassword: {
    required: "Write a password",
    minLength: {
      value: 8,
      message: "The password must have at least 8 characters",
    },
    pattern: {
      value: /^(?=.*\d)(?=.*[!@#$%^&?*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/,
      message:
        "The password must contain at least one number, one lower case and one upper case",
    },
  },
};

const initialFormState = {
  newPassword: "",
};

export default function Settings(props) {
  const {
    updateTitle,
    isUser,
    isOrgAdmin,
    user: userFromProps,
    organization: orgFromProps,
    isSuperAdmin,
  } = props;
  const classes = useStyles();
  const { register, handleSubmit, errors } = useForm();
  const [user, setUser] = useState(getInitialUser());
  const [formState, updateFormState] = useState(initialFormState);

  function onSubmit(data) {
    console.log(data);
    console.log(errors);
  }
  function onChange(e) {
    e.persist();
    updateFormState(() => ({ ...formState, [e.target.name]: e.target.value }));
  }

  useEffect(() => {
    updateTitle("Settings");
    isUser && setUser(userFromProps);
    isOrgAdmin && setUser(orgFromProps);
    isSuperAdmin && setUser({ name: "Super Admin" });
  }, [updateTitle, isUser, userFromProps, isOrgAdmin, orgFromProps]);

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />

      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <TextField
            className={classes.textField}
            autoComplete="name"
            name="name"
            variant="outlined"
            fullWidth
            id="name"
            label="Name"
            value={user.name}
            InputLabelProps={{ shrink: true }}
            autoFocus
            disabled
          />
        </Grid>
        <Grid item xs={12}>
          <Typography color="secondary">Change password</Typography>
        </Grid>
      </Grid>
      <form
        className={classes.form}
        noValidate
        onSubmit={handleSubmit((data) => onSubmit(data))}
      >
        <Grid container spacing={2}>
          <Grid item xs={12}>
            {!errors.currentPassword ? (
              <TextField
                className={classes.textField}
                variant="outlined"
                color="secondary"
                inputRef={register(validationSchema.currentPassword)}
                fullWidth
                name="currentPassword"
                label="Current password *"
                type="password"
                id="currentPassword"
                autoComplete="currentPassword"
              />
            ) : (
              <TextField
                className={classes.textField}
                variant="outlined"
                fullWidth
                name="currentPassword"
                label="Current password *"
                type="password"
                id="currentPassword"
                autoComplete="currentPassword"
                error
                helperText={errors.currentPassword.message}
                FormHelperTextProps={{
                  className: classes.helperText,
                }}
              />
            )}
          </Grid>
          <Grid item xs={12}>
            {!errors.newPassword ? (
              <TextField
                className={classes.textField}
                variant="outlined"
                color="secondary"
                inputRef={register(validationSchema.newPassword)}
                fullWidth
                name="newPassword"
                label="New password *"
                type="password"
                id="newPassword"
                autoComplete="newPassword"
                onChange={onChange}
              />
            ) : (
              <TextField
                className={classes.textField}
                variant="outlined"
                fullWidth
                name="newPassword"
                label="New password *"
                type="password"
                id="newPassword"
                autoComplete="newPassword"
                error
                helperText={errors.newPassword.message}
                FormHelperTextProps={{
                  className: classes.helperText,
                }}
                onChange={onChange}
              />
            )}
          </Grid>
          <Grid item xs={12}>
            {!errors.repeatPassword ? (
              <TextField
                className={classes.textField}
                color="secondary"
                variant="outlined"
                inputRef={register({
                  validate: (value) =>
                    value === formState.newPassword ||
                    "The passwords do not match",
                })}
                fullWidth
                name="repeatPassword"
                label="Repeat new password *"
                type="password"
                id="repeatPassword"
                autoComplete="repeatPassword"
              />
            ) : (
              <TextField
                className={classes.textField}
                variant="outlined"
                fullWidth
                name="repeatPassword"
                label="Repeat new password *"
                type="password"
                id="repeatPassword"
                autoComplete="repeatPassword"
                error
                helperText={errors.repeatPassword.message}
                FormHelperTextProps={{
                  className: classes.helperText,
                }}
              />
            )}
          </Grid>
        </Grid>
        <Button
          type="submit"
          variant="contained"
          color="secondary"
          className={classes.signInSubmit}
        >
          Change password
        </Button>
      </form>
    </main>
  );
}

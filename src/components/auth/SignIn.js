import { useState } from "react";
import { useStyles } from "../style";
import {
  Button,
  TextField,
  Link,
  InputAdornment,
  Typography,
  IconButton,
  Snackbar,
} from "@material-ui/core";
import { Alert as MuiAlert } from "@material-ui/lab";
import { LockOutlined, Visibility, VisibilityOff } from "@material-ui/icons";
import { useForm } from "react-hook-form";
import { getOrganization, getUser } from "../data";
import { Auth } from "aws-amplify";
import uniqueId from "lodash/uniqueId";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function SignIn(props) {
  const classes = useStyles();
  const {
    updateAuthState,
    setUser,
    setUserType,
    setOrganization,
    setSuperAdmin,
  } = props;
  const { register, handleSubmit, errors } = useForm();
  const [showPassword, updateShowPassword] = useState(false);
  const [signInErrors, setSignInErrors] = useState([]);

  async function onSubmit(data) {
    const { email, password } = data;
    try {
      await Auth.signIn(email, password).then((user) => {
        if (user.challengeName === "NEW_PASSWORD_REQUIRED") {
          setUser(user);
          updateAuthState("createPassword");
        } else {
          setUserType(
            user.signInUserSession.idToken.payload[
              "cognito:groups"
            ][0].toLowerCase()
          );
          if (
            user.signInUserSession.idToken.payload[
              "cognito:groups"
            ][0].toLowerCase() === "user"
          ) {
            getUser(user.attributes.sub).then((res) => {
              setUser(res.data.getUser);
            });
          } else if (
            user.signInUserSession.idToken.payload[
              "cognito:groups"
            ][0].toLowerCase() === "org-admin"
          ) {
            getOrganization(user.attributes.sub).then((res) => {
              setOrganization(res.data.getOrganization);
            });
          } else if (
            user.signInUserSession.idToken.payload[
              "cognito:groups"
            ][0].toLowerCase() === "super-admin"
          ) {
            setSuperAdmin({ id: user.username, email: user.attributes.email });
          }
          updateAuthState("signedIn");
        }
      });
    } catch (err) {
      setSignInErrors([
        ...signInErrors,
        { type: err.code, message: err.message },
      ]);
    }
  }

  const handleClickShowPassword = () => {
    updateShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSignInErrors([]);
  };

  return (
    <div className={classes.signIn}>
      <LockOutlined className={classes.signInIcon} fontSize="large" />
      <Typography component="h1" variant="h6">
        Sign In
      </Typography>
      <form
        className={classes.signInForm}
        onSubmit={handleSubmit((data) => onSubmit(data))}
      >
        {!errors.email ? (
          <TextField
            className={classes.signInTextField}
            color="secondary"
            variant="outlined"
            margin="normal"
            inputRef={register({
              required: "The email field can't be empty",
              pattern: {
                value: /\S+@\S+\.\S+/,
                message: "Enter a valid email address",
              },
            })}
            fullWidth
            id="email"
            label="Email"
            name="email"
            autoComplete="email"
            autoFocus
          />
        ) : (
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            id="email"
            label="Email"
            name="email"
            autoComplete="email"
            autoFocus
            error
            helperText={errors.email.message}
            FormHelperTextProps={{
              className: classes.signInHelperText,
            }}
          />
        )}
        {!errors.password ? (
          <TextField
            variant="outlined"
            color="secondary"
            margin="normal"
            inputRef={register({
              required: "The password field can't be empty",
            })}
            fullWidth
            name="password"
            label="Password"
            type={showPassword ? "text" : "password"}
            id="password"
            autoComplete="current-password"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        ) : (
          <TextField
            variant="outlined"
            margin="normal"
            fullWidth
            name="password"
            label="Password"
            type={showPassword ? "text" : "password"}
            id="password"
            autoComplete="current-password"
            error
            helperText={errors.password.message}
            FormHelperTextProps={{
              className: classes.helperText,
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        )}

        <Button
          type="submit"
          color="secondary"
          fullWidth
          variant="contained"
          className={classes.signInSubmit}
        >
          Sign In
        </Button>
        <Link
          onClick={() => updateAuthState("forgotPassword", {})}
          variant="body2"
          className={classes.signInLink}
        >
          Forgot password?
        </Link>
      </form>
      {signInErrors &&
        signInErrors.map((error) => (
          <Snackbar
            open={true}
            autoHideDuration={6000}
            onClose={handleClose}
            key={uniqueId("sign-in-")}
          >
            <Alert onClose={handleClose} severity="error">
              {error.message}
            </Alert>
          </Snackbar>
        ))}
    </div>
  );
}

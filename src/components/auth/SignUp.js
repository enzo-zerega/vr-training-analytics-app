import { useState, useEffect } from "react";
import {
  Button,
  Chip,
  Grid,
  TextField,
  Typography,
  Snackbar,
} from "@material-ui/core";
import { Cancel, Add } from "@material-ui/icons";
import useStyles from "../style/UseStyles";
import { useForm } from "react-hook-form";
import awsmobile from "../../aws-exports";
import AWS from "aws-sdk";
import uniqueId from "lodash/uniqueId";
import { Alert as MuiAlert } from "@material-ui/lab";
import { createUser, createTraining } from "../data";
import { useHistory } from "react-router-dom";

const { REACT_APP_IDENTITY_POOL_ID } = process.env;

AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: REACT_APP_IDENTITY_POOL_ID,
});

AWS.config.region = awsmobile.aws_cognito_region;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function SignUp(props) {
  const { register, handleSubmit, errors } = useForm();
  const { updateTitle, organization: organizationFromProps } = props;
  const classes = useStyles();
  const [orgTrainings, setOrgTrainings] = useState(
    organizationFromProps.trainings.items
  );
  const [trainings, setTrainings] = useState(
    organizationFromProps.trainings.items
  );
  const [availableTrainings, setAvailableTrainings] = useState([{ id: "" }]);
  const [signUpErrors, setSignUpErrors] = useState([]);
  const organizationId = organizationFromProps.id;
  const history = useHistory();

  async function onSubmit(validatedData) {
    const { email, name } = validatedData;
    try {
      const cognito = new AWS.CognitoIdentityServiceProvider();
      await cognito
        .adminCreateUser({
          UserPoolId: `${awsmobile.aws_user_pools_id}`,
          Username: email,
          TemporaryPassword: undefined,
        })
        .promise()
        .then((res) => {
          addUserToGroup(res.User);
          return res.User;
        })
        .then((user) =>
          createUser({
            id: user.Username,
            organizationId,
            name: name.trim(),
            status: user.UserStatus,
          })
        )
        .then((user) => {
          trainings.map((training) =>
            createTraining({
              userId: user.data.createUser.id,
              organizationTrainingId: training.id,
              metricCards: "{}",
              organizationId,
            })
          );
          history.push("/");
        })
        .catch((err) =>
          setSignUpErrors([
            ...signUpErrors,
            { type: err.code, message: err.message },
          ])
        );
    } catch (err) {
      setSignUpErrors([
        ...signUpErrors,
        { type: err.code, message: err.message },
      ]);
    }
  }

  async function addUserToGroup(user) {
    try {
      const cognito = new AWS.CognitoIdentityServiceProvider();

      await cognito
        .adminAddUserToGroup({
          GroupName: "user",
          UserPoolId: `${awsmobile.aws_user_pools_id}`,
          Username: user.Username,
        })
        .promise();
    } catch (err) {
      setSignUpErrors([
        ...signUpErrors,
        { type: err.code, message: err.message },
      ]);
    }
  }

  useEffect(() => {
    updateTitle("Add user");
    setTrainings(organizationFromProps.trainings.items);
    setOrgTrainings(organizationFromProps.trainings.items);
  }, [organizationFromProps.trainings.items, updateTitle]);

  const handleDelete = (id) => {
    const filteredTrainings = trainings.filter(
      (training) => training.id !== id
    );
    const selectedTraining = orgTrainings.filter(
      (training) => training.id === id
    );
    setAvailableTrainings([...availableTrainings, selectedTraining[0]]);
    setTrainings(filteredTrainings);
  };

  const handleAdd = (id) => {
    const selectedTraining = orgTrainings.filter(
      (training) => training.id === id
    );
    const filteredTrainings = availableTrainings.filter(
      (training) => training.id !== id
    );

    setAvailableTrainings(filteredTrainings);
    setTrainings([...trainings, selectedTraining[0]]);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSignUpErrors([]);
  };

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className={classes.dashboard}>
        <div className={classes.signUp}>
          <Typography
            className={classes.signUpTitle}
            component="h1"
            variant="h6"
          >
            Register Form
          </Typography>
          <form
            className={classes.signUpForm}
            noValidate
            onSubmit={handleSubmit((data) => onSubmit(data))}
          >
            <Grid container spacing={4}>
              <Grid item xs={12}>
                {!errors.name ? (
                  <TextField
                    autoComplete="name"
                    color="secondary"
                    inputRef={register({
                      required: "The first name field can't be empty",
                      maxLength: {
                        value: 50,
                        message: "The name cannot have more than 50 characters",
                      },
                    })}
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Name"
                    autoFocus
                  />
                ) : (
                  <TextField
                    autoComplete="name"
                    error
                    helperText={errors.name.message}
                    FormHelperTextProps={{
                      className: classes.signInHelperText,
                    }}
                    name="name"
                    variant="outlined"
                    required
                    fullWidth
                    id="name"
                    label="Name"
                    autoFocus
                  />
                )}
              </Grid>
              <Grid item xs={12}>
                {!errors.email ? (
                  <TextField
                    color="secondary"
                    inputRef={register({
                      required: "The email field can't be empty",
                      pattern: {
                        value: /\S+@\S+\.\S+/,
                        message: "Enter a valid email address",
                      },
                    })}
                    variant="outlined"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                  />
                ) : (
                  <TextField
                    error
                    helperText={errors.email.message}
                    FormHelperTextProps={{
                      className: classes.signInHelperText,
                    }}
                    variant="outlined"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                  />
                )}
              </Grid>
            </Grid>
            <Typography className={classes.signUpSubtitle}>
              Enrolled to trainings:
            </Typography>
            <div className={classes.trainingChipsRegister}>
              {orgTrainings.length
                ? trainings.map((training) => (
                    <Chip
                      className={classes.trainingChipRegister}
                      label={`${training.name}`}
                      onDelete={() => {
                        handleDelete(training.id);
                      }}
                      deleteIcon={<Cancel />}
                      key={uniqueId("sign-up-")}
                    />
                  ))
                : ""}
              {orgTrainings.length ? (
                availableTrainings.map(
                  (orgTraining) =>
                    orgTraining.id && (
                      <Chip
                        className={classes.trainingChipUnregister}
                        label={`${orgTraining.name}`}
                        onDelete={() => {
                          handleAdd(orgTraining.id);
                        }}
                        deleteIcon={<Add />}
                        key={uniqueId("sign-up")}
                      />
                    )
                )
              ) : (
                <Typography className={classes.counterZero}>
                  No trainings available.
                </Typography>
              )}
            </div>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.signUpSubmit}
            >
              Add user
            </Button>
          </form>
        </div>
        {signUpErrors &&
          signUpErrors.map((error) => (
            <Snackbar
              open={true}
              autoHideDuration={6000}
              onClose={handleClose}
              key={uniqueId("sign-in-")}
            >
              <Alert onClose={handleClose} severity="error">
                {error.message}
              </Alert>
            </Snackbar>
          ))}
      </div>
    </main>
  );
}

import { useState } from "react";
import { useStyles } from "../style";
import { Avatar, Button, TextField, Link, Typography } from "@material-ui/core";
import { LockOutlined } from "@material-ui/icons";
import { useForm } from "react-hook-form";
import { Auth } from "aws-amplify";
import { getOrganization, getUser } from "../data";

const initialFormState = {
  newPassword: "",
  repeatPassword: "",
};

export default function CreatePassword(props) {
  const classes = useStyles();
  const { register, handleSubmit, errors } = useForm();
  const {
    updateAuthState,
    user,
    setUserType,
    setUser,
    setOrganization,
  } = props;
  const [formState, updateFormState] = useState(initialFormState);

  async function onSubmit(data) {
    const { newPassword } = data;
    try {
      await Auth.completeNewPassword(user, newPassword).then((response) => {
        setUserType(
          response.signInUserSession.idToken.payload[
            "cognito:groups"
          ][0].toLowerCase()
        );
        if (
          response.signInUserSession.idToken.payload[
            "cognito:groups"
          ][0].toLowerCase() === "user"
        ) {
          getUser(response.username).then((res) => {
            setUser(res.data.getUser);
          });
        } else if (
          response.signInUserSession.idToken.payload[
            "cognito:groups"
          ][0].toLowerCase() === "org-admin"
        ) {
          getOrganization(response.username).then((res) => {
            setOrganization(res.data.getOrganization);
          });
        }
        updateAuthState("signedIn");
      });
    } catch (err) {
      console.log(err);
      updateAuthState("signIn", {});
    }
  }

  function onChange(e) {
    e.persist();
    updateFormState(() => ({ ...formState, [e.target.name]: e.target.value }));
  }

  const validationSchema = {
    newPassword: {
      required: "Write a password",
      minLength: {
        value: 8,
        message: "The password must have at least 8 characters",
      },
      pattern: {
        value: /^(?=.*\d)(?=.*[!@#$%^&?*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/,
        message:
          "The password must contain at least one number, one lower case and one upper case",
      },
    },
  };

  return (
    <div className={classes.signIn}>
      <Avatar className={classes.signInIcon}>
        <LockOutlined />
      </Avatar>
      <Typography component="h1" variant="h6">
        Create a new password
      </Typography>
      <form
        className={classes.signInForm}
        onSubmit={handleSubmit((data) => onSubmit(data))}
      >
        {!errors.newPassword ? (
          <TextField
            className={classes.signInTextField}
            variant="outlined"
            color="secondary"
            inputRef={register(validationSchema.newPassword)}
            fullWidth
            name="newPassword"
            label="New password *"
            type="password"
            id="newPassword"
            autoComplete="newPassword"
            onChange={onChange}
          />
        ) : (
          <TextField
            className={classes.signInTextField}
            variant="outlined"
            fullWidth
            name="newPassword"
            label="New password *"
            type="password"
            id="newPassword"
            autoComplete="newPassword"
            error
            helperText={errors.newPassword.message}
            FormHelperTextProps={{
              className: classes.helperText,
            }}
            onChange={onChange}
          />
        )}
        {!errors.repeatPassword ? (
          <TextField
            className={classes.textField}
            color="secondary"
            variant="outlined"
            inputRef={register({
              validate: (value) =>
                value === formState.newPassword || "The passwords do not match",
            })}
            fullWidth
            name="repeatPassword"
            label="Repeat new password *"
            type="password"
            id="repeatPassword"
            autoComplete="repeatPassword"
            onChange={onChange}
          />
        ) : (
          <TextField
            className={classes.textField}
            variant="outlined"
            fullWidth
            name="repeatPassword"
            label="Repeat new password *"
            type="password"
            id="repeatPassword"
            autoComplete="repeatPassword"
            error
            helperText={errors.repeatPassword.message}
            FormHelperTextProps={{
              className: classes.helperText,
            }}
            onChange={onChange}
          />
        )}
        <Button
          type="submit"
          className={classes.signInSubmit}
          fullWidth
          variant="contained"
          color="secondary"
        >
          Change password
        </Button>
        <Link
          onClick={() => updateAuthState("signIn", {})}
          variant="body2"
          className={classes.signInLink}
        >
          Back to Sign In →
        </Link>
      </form>
    </div>
  );
}

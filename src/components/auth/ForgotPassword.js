import { useStyles } from "../style";
import { Button, TextField, Link, Typography } from "@material-ui/core";
import { LockOutlined } from "@material-ui/icons";

// import { useForm } from "react-hook-form";

export default function CreatePassword(props) {
  const classes = useStyles();
  // const { register, handleSubmit } = useForm();

  const { updateAuthState } = props;

  return (
    <div className={classes.signIn}>
      <LockOutlined className={classes.signInIcon} fontSize="large" />
      <Typography component="h1" variant="h6">
        Forgot your password?
      </Typography>
      <form
        className={classes.signInForm}
        onSubmit={(data) => console.log(data)}
      >
        <TextField
          variant="outlined"
          className={classes.signInTextField}
          color="secondary"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          autoFocus
        />
        <Button
          type="submit"
          className={classes.signInSubmit}
          fullWidth
          variant="contained"
          color="secondary"
        >
          Send reset link
        </Button>
        <Link
          onClick={() => updateAuthState("signIn", {})}
          variant="body2"
          className={classes.signInLink}
        >
          Back to Sign In →
        </Link>
      </form>
    </div>
  );
}

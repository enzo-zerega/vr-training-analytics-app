import { SignIn, ForgotPassword, CreatePassword } from "./";
import { Fragment } from "react";

function Auth(props) {
  const { authState } = props;

  const signIn = authState === "signIn";
  const createPassword = authState === "createPassword";
  const forgotPassword = authState === "forgotPassword";

  return (
    <Fragment>
      {signIn && <SignIn {...props} />}
      {forgotPassword && <ForgotPassword {...props} />}
      {createPassword && <CreatePassword {...props} />}
    </Fragment>
  );
}

export default Auth;

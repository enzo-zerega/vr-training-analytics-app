import { API, graphqlOperation } from "aws-amplify";
import { createOrganizationTraining as createOrganizationTrainingMutation } from "./mutations";

export default async function createOrganizationTraining(input) {
  const training = await API.graphql(
    graphqlOperation(createOrganizationTrainingMutation, {
      input,
    })
  );
  return training;
}

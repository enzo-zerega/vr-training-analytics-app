import { API, graphqlOperation } from "aws-amplify";
import { updateUser as updateUserMutation } from "./mutations";

export default async function updateUser(input) {
  const user = await API.graphql(
    graphqlOperation(updateUserMutation, {
      input,
    })
  );
  return user;
}

import { API, graphqlOperation } from "aws-amplify";
import { listUsers } from "./queries";

export default async function getUsers(orgId) {
  const users = await API.graphql(graphqlOperation(listUsers, { orgId }));
  return users;
}

import { API, graphqlOperation } from "aws-amplify";
import { deleteTraining as deleteTrainingMutation } from "./mutations";

export default async function deleteTraining(input) {
  const training = await API.graphql(
    graphqlOperation(deleteTrainingMutation, {
      input,
    })
  );
  return training;
}

import { API, graphqlOperation } from "aws-amplify";
import { listSessions as listSessionsQuery } from "./queries";

export default async function listSessions() {
  const sessions = await API.graphql(graphqlOperation(listSessionsQuery));
  return sessions;
}

const organization = {
  id: "",
  name: "",
  avatar: "",
  trainings: {
    items: [
      {
        id: "",
        name: "",
        trainingIndicators: `[{"type":""}]`,
        sessionIndicators: `[{"type":""}]`,
        metricCards: `[{"type":"","card":""}]`,
      },
    ],
  },
  users: { items: [] },
  globalIndicators: `[{"type":""}]`,
};

export default function getInitialOrganization() {
  return organization;
}

const indicators = [
  "progress",
  "timespent",
  "monthlytimespent",
  "sessionstatus",
  "passrate",
  "monthlypassrate",
  "frequenterrors",
];

export default function listTrainingIndicators() {
  return indicators;
}

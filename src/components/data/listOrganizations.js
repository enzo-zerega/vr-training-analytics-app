import { API, graphqlOperation } from "aws-amplify";
import { listOrganizations as listOrganizationsQuery } from "./queries";

export default async function listOrganizations() {
  const sessions = await API.graphql(graphqlOperation(listOrganizationsQuery));
  return sessions;
}

import { API, graphqlOperation } from "aws-amplify";
import { createUser as createUserMutation } from "./mutations";

export default async function createUser(input) {
  const user = await API.graphql(
    graphqlOperation(createUserMutation, {
      input,
    })
  );
  return user;
}

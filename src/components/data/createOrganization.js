import { API, graphqlOperation } from "aws-amplify";
import { createOrganization as createOrganizationMutation } from "./mutations";

export default async function createOrganization(input) {
  const organization = await API.graphql(
    graphqlOperation(createOrganizationMutation, {
      input,
    })
  );
  return organization;
}

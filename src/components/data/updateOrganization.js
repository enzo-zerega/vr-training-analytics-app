import { API, graphqlOperation } from "aws-amplify";
import { updateOrganization as updateOrganizationMutation } from "./mutations";

export default async function updateOrganization(input) {
  const organization = await API.graphql(
    graphqlOperation(updateOrganizationMutation, {
      input,
    })
  );
  return organization;
}

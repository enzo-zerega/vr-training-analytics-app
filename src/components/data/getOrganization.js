import { API, graphqlOperation } from "aws-amplify";
import { getOrganization as getOrganizationById } from "./queries";

export default async function getOrganization(id) {
  const organization = await API.graphql(
    graphqlOperation(getOrganizationById, { id })
  );
  return organization;
}

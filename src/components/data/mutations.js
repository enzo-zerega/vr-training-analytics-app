export const createUser = /* GraphQL */ `
  mutation CreateUser(
    $input: CreateUserInput!
    $condition: ModelUserConditionInput
  ) {
    createUser(input: $input, condition: $condition) {
      id
    }
  }
`;
export const createTraining = /* GraphQL */ `
  mutation CreateTraining(
    $input: CreateTrainingInput!
    $condition: ModelTrainingConditionInput
  ) {
    createTraining(input: $input, condition: $condition) {
      id
    }
  }
`;
export const updateUser = /* GraphQL */ `
  mutation UpdateUser(
    $input: UpdateUserInput!
    $condition: ModelUserConditionInput
  ) {
    updateUser(input: $input, condition: $condition) {
      id
    }
  }
`;
export const deleteTraining = /* GraphQL */ `
  mutation DeleteTraining(
    $input: DeleteTrainingInput!
    $condition: ModelTrainingConditionInput
  ) {
    deleteTraining(input: $input, condition: $condition) {
      id
    }
  }
`;
export const createOrganization = /* GraphQL */ `
  mutation CreateOrganization(
    $input: CreateOrganizationInput!
    $condition: ModelOrganizationConditionInput
  ) {
    createOrganization(input: $input, condition: $condition) {
      id
    }
  }
`;
export const updateOrganization = /* GraphQL */ `
  mutation UpdateOrganization(
    $input: UpdateOrganizationInput!
    $condition: ModelOrganizationConditionInput
  ) {
    updateOrganization(input: $input, condition: $condition) {
      id
    }
  }
`;
export const createOrganizationTraining = /* GraphQL */ `
  mutation CreateOrganizationTraining(
    $input: CreateOrganizationTrainingInput!
    $condition: ModelOrganizationTrainingConditionInput
  ) {
    createOrganizationTraining(input: $input, condition: $condition) {
      id
    }
  }
`;
export const updateOrganizationTraining = /* GraphQL */ `
  mutation UpdateOrganizationTraining(
    $input: UpdateOrganizationTrainingInput!
    $condition: ModelOrganizationTrainingConditionInput
  ) {
    updateOrganizationTraining(input: $input, condition: $condition) {
      id
    }
  }
`;

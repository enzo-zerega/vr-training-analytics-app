import { API, graphqlOperation } from "aws-amplify";

export default async function listTrainingsByOrgTrainingId(trainingId) {
  const trainings = await API.graphql(
    graphqlOperation(`
    query ListTrainings {
      listTrainings(filter: {organizationTrainingId: {eq: "${trainingId}"}}) {
        items {
          id
          details {
            id
            name
          }
          sessions {
            items {
              id
              createdAt
              name
              type
              stepsNumber
              steps {
                items {
                  id
                  startedAt
                  finishedAt
                  status
                  name
                }
                nextToken
              }
            }
          }
        }
        nextToken
      }
    }
    `)
  );
  return trainings;
}

const user = {
  id: "",
  name: "",
  status: "",
  organization: {
    name: "",
    id: "",
    avatar: "",
    globalIndicators: `[{"type":"","parameters":{"scope":""}}]`,
  },
  organizationId: "",
  trainings: {
    items: [
      {
        details: {
          name: "",
          trainingIndicators: `[{"type":""}]`,
          sessionIndicators: `[{"type":""}]`,
          metricCards: `[{"type":"","card":""}]`,
        },
        sessions: {
          items: [
            {
              name: "",
              type: "",
              stepsNumber: 0,
              steps: {
                items: [
                  {
                    name: "",
                    startedAt: "2021-01-01T00:00:00+01:00",
                    finishedAt: "2021-01-01T00:00:00+01:00",
                    status: "",
                    metrics: {
                      items: [
                        {
                          data: `[{"dataset":"","labels":"","text":""}]`,
                          name: "",
                          type: "",
                        },
                      ],
                    },
                  },
                ],
              },
            },
          ],
        },
      },
    ],
  },
};

export default function getInitialUser() {
  return user;
}

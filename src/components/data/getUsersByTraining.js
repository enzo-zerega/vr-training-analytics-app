import { API, graphqlOperation } from "aws-amplify";

export default async function getUsersByTraining(trainingId) {
  const users = await API.graphql(
    graphqlOperation(`
    query ListUsers {
      listUsers {
          items {
            id
            name
            trainings {
              items {
                details {
                  id
                  name
                }
              }
            }
          }
      }
    }
    `)
  );
  let matches = [];
  users.data.listUsers.items.map((user) => {
    user.trainings.items.map((training) => {
      if (training.details.id === trainingId) matches.push(user);
      return training;
    });
    return user;
  });
  return matches;
}

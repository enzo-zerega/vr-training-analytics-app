const indicators = [
  "progress",
  "timespent",
  "monthlytimespent",
  "sessionstatus",
];

export default function listGlobalIndicators() {
  return indicators;
}

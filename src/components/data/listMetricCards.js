const cards = [
  {
    type: "chart",
    cards: ["doughnut", "bar"],
  },
  {
    type: "text",
    cards: ["task"],
  },
  {
    type: "number",
    cards: ["int-unit"],
  },
];

export default function listMetricCards() {
  return cards;
}

import { API, graphqlOperation } from "aws-amplify";
import { createTraining as createTrainingMutation } from "./mutations";

export default async function createUser(input) {
  const training = await API.graphql(
    graphqlOperation(createTrainingMutation, {
      input,
    })
  );
  return training;
}

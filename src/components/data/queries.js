export const getOrganization = /* GraphQL */ `
  query GetOrganization($id: ID!) {
    getOrganization(id: $id) {
      id
      name
      avatar
      trainings {
        items {
          id
          name
          trainingIndicators
          sessionIndicators
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      users {
        items {
          id
        }
        nextToken
      }
      globalIndicators
      createdAt
      updatedAt
    }
  }
`;

export const listOrganizationTrainings = /* GraphQL */ `
  query ListOrganizationTrainings(
    $filter: ModelOrganizationTrainingFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listOrganizationTrainings(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        organizationId
        name
        trainingIndicators
        sessionIndicators
        metricCards
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      organizationId
      organization {
        id
        name
        avatar
        globalIndicators
      }
      name
      status
      trainings {
        items {
          id
          details {
            id
            name
            trainingIndicators
            sessionIndicators
            metricCards
          }
          metricCards
          createdAt
          updatedAt
          sessions {
            items {
              id
              createdAt
              name
              type
              stepsNumber
              steps {
                items {
                  id
                  createdAt
                  startedAt
                  finishedAt
                  name
                  status
                  metrics {
                    items {
                      id
                      name
                      type
                      data
                    }
                    nextToken
                  }
                }
              }
            }
          }
        }
      }
      createdAt
      updatedAt
    }
  }
`;
export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        organizationId
        name
        status
        trainings {
          items {
            id
            details {
              name
            }
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

export const listSessions = /* GraphQL */ `
  query ListSessions(
    $filter: ModelSessionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSessions(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        createdAt
        name
        type
        stepsNumber
        steps {
          items {
            id
            createdAt
            startedAt
            finishedAt
            name
            status
            metrics {
              items {
                id
                name
                type
                data
              }
              nextToken
            }
          }
        }
      }
      nextToken
    }
  }
`;
export const listOrganizations = /* GraphQL */ `
  query ListOrganizations(
    $filter: ModelOrganizationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listOrganizations(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        avatar
        trainings {
          items {
            id
            name
            trainingIndicators
            sessionIndicators
            metricCards
            createdAt
            updatedAt
          }
          nextToken
        }
        users {
          items {
            id
          }
          nextToken
        }
        globalIndicators
      }
      nextToken
    }
  }
`;

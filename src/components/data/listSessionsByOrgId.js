import { API, graphqlOperation } from "aws-amplify";

export default async function listSessionsByOrgId(orgId) {
  const trainings = await API.graphql(
    graphqlOperation(`
    query ListSessions {
      listSessions(filter: {organizationId: {eq: "${orgId}"}}) {
        items {
          id
          createdAt
          name
          type
          stepsNumber
          steps {
            items {
              id
              createdAt
              startedAt
              finishedAt
              name
              status
              metrics {
                items {
                  id
                  name
                  type
                  data
                }
                nextToken
              }
            }
          }
        }
      }
    }
    `)
  );
  return trainings;
}

import { API, graphqlOperation } from "aws-amplify";
import { listOrganizationTrainings as listTrainingsQuery } from "./queries";

export default async function listOrganizationTrainings() {
  const trainings = await API.graphql(graphqlOperation(listTrainingsQuery));
  return trainings;
}

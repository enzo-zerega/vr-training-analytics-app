import { API, graphqlOperation } from "aws-amplify";
import { updateOrganizationTraining as updateOrganizationTrainingMutation } from "./mutations";

export default async function updateOrganization(input) {
  const training = await API.graphql(
    graphqlOperation(updateOrganizationTrainingMutation, {
      input,
    })
  );
  return training;
}

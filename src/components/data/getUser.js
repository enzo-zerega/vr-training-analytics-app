import { API, graphqlOperation } from "aws-amplify";
import { getUser as getUserById } from "./queries";

export default async function getUser(id) {
  const user = await API.graphql(graphqlOperation(getUserById, { id }));
  return user;
}

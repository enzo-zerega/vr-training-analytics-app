import { API, graphqlOperation } from "aws-amplify";
import { listOrganizationTrainings as listTrainingsQuery } from "./queries";

export default async function listTrainings() {
  const trainings = await API.graphql(graphqlOperation(listTrainingsQuery));
  return trainings;
}

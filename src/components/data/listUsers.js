import { API, graphqlOperation } from "aws-amplify";
import { listUsers as listUsersQuery } from "./queries";

export default async function listUsers() {
  const users = await API.graphql(graphqlOperation(listUsersQuery));
  return users;
}

/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createOrganization = /* GraphQL */ `
  mutation CreateOrganization(
    $input: CreateOrganizationInput!
    $condition: ModelOrganizationConditionInput
  ) {
    createOrganization(input: $input, condition: $condition) {
      id
      name
      avatar
      trainings {
        items {
          id
          organizationId
          name
          trainingIndicators
          sessionIndicators
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      users {
        items {
          id
          organizationId
          status
          name
          createdAt
          updatedAt
        }
        nextToken
      }
      globalIndicators
      createdAt
      updatedAt
    }
  }
`;
export const updateOrganization = /* GraphQL */ `
  mutation UpdateOrganization(
    $input: UpdateOrganizationInput!
    $condition: ModelOrganizationConditionInput
  ) {
    updateOrganization(input: $input, condition: $condition) {
      id
      name
      avatar
      trainings {
        items {
          id
          organizationId
          name
          trainingIndicators
          sessionIndicators
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      users {
        items {
          id
          organizationId
          status
          name
          createdAt
          updatedAt
        }
        nextToken
      }
      globalIndicators
      createdAt
      updatedAt
    }
  }
`;
export const deleteOrganization = /* GraphQL */ `
  mutation DeleteOrganization(
    $input: DeleteOrganizationInput!
    $condition: ModelOrganizationConditionInput
  ) {
    deleteOrganization(input: $input, condition: $condition) {
      id
      name
      avatar
      trainings {
        items {
          id
          organizationId
          name
          trainingIndicators
          sessionIndicators
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      users {
        items {
          id
          organizationId
          status
          name
          createdAt
          updatedAt
        }
        nextToken
      }
      globalIndicators
      createdAt
      updatedAt
    }
  }
`;
export const createOrganizationTraining = /* GraphQL */ `
  mutation CreateOrganizationTraining(
    $input: CreateOrganizationTrainingInput!
    $condition: ModelOrganizationTrainingConditionInput
  ) {
    createOrganizationTraining(input: $input, condition: $condition) {
      id
      organizationId
      name
      trainingIndicators
      sessionIndicators
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const updateOrganizationTraining = /* GraphQL */ `
  mutation UpdateOrganizationTraining(
    $input: UpdateOrganizationTrainingInput!
    $condition: ModelOrganizationTrainingConditionInput
  ) {
    updateOrganizationTraining(input: $input, condition: $condition) {
      id
      organizationId
      name
      trainingIndicators
      sessionIndicators
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const deleteOrganizationTraining = /* GraphQL */ `
  mutation DeleteOrganizationTraining(
    $input: DeleteOrganizationTrainingInput!
    $condition: ModelOrganizationTrainingConditionInput
  ) {
    deleteOrganizationTraining(input: $input, condition: $condition) {
      id
      organizationId
      name
      trainingIndicators
      sessionIndicators
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const createUser = /* GraphQL */ `
  mutation CreateUser(
    $input: CreateUserInput!
    $condition: ModelUserConditionInput
  ) {
    createUser(input: $input, condition: $condition) {
      id
      organizationId
      status
      organization {
        id
        name
        avatar
        trainings {
          nextToken
        }
        users {
          nextToken
        }
        globalIndicators
        createdAt
        updatedAt
      }
      name
      trainings {
        items {
          id
          userId
          organizationId
          organizationTrainingId
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateUser = /* GraphQL */ `
  mutation UpdateUser(
    $input: UpdateUserInput!
    $condition: ModelUserConditionInput
  ) {
    updateUser(input: $input, condition: $condition) {
      id
      organizationId
      status
      organization {
        id
        name
        avatar
        trainings {
          nextToken
        }
        users {
          nextToken
        }
        globalIndicators
        createdAt
        updatedAt
      }
      name
      trainings {
        items {
          id
          userId
          organizationId
          organizationTrainingId
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteUser = /* GraphQL */ `
  mutation DeleteUser(
    $input: DeleteUserInput!
    $condition: ModelUserConditionInput
  ) {
    deleteUser(input: $input, condition: $condition) {
      id
      organizationId
      status
      organization {
        id
        name
        avatar
        trainings {
          nextToken
        }
        users {
          nextToken
        }
        globalIndicators
        createdAt
        updatedAt
      }
      name
      trainings {
        items {
          id
          userId
          organizationId
          organizationTrainingId
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createTraining = /* GraphQL */ `
  mutation CreateTraining(
    $input: CreateTrainingInput!
    $condition: ModelTrainingConditionInput
  ) {
    createTraining(input: $input, condition: $condition) {
      id
      userId
      organizationId
      organizationTrainingId
      details {
        id
        organizationId
        name
        trainingIndicators
        sessionIndicators
        metricCards
        createdAt
        updatedAt
      }
      sessions {
        items {
          id
          userId
          organizationId
          name
          type
          trainingId
          stepsNumber
          createdAt
          updatedAt
        }
        nextToken
      }
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const updateTraining = /* GraphQL */ `
  mutation UpdateTraining(
    $input: UpdateTrainingInput!
    $condition: ModelTrainingConditionInput
  ) {
    updateTraining(input: $input, condition: $condition) {
      id
      userId
      organizationId
      organizationTrainingId
      details {
        id
        organizationId
        name
        trainingIndicators
        sessionIndicators
        metricCards
        createdAt
        updatedAt
      }
      sessions {
        items {
          id
          userId
          organizationId
          name
          type
          trainingId
          stepsNumber
          createdAt
          updatedAt
        }
        nextToken
      }
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const deleteTraining = /* GraphQL */ `
  mutation DeleteTraining(
    $input: DeleteTrainingInput!
    $condition: ModelTrainingConditionInput
  ) {
    deleteTraining(input: $input, condition: $condition) {
      id
      userId
      organizationId
      organizationTrainingId
      details {
        id
        organizationId
        name
        trainingIndicators
        sessionIndicators
        metricCards
        createdAt
        updatedAt
      }
      sessions {
        items {
          id
          userId
          organizationId
          name
          type
          trainingId
          stepsNumber
          createdAt
          updatedAt
        }
        nextToken
      }
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const createSession = /* GraphQL */ `
  mutation CreateSession(
    $input: CreateSessionInput!
    $condition: ModelSessionConditionInput
  ) {
    createSession(input: $input, condition: $condition) {
      id
      userId
      organizationId
      name
      type
      trainingId
      stepsNumber
      steps {
        items {
          id
          userId
          organizationId
          name
          sessionId
          startedAt
          finishedAt
          status
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateSession = /* GraphQL */ `
  mutation UpdateSession(
    $input: UpdateSessionInput!
    $condition: ModelSessionConditionInput
  ) {
    updateSession(input: $input, condition: $condition) {
      id
      userId
      organizationId
      name
      type
      trainingId
      stepsNumber
      steps {
        items {
          id
          userId
          organizationId
          name
          sessionId
          startedAt
          finishedAt
          status
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteSession = /* GraphQL */ `
  mutation DeleteSession(
    $input: DeleteSessionInput!
    $condition: ModelSessionConditionInput
  ) {
    deleteSession(input: $input, condition: $condition) {
      id
      userId
      organizationId
      name
      type
      trainingId
      stepsNumber
      steps {
        items {
          id
          userId
          organizationId
          name
          sessionId
          startedAt
          finishedAt
          status
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createStep = /* GraphQL */ `
  mutation CreateStep(
    $input: CreateStepInput!
    $condition: ModelStepConditionInput
  ) {
    createStep(input: $input, condition: $condition) {
      id
      userId
      organizationId
      name
      sessionId
      startedAt
      finishedAt
      status
      metrics {
        items {
          id
          userId
          organizationId
          stepId
          name
          type
          data
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateStep = /* GraphQL */ `
  mutation UpdateStep(
    $input: UpdateStepInput!
    $condition: ModelStepConditionInput
  ) {
    updateStep(input: $input, condition: $condition) {
      id
      userId
      organizationId
      name
      sessionId
      startedAt
      finishedAt
      status
      metrics {
        items {
          id
          userId
          organizationId
          stepId
          name
          type
          data
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteStep = /* GraphQL */ `
  mutation DeleteStep(
    $input: DeleteStepInput!
    $condition: ModelStepConditionInput
  ) {
    deleteStep(input: $input, condition: $condition) {
      id
      userId
      organizationId
      name
      sessionId
      startedAt
      finishedAt
      status
      metrics {
        items {
          id
          userId
          organizationId
          stepId
          name
          type
          data
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createMetric = /* GraphQL */ `
  mutation CreateMetric(
    $input: CreateMetricInput!
    $condition: ModelMetricConditionInput
  ) {
    createMetric(input: $input, condition: $condition) {
      id
      userId
      organizationId
      stepId
      name
      type
      data
      createdAt
      updatedAt
    }
  }
`;
export const updateMetric = /* GraphQL */ `
  mutation UpdateMetric(
    $input: UpdateMetricInput!
    $condition: ModelMetricConditionInput
  ) {
    updateMetric(input: $input, condition: $condition) {
      id
      userId
      organizationId
      stepId
      name
      type
      data
      createdAt
      updatedAt
    }
  }
`;
export const deleteMetric = /* GraphQL */ `
  mutation DeleteMetric(
    $input: DeleteMetricInput!
    $condition: ModelMetricConditionInput
  ) {
    deleteMetric(input: $input, condition: $condition) {
      id
      userId
      organizationId
      stepId
      name
      type
      data
      createdAt
      updatedAt
    }
  }
`;

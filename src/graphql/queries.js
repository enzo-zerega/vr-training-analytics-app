/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getOrganization = /* GraphQL */ `
  query GetOrganization($id: ID!) {
    getOrganization(id: $id) {
      id
      name
      avatar
      trainings {
        items {
          id
          organizationId
          name
          trainingIndicators
          sessionIndicators
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      users {
        items {
          id
          organizationId
          status
          name
          createdAt
          updatedAt
        }
        nextToken
      }
      globalIndicators
      createdAt
      updatedAt
    }
  }
`;
export const listOrganizations = /* GraphQL */ `
  query ListOrganizations(
    $filter: ModelOrganizationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listOrganizations(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        avatar
        trainings {
          nextToken
        }
        users {
          nextToken
        }
        globalIndicators
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getOrganizationTraining = /* GraphQL */ `
  query GetOrganizationTraining($id: ID!) {
    getOrganizationTraining(id: $id) {
      id
      organizationId
      name
      trainingIndicators
      sessionIndicators
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const listOrganizationTrainings = /* GraphQL */ `
  query ListOrganizationTrainings(
    $filter: ModelOrganizationTrainingFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listOrganizationTrainings(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        organizationId
        name
        trainingIndicators
        sessionIndicators
        metricCards
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      organizationId
      status
      organization {
        id
        name
        avatar
        trainings {
          nextToken
        }
        users {
          nextToken
        }
        globalIndicators
        createdAt
        updatedAt
      }
      name
      trainings {
        items {
          id
          userId
          organizationId
          organizationTrainingId
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        organizationId
        status
        organization {
          id
          name
          avatar
          globalIndicators
          createdAt
          updatedAt
        }
        name
        trainings {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getTraining = /* GraphQL */ `
  query GetTraining($id: ID!) {
    getTraining(id: $id) {
      id
      userId
      organizationId
      organizationTrainingId
      details {
        id
        organizationId
        name
        trainingIndicators
        sessionIndicators
        metricCards
        createdAt
        updatedAt
      }
      sessions {
        items {
          id
          userId
          organizationId
          name
          type
          trainingId
          stepsNumber
          createdAt
          updatedAt
        }
        nextToken
      }
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const listTrainings = /* GraphQL */ `
  query ListTrainings(
    $filter: ModelTrainingFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTrainings(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        organizationId
        organizationTrainingId
        details {
          id
          organizationId
          name
          trainingIndicators
          sessionIndicators
          metricCards
          createdAt
          updatedAt
        }
        sessions {
          nextToken
        }
        metricCards
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSession = /* GraphQL */ `
  query GetSession($id: ID!) {
    getSession(id: $id) {
      id
      userId
      organizationId
      name
      type
      trainingId
      stepsNumber
      steps {
        items {
          id
          userId
          organizationId
          name
          sessionId
          startedAt
          finishedAt
          status
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listSessions = /* GraphQL */ `
  query ListSessions(
    $filter: ModelSessionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSessions(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        organizationId
        name
        type
        trainingId
        stepsNumber
        steps {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getStep = /* GraphQL */ `
  query GetStep($id: ID!) {
    getStep(id: $id) {
      id
      userId
      organizationId
      name
      sessionId
      startedAt
      finishedAt
      status
      metrics {
        items {
          id
          userId
          organizationId
          stepId
          name
          type
          data
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listSteps = /* GraphQL */ `
  query ListSteps(
    $filter: ModelStepFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSteps(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        organizationId
        name
        sessionId
        startedAt
        finishedAt
        status
        metrics {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getMetric = /* GraphQL */ `
  query GetMetric($id: ID!) {
    getMetric(id: $id) {
      id
      userId
      organizationId
      stepId
      name
      type
      data
      createdAt
      updatedAt
    }
  }
`;
export const listMetrics = /* GraphQL */ `
  query ListMetrics(
    $filter: ModelMetricFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listMetrics(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userId
        organizationId
        stepId
        name
        type
        data
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

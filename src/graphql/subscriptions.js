/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateOrganization = /* GraphQL */ `
  subscription OnCreateOrganization($id: String) {
    onCreateOrganization(id: $id) {
      id
      name
      avatar
      trainings {
        items {
          id
          organizationId
          name
          trainingIndicators
          sessionIndicators
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      users {
        items {
          id
          organizationId
          status
          name
          createdAt
          updatedAt
        }
        nextToken
      }
      globalIndicators
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateOrganization = /* GraphQL */ `
  subscription OnUpdateOrganization($id: String) {
    onUpdateOrganization(id: $id) {
      id
      name
      avatar
      trainings {
        items {
          id
          organizationId
          name
          trainingIndicators
          sessionIndicators
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      users {
        items {
          id
          organizationId
          status
          name
          createdAt
          updatedAt
        }
        nextToken
      }
      globalIndicators
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteOrganization = /* GraphQL */ `
  subscription OnDeleteOrganization($id: String) {
    onDeleteOrganization(id: $id) {
      id
      name
      avatar
      trainings {
        items {
          id
          organizationId
          name
          trainingIndicators
          sessionIndicators
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      users {
        items {
          id
          organizationId
          status
          name
          createdAt
          updatedAt
        }
        nextToken
      }
      globalIndicators
      createdAt
      updatedAt
    }
  }
`;
export const onCreateOrganizationTraining = /* GraphQL */ `
  subscription OnCreateOrganizationTraining($organizationId: String) {
    onCreateOrganizationTraining(organizationId: $organizationId) {
      id
      organizationId
      name
      trainingIndicators
      sessionIndicators
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateOrganizationTraining = /* GraphQL */ `
  subscription OnUpdateOrganizationTraining($organizationId: String) {
    onUpdateOrganizationTraining(organizationId: $organizationId) {
      id
      organizationId
      name
      trainingIndicators
      sessionIndicators
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteOrganizationTraining = /* GraphQL */ `
  subscription OnDeleteOrganizationTraining($organizationId: String) {
    onDeleteOrganizationTraining(organizationId: $organizationId) {
      id
      organizationId
      name
      trainingIndicators
      sessionIndicators
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const onCreateUser = /* GraphQL */ `
  subscription OnCreateUser($id: String, $organizationId: String) {
    onCreateUser(id: $id, organizationId: $organizationId) {
      id
      organizationId
      status
      organization {
        id
        name
        avatar
        trainings {
          nextToken
        }
        users {
          nextToken
        }
        globalIndicators
        createdAt
        updatedAt
      }
      name
      trainings {
        items {
          id
          userId
          organizationId
          organizationTrainingId
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUser = /* GraphQL */ `
  subscription OnUpdateUser($id: String, $organizationId: String) {
    onUpdateUser(id: $id, organizationId: $organizationId) {
      id
      organizationId
      status
      organization {
        id
        name
        avatar
        trainings {
          nextToken
        }
        users {
          nextToken
        }
        globalIndicators
        createdAt
        updatedAt
      }
      name
      trainings {
        items {
          id
          userId
          organizationId
          organizationTrainingId
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUser = /* GraphQL */ `
  subscription OnDeleteUser($id: String, $organizationId: String) {
    onDeleteUser(id: $id, organizationId: $organizationId) {
      id
      organizationId
      status
      organization {
        id
        name
        avatar
        trainings {
          nextToken
        }
        users {
          nextToken
        }
        globalIndicators
        createdAt
        updatedAt
      }
      name
      trainings {
        items {
          id
          userId
          organizationId
          organizationTrainingId
          metricCards
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateTraining = /* GraphQL */ `
  subscription OnCreateTraining($userId: String, $organizationId: String) {
    onCreateTraining(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      organizationTrainingId
      details {
        id
        organizationId
        name
        trainingIndicators
        sessionIndicators
        metricCards
        createdAt
        updatedAt
      }
      sessions {
        items {
          id
          userId
          organizationId
          name
          type
          trainingId
          stepsNumber
          createdAt
          updatedAt
        }
        nextToken
      }
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateTraining = /* GraphQL */ `
  subscription OnUpdateTraining($userId: String, $organizationId: String) {
    onUpdateTraining(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      organizationTrainingId
      details {
        id
        organizationId
        name
        trainingIndicators
        sessionIndicators
        metricCards
        createdAt
        updatedAt
      }
      sessions {
        items {
          id
          userId
          organizationId
          name
          type
          trainingId
          stepsNumber
          createdAt
          updatedAt
        }
        nextToken
      }
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteTraining = /* GraphQL */ `
  subscription OnDeleteTraining($userId: String, $organizationId: String) {
    onDeleteTraining(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      organizationTrainingId
      details {
        id
        organizationId
        name
        trainingIndicators
        sessionIndicators
        metricCards
        createdAt
        updatedAt
      }
      sessions {
        items {
          id
          userId
          organizationId
          name
          type
          trainingId
          stepsNumber
          createdAt
          updatedAt
        }
        nextToken
      }
      metricCards
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSession = /* GraphQL */ `
  subscription OnCreateSession($userId: String, $organizationId: String) {
    onCreateSession(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      name
      type
      trainingId
      stepsNumber
      steps {
        items {
          id
          userId
          organizationId
          name
          sessionId
          startedAt
          finishedAt
          status
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSession = /* GraphQL */ `
  subscription OnUpdateSession($userId: String, $organizationId: String) {
    onUpdateSession(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      name
      type
      trainingId
      stepsNumber
      steps {
        items {
          id
          userId
          organizationId
          name
          sessionId
          startedAt
          finishedAt
          status
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSession = /* GraphQL */ `
  subscription OnDeleteSession($userId: String, $organizationId: String) {
    onDeleteSession(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      name
      type
      trainingId
      stepsNumber
      steps {
        items {
          id
          userId
          organizationId
          name
          sessionId
          startedAt
          finishedAt
          status
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateStep = /* GraphQL */ `
  subscription OnCreateStep($userId: String, $organizationId: String) {
    onCreateStep(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      name
      sessionId
      startedAt
      finishedAt
      status
      metrics {
        items {
          id
          userId
          organizationId
          stepId
          name
          type
          data
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateStep = /* GraphQL */ `
  subscription OnUpdateStep($userId: String, $organizationId: String) {
    onUpdateStep(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      name
      sessionId
      startedAt
      finishedAt
      status
      metrics {
        items {
          id
          userId
          organizationId
          stepId
          name
          type
          data
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteStep = /* GraphQL */ `
  subscription OnDeleteStep($userId: String, $organizationId: String) {
    onDeleteStep(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      name
      sessionId
      startedAt
      finishedAt
      status
      metrics {
        items {
          id
          userId
          organizationId
          stepId
          name
          type
          data
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateMetric = /* GraphQL */ `
  subscription OnCreateMetric($userId: String, $organizationId: String) {
    onCreateMetric(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      stepId
      name
      type
      data
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateMetric = /* GraphQL */ `
  subscription OnUpdateMetric($userId: String, $organizationId: String) {
    onUpdateMetric(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      stepId
      name
      type
      data
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteMetric = /* GraphQL */ `
  subscription OnDeleteMetric($userId: String, $organizationId: String) {
    onDeleteMetric(userId: $userId, organizationId: $organizationId) {
      id
      userId
      organizationId
      stepId
      name
      type
      data
      createdAt
      updatedAt
    }
  }
`;
